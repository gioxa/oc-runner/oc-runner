
## copy

### purpose

copy data from a file or text to a variable or a file or the terminal, with an image like scratch!

```bash
copy \
   (--from_text="text"|--from_file=file) \
   (--to_var=varname|--to_file=filename|--to_term) \
   [--substitute] \
   [--append] \
   [--quiet] \
   [--if_value=${var}] \
   [--if_zero="${var}"]
```
#### Optional:

- `--substitute` : all variables in the text or file are resolved.
- `--append` :the from_xxx will be appended to the defined output, exept for the `--to_term`
- `--quiet` : no info will be outputed, and with `--term` only the data is displayed.
- `--if_value=${var}` : command will only execute on the `var` not empty
- `--if_zero="${var}"` : command will only be executed if th `var` is not defined or empty

### Note

1. when using --from_text, enclose option in double quotes (`"`), espicially when an variable consist out of multiple words.
2. `--substitute` will substitute `${var}` or `$var` with environment variables. 
   For correct wow, use `${var}`, if a `$notavar` is requeired, escape by double `$`, use `$$notavar` or `$${notavar}`

[top](#oc-runner)

