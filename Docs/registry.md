
# Build-in commands

## Registry Push

### registry_push [options] [pathname ...]

#### options:

 - `--rootfs[=<path2rootfs>]` push a rootfs to registry, default `./rootfs`.
 - `--archive[=<path2archive.tar.gz>]` push an archive as layer, default `./layer.tar.gz`.
 - `--from_image=<repo_path>[:tag]` from a qualified reponame
 - `--from_ISR` default:`<IS_REGISTRY>/namespace/is-project_path_slug[-name][:latest]`
 - `--from_GLR` default:`<CI_REGISTRY>/<CI_PROJECT_PATH>[/name][:latest]`
 - `--from_name` optional name with:
      - `--from_ISR`
      - `--from_GLR`.
 - `--from_reference` optional reference with:
      - `--from_ISR`
      - `--from_GLR`
 - `--froms_credentials` credentials to be used to pull image,
 - `--image=<repo_path>[:tag]` push to image.
 - `--ISR` defaults to `<IS_REGISTRY>/<namespace>/is-<project_path_slug>[-<--name>][:latest]`
 - `--GLR`  defaults to `<CI_REGISTRY>/<CI_PROJECT_PATH>[/<--name>][:latest]`
 - `--name`  optional name with:
      - `--ISR`
      - `--GLR`
 - `--reference`  optional reference with:
      - `--ISR`
      - `--GLR`
 - `--credentials` credentials to be used to push image format:
      - format: `base64("<user>:<token>")`
 - `--config` docker config file, default: `./config.yaml`
 - `--u2g` copies the permissions of the owner to the group.
 - `--skip_label` only valid on a pure Transfer:
      - `--from_xxx`
      - not `--config`
      - not `--rootfs`
      - not `--archive`

   will not append dummy layer with container command history and not update the io.or-runner.from.xxx.

#### pathname:

- Files and/or directories to include in the image layer
- optional defaults to `.` when `--rootfs` is defined
- supports simple bash expansion, through `wordexp()`,
- **Does not support brace expansion: e.g. `registry_push usr/{bin,lib}` will fail!**
- If `--rootfs` is defined, expanded relative to `rootfs`

### registry_push Functions:

#### General Authorisation:

By default, the oc-dispatcher **and** the oc-executor will use the availleble tokens at hand:

1. for dockerHub: the secret variable `DOCKER_CREDENTIALS`
2. for any image mathing the `CI_REGISTRY`, the `CI_REGISTRY_USER` & `PASSWORD` 
3. for ImageStream the service account token of the `oc-dispatcher`

For registy_push this can be overwritten by : 

1. --credentials=ey... : for pushing an image
2. --from_credentials=ey... : for pulling an image

#### 1. Create Image Layer

1. create an image layer from a Directory
    1. by not defining any `(from_xxx)options` nor `--archive` nor `--rootfs` nor `pathname`:  defaults with root at `$PWD/rootfs` and pathname=`.`
    2. with `root` at current Directory by defining at leat one `pathname`, e.g. `.`
    3.  with `--rootfs`
        - define `--rootfs`, defaults to `$PWD/rootfs`
        - define `--rootfs=test` create a layer with root at `$PWD/test`
        - define `--rootfs=/test` create a layer with root at `/test`
        - define `--rootfs=$CI_PROJECT_DIR/test` create a layer with root at `<project root>/test`

    4. with `--rootfs` and `pathname`
    5. File Permisions:    
        - use `--u2g` to copy owner permisions to the group(root) premissions (so no matter image is run under, filesystem can be writable)
        - the Owner/group of all files is changed to `root:root`

2. create an image layer from an archive with `--archive`
    - `--archive` defaults to layer.tar.gz at current directory
    - `--archive=myarchive.tar.gz`
    - Archive is streamed as is, while content and blob SHA256SUM is calculated.

3. `--archive` cannot be used in conjunction with `--rootfs` or `pathname`

#### 2. FROM image

1. create image layers `--from_image=repository_name`
    - dockerhub: `centos:latest`
    - Full Repo Name: `registry.gitlab.com/mygroup/myproject[myname][:myreference]`
    - ImageStream: `ImageStream[/myname][:myreference]`
    - Gitlab_registry: `Gitlab[/myname][:myreference]`

2. create image layers from ImageStream or Gitlab-registry
    - Define from which registry
        - `--from_ISR`
        - `--from_GLR`
    - Optional define `--from_image_name=myname`
    - Optional define `--from_reference=myreference` defaults to `latest`

3. Authorisation
    - Authorisation is used from Gitlab token and Openshift token for authorisation
    - For access to external private repository use:
        `--from_credentials=base64(user:passwd)`

#### 3. CONFIGURE Image

 **`--config[=docker_config.yml]`**

In stead of defining all on command line the docker Config, we use a yaml file.

**Sample `docker_config.yml`**

```yaml
Hostname: "test"
# Delete Entrypoint if present in --from_image
Entrypoint: null
WorkDir: "/"
Cmd:
  # delete all Cmd Entries if present in --from_image
  - _*: null
  # and add new entries for Cmd
  - /bin/bash
  - -c
  - echo hello
User: ""
Volumes:
  # delete volume /home if present in --from_image, and add /build
  - /home: null
  - /build
ExportPorts:
  - 8080/tcp
  # and delete 9090/udp entry if present in --from_image
  - 9090/udp: null
Labels:
  maintainer: "$GITLAB_USER_EMAIL"
  title: |
          Test Image for building with --config
  description: |
             Test Image for building and testing oc-runner :
             Push and so on
  vendor: Gioxa Ltd. HongKong                
  tags: "$IMAGE_TAGS"
```

- Parsing of the yaml:
    - support for bash variable substitution, if the String value has double quotes
    - Single quotes will force a string.
    - a number or True or False without quotes will result in a json number and json boolean object!

**On a Merge** *(when using --from_image=....)*

Old value's are merged,deleted or overwriten.

- Set item as:

    ```yaml
  key/value-item: null
```
and  all old key:values are deleted and the key/value-item is set to null.

-  set key/value with

    ```yaml
key/value-item:
  _*: null
  key: value
```
to delete all old key:value's and append new values on Merge

- set an key/value to

    ```yaml
key/value-item:
  key: null
```
to delete that key on Merge

- set to "" :

    ```yaml
key/value-item:
  key:
```

will set `"key": ""` after merge, no delete!!

#### 4. PUSH **Examples:**

- keep it real simple

    ```bash
registry_push
```
    OR

    ```bash
registry_push [--GLR] [reference=latest] [--rootfs][=./rootfs]
```
   - will create from `./rootfs` directory a new image layer
   - pushed to registry as `Gilab:latest` or `$CI_REGISTRY_IMAGE:latest`
   - the [optional] items are the defaults

- keep it simple

    ```bash
registry_push --name=test .
```
    - will create from `.` of the current directory a new image layer
    - pushed to registry as `Gilab:latest` or `$CI_REGISTRY_IMAGE:latest`


-  Create single layer image from defined filesystem:

    ```bash
    registry_push \
         --ISR \
         --rootfs=/ \
         --u2g \
         /opt/el7/x86_64/*.rpm \
         /etc/myconfig
    ```
    - will create an image layer
    - push it to the ImageStream,
    - will be containing:
        - `/opt/el7/x86_64/*.rpm`
        - `/etc/myconfig`
    - will have same group permissions (root) as owner (non-root)
    - accesible in CI as:
        - `ImageStream`
        - `ImageStream:latest`

-  Create single layer from existing archive

    ```bash
registry_push --archive --ISR --name=temp1 --reference=first
```

    - will create a single layer image from `./layer.tar.gz`
    - push it to the ImageStream, accesible in CI as: `ImageStream/temp1:first`

- Append a rootfs to an existing image

    ```bash
registry_push \
               --from_image=ubuntu \
               [--GLR] \
               --name=test \
               --reference=1 \
               --rootfs[=./rootfs] \
               /opt/el7/x86_64/*.rpm \
               /etc/myconfig
```

    - will append a new layer to `ubuntu`
    - from `./rootfs`
    - with content `/rootfs/opt/el7/x86_64/*.rpm` and `/etc/myconfig`
    - and push it to Gitlab registry `$CI_REGISTRY_IMAGE/test:1` accesible in CI as:`Gitlab/test:1`

- Transfer Image:

    ```bash
registry_push \
          --from_image=ubuntu \
          --GLR \
          --name=ubuntu \
          --reference=1 \
          --config=myubuntu.yaml
```
    - transfer an image from a docker repository to our Gilab-Registry/project_path
    - with modified docker config as defined in `./myubuntu.yaml`
    - and pushed it to Gitlab registry `$CI_REGISTRY_IMAGE/ubuntu:1` accesible in CI as:`Gitlab/ubuntu:1`

- Transfer Image without history:

    ```bash
registry_push \
          --from_image=Gitlab/ubuntu:1 \
          --image=mynamespace/ubuntu:1 \
          --skip-label
```
    - transfer an image from our Gitlab registry to dockerhub
    - skip history layer, so we keep info regarding the build in the labels from the build phase.

#### 5. autolabel

when building a docker image from a rootfs or archive, oc-runner labels are automaticle added for tracebility:

- For images created from a rootfs or archive:

```yaml
labels:
  io.oc-runner-version: 0.0.33
  io.oc-runner.build.image-digest: sha256:2a61f8abd6250751c4b1dd3384a2bdd8f87e0e60d11c064b8a90e2e552fee2d7
  io.oc-runner.build.image: registry.hub.docker.com/library/centos:7.4.1708
  io.oc-runner.build.image-nick: centos:7.4.1708
  io.oc-runner.build.image-tag: 7.4.1708
  io.oc-runner.build.commit.author: Danny <danny.goossen@gioxa.com>
  io.oc-runner.build.commit.id: e2b011bfbecaa4a6a843cc3014d54ed891344e37
  io.oc-runner.build.commit.message: prepend project dir for workspaces if not start with /
  io.oc-runner.build.commit.ref: 52-change-registry-push-content-to-allow-bash-like-expansion
  io.oc-runner.build.job-url: https://gitlab.gioxa.com/deployctl/oc-runner/-/jobs/49769
  io.oc-runner.build.environment: production
  io.oc-runner.build.source.tree: https://gitlab.gioxa.com/deployctl/oc-runner/tree/e2b011bf
```

- Additional for all(`*`) `registry_push` with a `--from_xxx` :
    
    ```yaml
labels:
  io.oc-runner.build.from-image.tag: latest
  io.oc-runner.build.from-image.digest: sha256:2a61f8abd6250751c4b1dd3384a2bdd8f87e0e60d11c064b8a90e2e552fee2d7
  io.oc-runner.build.from-image.nick-name: centos
  io.oc-runner.build.from-image.docker-repo: registry.hub.docker.com/library/centos
```

**Note** unless `--skip_label` was defined, the `io.oc-runner.build.from-xxxx` is skipped!

- And see [variables](#variables) section for the [org.opencontainer.image](https://github.com/opencontainers/image-spec/blob/master/annotations.md#pre-defined-annotation-keys) label scheme.

## registry_tag_image

tag's an existing image defaults to ImageStream:latest

```bash
registry_tag_image \
  [--image=repo-name] | [ [--name=name] [--GLR | [--ISR]] [--reference=newtag] ]
```

Defaults to:

`reference`: latest

available for use as build image as:

- default (`--ISR`) as :
    `ImageStream[/<name>]:[<reference>]`
- with `--GLR` as:
    `Gitlab[/<name>]:[<reference>]`

and external as:

- with default (`--ISR`) as:
    `<oc-registry>/<oc-namespace>/is-<CI_PROJECT_PATH>[-<name>]:<reference>`
- with `--GLR` as 
    `<CI-REGISTRY-IMAGE>[/<name>]:<reference>`


[top](#oc-runner)

