
## MicroBadger API

Update [Micro Badger](https://microbadger.com/) with latest Tag on [DockerHub](https://hub.docker.com/) for a given image.

```bash
MicroBadger_Update [--allow-fail] --image=repo-name
```

**Note** `--image=namespace/image` need to be a valid docker image `repo-name`:

   - valid:
        - `centos`
        - `library/centos`
        - `register.hub.docker.com/library/centos`

   - tag is ignored, thus follow examples also are valid:
        - `centos:latest`
        - `library/centos:7.4`
        - `register.hub.docker.com/library/centos:7.5`

[top](#oc-runner)

