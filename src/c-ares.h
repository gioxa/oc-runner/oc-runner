//
//  c-ares.h
//  oc-runner
//
//  Created by Danny Goossen on 27/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#ifndef __oc_runner__c_ares__
#define __oc_runner__c_ares__

#include <stdio.h>

char * getip(const char * hostname);
char * gethost(const char * hostip);

#endif /* defined(__oc_runner__c_ares__) */
