/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file oc_api.h
 *  \brief Header api wrapper for openshift
 *  \author Danny Goossen
 *  \date 19/10/17
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#ifndef __oc_runner__oc_api__
#define __oc_runner__oc_api__

#include "common.h"

/**
 \brief openshift api data storage
 */
struct oc_api_data_s{
   char * master_url;
   char * token;
   char * ca_cert_bundle;
   CURL * curl;
   char * headertoken;
   struct curl_slist *headers;
};

/**
 * \brief Clean up oc_api
 *
 */
void oc_api_cleanup(struct oc_api_data_s ** oc_api_data);

/**
 * \brief init oc_api
 *
 */
int oc_api_init(  struct oc_api_data_s ** oc_api_data, const char * ca_cert_bundle, const char * master_url, const char * token);

/**
 * \brief oc_api curl wrapper
 *
 */
int oc_api( struct oc_api_data_s * oc_api_data ,char * methode, cJSON * data_json, cJSON ** result, const char * api, ...);

// higer level api's
/**
 * \brief get uid range and scc
 *
 */
int oc_api_get_restrictions(struct oc_api_data_s * oc_api_data,char * name_space, char * hostname,uid_t * start_uid, uid_t * range, char ** scc, char ** node);

/**
 *  \brief Creates an empty Imagestream map, in case EC2 node, which cannot push a manifest
 *  \param namespace we operate in
 *  \param image_name of the imagestream
 *  \return the Imagestream_map in cjson struct or NULL on failure
 *  \note caller is responsible to free the struct
 */
cJSON* make_imagestream_template( const char * namespace, const char * image_name);

/**
 \brief Creates an imageStream
 \return 1 on success
 */
int create_imagestream(struct oc_api_data_s * oc_api_data,const cJSON * job,const char * oc_image_name);

#endif /* defined(__oc_runner__oc_api__) */
