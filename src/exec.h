
/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */


/*! \file exec.h
 *  \brief Executes a command with or whithout shell
 *  \author Danny Goossen
 *  \date 4/3/17
 *  \copyright (c) 2017 deployctl, Gioxa Ltd.
 *
 */


#ifndef __deployctl__exec__
#define __deployctl__exec__

#include "common.h"

/**
 *  \brief Get Shell from environment or find one.
 *  \return shell pathname e.g. /usr/bin/sh
 *  \note Need edit, should return const char, for now no way to determing if const char or char
 * Finds a shell in the system and returns the path
 */
char * check_shell(void);

/**
 *  \brief initialise the dataexchange structure.
 *  \param parameters global program parameters
 *  \param trace struct for gitlab feedback
 *  \note Need edit, should return const char, for now no way to determing if const char or char
 * Finds a shell in the system and returns the path
 */
void * init_data_exchange(parameter_t * parameters ,struct trace_Struct* trace, Environment * Env);

/**
 *  \brief frees content and struct.
 *  \param data_exchange global program parameters
*/
void clr_data_exchange(void ** data_exchange);

/*! fn int exec_color(void * opaque)
 *
 * \brief Executes a command as per all info in \a opaque.
 *
 *
 * \param opaque dataexchange
 */
int exec_color(void * opaque);

#endif /* defined(__deployctl__exec__) */
