/*! \file deploy-runner.c
 *  \brief functions to setup and check the runners for jobs
 *  \author Created by Danny Goossen on 21/7/17.
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#include "deployd.h"
#include "curl_headers.h"


/*! \fn static cJSON* read_runners_file( const char * dirpath, const char * filename)
    \param dirpath directory
    \param filename file to read
    \return the cJSON runners settings, NULL on errors including if a\ dirpath or \a filename is NULL
    \note caller is responsible for freeing
 */
static cJSON* read_runners_file( const char * dirpath, const char * filename);


/*! \fn static int progress_callback(void *clientp,   curl_off_t dltotal,   curl_off_t dlnow,   curl_off_t ultotal,   curl_off_t ulnow)
 *  \brief callback function for \b get_job to interupt long polling for quick gracefull exit
 *  \note used here to interupt the long polling
 *  \param clientp the callback data
 */
static int progress_callback(void *clientp, curl_off_t dltotal,   curl_off_t dlnow,   curl_off_t ultotal,   curl_off_t ulnow);

/*! \fn cJSON * get_job(cJSON * runner,cJSON * info, void * stop_struct, CURL * curl)
 */
static cJSON * get_job(cJSON * runner,cJSON * info, void * stop_struct, CURL * curl);


/*! \fn void * check_runner_thread(void * data)
 */
void * check_runner_thread(void * data);



struct check_runner_data_s{
   cJSON * runner;
   cJSON * job;
   cJSON * info;
   parameter_t * parameters;
   int stop;
   int stop_ack;
};






int runners(parameter_t * parameters)
{
   char * print_it=NULL;
   pthread_mutex_t nginx_lock;
   pthread_mutex_t repo_lock;
   struct check_runner_data_s check_runner_data[8];
   pthread_t thread_id[8];
   //printf("deploy-runner!\n");
   while (!parameters->exitsignal)
   {
      cJSON * runners=read_runners_file(parameters->testprefix,parameters->config_file);
      if (runners==NULL)
      {
        error("runners: No runners, exit\n");
        return -1;
      }
      alert("runners: Got %d runners to handle\n",cJSON_GetArraySize(runners));

      if (cJSON_GetArraySize(runners)>8)
      {
         error("runners: Too Many Runners, Max 8 runners, exit\n");
         return -1;
      }
      cJSON * pos=NULL;
      int count=0;
      cJSON_ArrayForEach(pos,runners)
      {
         count++;
         cJSON * tmp=cJSON_GetObjectItem(pos, "interval");
         if (tmp)
         {
            if (!cJSON_IsNumber(tmp))
            {
               int interval=(int)strtol(tmp->valuestring, NULL, 10);
               cJSON_ReplaceItemInObject(pos, "interval", cJSON_CreateNumber(interval));
            }
         }
         else
			 cJSON_AddItemToObject(pos, "interval", cJSON_CreateNumber(1));
         tmp=cJSON_GetObjectItem(pos, "errorcount");
         if (!tmp) cJSON_AddItemToObject(pos, "errorcount", cJSON_CreateNumber(0));
         tmp=cJSON_GetObjectItem(pos, "id");
         if (!tmp) cJSON_AddItemToObject(pos, "id", cJSON_CreateNumber(count));
      }
      cJSON * info=cJSON_CreateObject();
      
      cJSON_AddStringToObject(info, "name", "oc-runner");
      cJSON_AddStringToObject(info, "version", PACKAGE_VERSION);
      cJSON_AddStringToObject(info, "revision", "1");
      cJSON_AddStringToObject(info, "platform", "Linux");
      cJSON_AddStringToObject(info, "architecture", "x86_64");
      cJSON * tmp=cJSON_GetObjectItem(pos, "build_storage");
	   if (tmp)
	   {
		   if (cJSON_IsNumber(tmp))
		   {
			   parameters->build_storage=tmp->valueint;
		   }
		   else if (cJSON_IsTrue(tmp))
		       parameters->build_storage=1;
		   else if (cJSON_IsFalse(tmp))
			   parameters->build_storage=0;
			else
			  parameters->build_storage=1;
	   }
	   else
		   parameters->build_storage=0;
		   
      //cJSON_AddStringToObject(info, "User-Agent", "gitlab-runner 10.0.0-rc.1");
      // fmt.Sprintf("%s %s (%s; %s; %s/%s)", v.Name, v.Version, v.Branch, v.GOVersion, v.OS, v.Architecture)


      alert("runners: Ready to start polling %d\n",cJSON_GetArraySize(runners));
      if (getdebug()) // TODO make this nicer
      {
         print_it =cJSON_PrintBuffered(runners, 1, 1);
         debug("%s\n",print_it);
         if (print_it) free(print_it);
      }

      cJSON * runner=NULL;
      int i=0;
      pthread_mutex_init(&nginx_lock, NULL);
      pthread_mutex_init(&repo_lock, NULL);
      cJSON_ArrayForEach(runner,runners)
      {
         check_runner_data[i].stop=0;
         check_runner_data[i].stop_ack=0;
         check_runner_data[i].info=info;
         check_runner_data[i].job=NULL;
         check_runner_data[i].parameters=parameters;

         check_runner_data[i].runner=runner;
		  if (cJSON_GetArraySize(runners)>1)
         	pthread_create(&thread_id[i], NULL,check_runner_thread, &check_runner_data[i]);
		  else
			check_runner_thread(&check_runner_data[i]);
         usleep(250000); // sleep 250ms to avoid startup race.
         i++;
      }
      while (!parameters->hubsignal && !parameters->exitsignal) usleep(1000000);
      debug("runners: breakout while\n");
      int j;
      int n_runners=cJSON_GetArraySize(runners);
      for (j=0;j<n_runners;j++) check_runner_data[j].stop=1;
      alert("runners: Waiting for runners to stop...\n");
      // wait for runners to stop
      int all_stop=0;
      int stop_timout=60*10; // 10 minute wait for deployments to stop
      while (!all_stop && stop_timout)
      {
         int stop_nack=0;
         for (j=0;j<n_runners;j++) if (check_runner_data[j].job) stop_nack= stop_nack + (int)~(check_runner_data[j].stop_ack);
         all_stop=(stop_nack==0);
         usleep(1000000);
         stop_timout--;
      }
      if (!stop_timout)
      {  // not all stopped, cancel the ones that did not stop
         alert("runners: Not All runners down, Cancelling runner threads\n");
         for (j=0;j<n_runners;j++)
         {
            if (!check_runner_data[j].stop_ack) pthread_cancel(thread_id[j]);
         }
      }
      // and join all together.
      alert("runners: joining runner threads\n");
      for (j=0;j<n_runners;j++) pthread_join(thread_id[j], NULL);
      alert("runners: All runners down...\n");
      parameters->hubsignal=0;
	   
      cJSON_Delete(runners);
      runners=NULL;
   }
   pthread_mutex_destroy(&nginx_lock);
   pthread_mutex_destroy(&repo_lock);

   return 0;
}

// local helpers
// TODO use read yaml file!!!
static cJSON* read_runners_file( const char * dirpath, const char * filename)
{
    cJSON * runners_json=NULL;
    
    if (!dirpath || !filename) return NULL;
    //printf("%s/%s\n",dirpath,filename);
    char filepath[1024];
   if (dirpath && strlen(dirpath) >0)
    snprintf(filepath,1024, "%s/%s",dirpath,filename);
   else
      snprintf(filepath,1024, "%s",filename);
    FILE *f = fopen(filepath, "r");
    if (f == NULL)
    {
        error("process_runners_file: %s\n",strerror(errno));
        return NULL;
    }
    dynbuffer* mem=dynbuffer_init();
    char buf[1024];
    bzero(buf, 1024);
    while( fgets( buf, 1024, f ) != NULL )
    {
        dynbuffer_write(buf, mem );
        bzero(buf, 1024);
    }
    fclose(f);
    //fprintf(stderr,"readfile: >\n%s\n",mem.memory);
    ;
	if (dynbuffer_len(mem)>0)
        runners_json= yaml_sting_2_cJSON (NULL , dynbuffer_get(mem));
    dynbuffer_clear(&mem);
    // validate json
    if (!runners_json)
    {
        error("Failed to get runners.yaml %s \n",filepath);
        return NULL;
    }
    else
    {
        alert(" got runners\n");
        // TODO for each, check if token and url
        // if present, cleanup url, not allowed to have / at the end!!!
        // otherwise no long polling
        /*
         if (check_presence(runners_json,
         (char *[])
         {"token","ci_url",NULL},
         NULL) == 0 )
         {
         
         
         }
         else
         {
         cJSON_Delete(runners_json);
         return NULL;
         }
         */
        return runners_json;
    }
}
static int progress_callback(void *clientp,  __attribute__((unused)) curl_off_t dltotal, __attribute__((unused))  curl_off_t dlnow,  __attribute__((unused)) curl_off_t ultotal, __attribute__((unused))  curl_off_t ulnow)
{
    stop_struct_t * stop_struct_data=(stop_struct_t *)clientp;
    if (*(stop_struct_data->exit) || *(stop_struct_data->reload)) return 1;
    
    return 0;
}


static cJSON * get_job(cJSON * runner,cJSON * info, void * stop_struct, CURL * curl)
{
    //curl --request POST "https://gitlab.example.com/api/v4/jobs/request" --form "token=t0k3n"
    //Responses:
    
    //Status	Data	Description
    //201	yes	When a build is scheduled for a runner
    //204	no	When no builds are scheduled for a runner (for GitLab Runner >= v1.3.0)
    //403	no	When invalid token is used or no token is sent

    const char * token=cJSON_get_key(runner, "token");
    const char * domain=cJSON_get_key(runner, "ci_url");
    if (!domain || !token) return NULL;
    int * error_count=&(cJSON_GetObjectItem(runner, "errorcount")->valueint);
    int runner_id=cJSON_GetObjectItem(runner, "id")->valueint;
    cJSON * result=NULL;
    CURLcode res;
    if(curl)
    {
        //build request
        char request[1024];
        sprintf(request, "%s/api/v4/jobs/request",domain);
        
        dynbuffer* chunk=dynbuffer_init();
        struct curl_headers_struct headers_r;
		
        curl_headers_init(&headers_r);
        // add JSON header
        struct curl_slist *headers = NULL;
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "accept: application/json");
        //headers = curl_slist_append(headers, "User-Agent: gitlab-runner 10.0.0-RC1");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        
        // Enable intruption for gracefull shutdown
        curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, &progress_callback);
        curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, stop_struct);
        /* enable progress meter */
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
        
        curl_easy_setopt(curl, CURLOPT_URL, (char*)request);
        
        cJSON * params=cJSON_CreateObject();
        cJSON_AddStringToObject(params, "token", token);
        
        
        const char * tmp=cJSON_get_key(runner, "last_update");
        if (tmp) cJSON_AddStringToObject(params, "last_update", tmp);
        
        cJSON_AddItemToObject(params, "info", info);
        char * params_str=cJSON_PrintUnformatted(params);
        cJSON_DetachItemFromObject(params, "info");
        cJSON_Delete(params);
        
        //debug("runner %2d: get_job_sent_params: %s\n",runner_id,params_str);
        /* complete within 70 seconds (need 50 for long polling)*/
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 70L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, params_str);
        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
        /* send all data to this function  */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        
        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, chunk);
        
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION,curl_headers_cb);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, &headers_r);
        res = curl_easy_perform(curl);
        curl_slist_free_all(headers);
        
        long http_code = 0;
        curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
        const char * x_last_update=cJSON_get_key(headers_r.header, "X-GitLab-Last-Update");
        if (x_last_update)
        {
            if (cJSON_get_key(runner, "last_update"))
                cJSON_ReplaceItemInObject(runner, "last_update", cJSON_CreateString(x_last_update));
            else
                cJSON_AddItemToObject(runner, "last_update", cJSON_CreateString(x_last_update));
        }
        
        if (validate_key(headers_r.header, "Gitlab-Ci-Builds-Polling","yes")==0  )
        {
            alert("runner %2d: Long Polling (%s) ...\n",runner_id, domain);
            if (cJSON_get_key(runner, "Long Polling"))
                cJSON_ReplaceItemInObject(runner, "Long Polling", cJSON_CreateString("yes"));
            else
                cJSON_AddItemToObject(runner,"Long Polling", cJSON_CreateString("yes"));
            
        }
        else
        {
            if (cJSON_get_key(runner, "Long Polling"))
                cJSON_ReplaceItemInObject(runner, "Long Polling", cJSON_CreateString("no"));
            else
                cJSON_AddItemToObject(runner,"Long Polling", cJSON_CreateString("no"));
        }
        if (http_code == 201 && res != CURLE_ABORTED_BY_CALLBACK)
        {
            debug("runner %2d: got a job\n",runner_id);
            // printf("%s\n",dynbuffer_get(chunk));
            result= cJSON_Parse(dynbuffer_get(chunk));
            if (*error_count) (*error_count)=0;
        }
        else if ((http_code == 204 )&& res != CURLE_ABORTED_BY_CALLBACK)
        {
            debug("runner %2d: no job scheduled : %s\n",runner_id,dynbuffer_get(chunk));
            if (*error_count) (*error_count)=0;
        }
        else
        {
            if ((*error_count)<10) (*error_count)++; else (*error_count)=10;
            if (res)
                error( "runner %2d: (%2d) getjob failed %s\n",runner_id,(*error_count),curl_easy_strerror(res));
            else
                error("runner %2d: (%2d) X-Request-Id:%s =>getjob failed HTTP_code: %ld >> %s\n",runner_id,(*error_count),cJSON_get_key(headers_r.header, "X-Request-Id") ,http_code,dynbuffer_get(chunk));
        }
        dynbuffer_clear(&chunk);
        curl_headers_free(&headers_r);
        free(params_str);
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, NULL);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, NULL);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);
        curl_easy_setopt(curl, CURLOPT_HTTPPOST, NULL);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, NULL);
    }
    
    return result;
}

void * check_runner_thread(void * data)
{
    
    CURL * curl;
    curl = curl_easy_init();
    
    int new_interval;
    int interval;
    struct timeval now_time, last_time;
    
    int * nextcheck=NULL;
    int runner_id;
    int error_count=0;
    int old_error=0;
    cJSON * job=NULL;
    cJSON * runner=((struct check_runner_data_s *)data)->runner;
    cJSON * info=((struct check_runner_data_s *)data)->info;
    parameter_t * parameters=((struct check_runner_data_s *)data)->parameters;
    stop_struct_t stop_struct_data;
    stop_struct_data.exit=&parameters->exitsignal;
    stop_struct_data.reload=&parameters->hubsignal;
    
    alert("check with ca: %s\n",parameters->ca_bundle);
    curl_easy_setopt(curl, CURLOPT_CAINFO, parameters->ca_bundle);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, PACKAGE_STRING); 
    // init next check
    gettimeofday(&last_time, NULL);
    cJSON_AddItemToObject(runner, "nextcheck", cJSON_CreateNumber((int)last_time.tv_sec ));
    runner_id=cJSON_GetObjectItem(runner, "id")->valueint;
    interval=cJSON_GetObjectItem(runner, "interval")->valueint;
    nextcheck=&cJSON_GetObjectItem(runner, "nextcheck")->valueint;
    while (!((struct check_runner_data_s *)data)->stop && !parameters->exitsignal && !parameters->hubsignal) {
        gettimeofday(&now_time, NULL);
        // get next interval
        
        if ((int)now_time.tv_sec >= *nextcheck  )
        {
            old_error=error_count;
            //setdebug();
            job=get_job(runner,info, &stop_struct_data, curl);
            // get error_count
            error_count=cJSON_GetObjectItem(runner, "errorcount")->valueint;
            
            if (job)
            {
				alert("\n\n-------------------------------------------\n\nlocal exec=%d\n",parameters->shell_exec);
				cJSON_AddStringToObject(job, "ci_url", cJSON_get_key(runner, "ci_url"));
				
                // spin off thread
				if (parameters->shell_exec==0)
					do_job_oc(runner, job,parameters);
				else
					do_local_shell(cJSON_Duplicate(job,1),parameters );
                cJSON_Delete(job);
                job=NULL;
                
            }
            // Calculate new interval and update next_check
            // see issue #61:
            //if long polling, set interval to 0 else if interval =0, set it to a minimun of 3
            if (cJSON_get_key(runner, "Long Polling"))
            {
                if (validate_key(runner, "Long Polling", "yes")==0)
                    new_interval=error_count *6;
                else
                {
                    if (interval ==0)
                    { // if not long polling and interval is 0, set it to 3 seconds + xxx according to errorcount
                        new_interval=3 + error_count *6;
                    }
                    else
                        new_interval=interval + error_count *6;
                }
            }
            else
            {  // not long polling and interval is 0, set it to 3 seconds
                if (interval ==0)
                    new_interval=3 + error_count *6;
                else
                    new_interval=interval + error_count *6;
            }
            
            if (new_interval>60) new_interval=60; // max 60 seconds
            
            // update next check
            *nextcheck=(int)now_time.tv_sec+new_interval;
            // log info on error count get new job
            if (old_error!=error_count) alert("runner %2d: new interval %d (error_count=%d)\n",runner_id,new_interval,error_count);
            
            
        }
        else
            usleep(1000000); // it was not time yet, so sleep a second.
    }
    alert("runner %2d: exit, stop received\n",runner_id);
    ((struct check_runner_data_s *)data)->stop_ack=1;
    if (curl) curl_easy_cleanup(curl);
    return NULL;
}


