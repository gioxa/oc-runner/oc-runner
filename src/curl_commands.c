/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/* \file curl_commands.c
 *  \brief custom oc-runner curl commands
 *  \author Created by Danny Goossen on 18/07/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 */

#include "curl_commands.h"
#include <curl/curl.h>
#include "deployd.h"
#include "dyn_buffer.h"
#include "dyn_trace.h"
#include "error.h"
#include "simple_http.h"

/**
 \brief typedef parameter structure, \b  curl_parameter_s
 */
typedef  struct curl_parameter_s curl_parameter_t;

/**
 \brief parameter structure, general sub-program settings
 Parameter settings for the curl API sub programs
 */
struct curl_parameter_s
{
	u_int8_t verbose; /**< verbose output */
	u_int8_t quiet;   /**< no output */
	u_int8_t allow_fail; /**< allow failure */
};




/**
 \brief internal oc-runner curl_post command
 */
int process_options_curl(size_t argc, char * const *argv, curl_parameter_t * parameters);

/**
 \brief internal free parameters
 */
void clear_curl_parameters(curl_parameter_t **parameter);



int curl_post(__attribute__((unused))const cJSON * job,struct trace_Struct * trace,size_t argc,char * const *argv)
{
	
	int res=-1;

	curl_parameter_t * parameters=calloc(1, sizeof(struct curl_parameter_s));
	
	int argscount=0;
	
	if ((argscount=process_options_curl(argc, argv,parameters)) < 1 || (argc-argscount)>1)
	{
		error(" *** ERRORin command line options, exit \n");
		Write_dyn_trace(trace, red, "\nError command line options\n");
		update_details(trace);
		return res;
	}
	CURL *curl = simple_http_init(DEFAULT_CA_BUNDLE, easy_http_follow_location, easy_http_quiet, easy_http_SSL_VERIFYPEERL, PACKAGE_STRING);
	if(curl)
	{
		char *result=NULL;
		int count=0;
		do{
			if (count){
				print_api_error(trace, res);
				usleep(2000000LL * count*count);
			}
			count++;
			res= simple_httppost_v(curl,&result , NULL, "%s",argv[argscount]);
		} while (((res>=500 && res<600) || res<0) && count<5);

		if (res >= 200 && res<300)
		{
			Write_dyn_trace(trace, green, "\n\tResponse: ");
			Write_dyn_trace(trace, none, "%s\n",result);
			
		}
		else
		{
			print_api_error(trace, res);
			if (!res)
			{
				Write_dyn_trace(trace, red, "\n\tResponse: ");
				Write_dyn_trace(trace, none, "%s\n",result);
			}
		}
		if (result) free(result);
		result=NULL;
		simple_http_clean(&curl);
	}
	else
		Write_dyn_trace(trace, red, "System: failed to initialize curl\n");
	if(res && parameters->allow_fail)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		res=0;
	}
	clear_curl_parameters(&parameters);
	update_details(trace);
	return res;
}


// helper functions

void clear_curl_parameters(curl_parameter_t **parameter)
{
	if (parameter && *parameter)
	{
		//	if((*parameter)->rootfs!=NULL) free((*parameter)->rootfs);
		
		free(*parameter);
		(*parameter)=NULL;
	}
	return;
}

int process_options_curl(size_t argc, char * const *argv, curl_parameter_t * parameters)
{
	
	struct option long_options[] = {
		
		{ "verbose",       0, NULL, 'v' },
		{ "quiet",         0, NULL, 'q' },
		{ "allow-fail",    0, NULL, 'f' },
		{ "allow_fail",    0, NULL, 'f' },
		{ "allow-failure", 0, NULL, 'f' },
		{ "allow_failure", 0, NULL, 'f' },
		{ NULL,            0, NULL,  0  }
	};
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameters->quiet)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameters->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameters->verbose)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
						parameters->quiet = 1;
				}
					break;
				case 'f': {
					parameters->allow_fail=1;
				}
					break;

					
					/* otherwise    : display usage information */
				default:
					return -1;
					break;
			}
		}
	}
	return optind;
}
