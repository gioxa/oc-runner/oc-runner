/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file yalm2cjson.h
 * \brief header file for Parse yaml and json to a cJSON object
 * \author Created by Danny Goossen on 10/5/2017.
 * \copyright 2018, Danny Goossen. MIT License
 *
 */

#ifndef __deployctl__yaml2cjson__
#define __deployctl__yaml2cjson__

#include <stdio.h>

/**
 \brief parses a yaml string to an cJSON object
 \param opaque reserved for testing
 \param yaml string to parse
 \return a cJSON array of the yaml documents
 */
cJSON * yaml_sting_2_cJSON (void * opaque ,const char * yaml);

/**
 \brief parses a yaml string to an cJSON object
 \param opaque reserved for testing
 \param env_vars cJSON object with evironment variables, to substitude double quoted key:values
 \param yaml string to parse
 \return a cJSON array of the yaml documents
 */
cJSON * yaml_sting_2_cJSON_env (void * opaque ,cJSON * env_vars,const char * yaml);

/**
 \brief parses a yaml file to an cJSON object
 \param opaque reserved for testing
 \param fileName the file to parse in format of printf()
 \return a cJSON array of the yaml documents
 \note caller is responsible for freeing the cJSON object
 */
cJSON * yaml_file_2_cJSON ( void * opaque ,const char * fileName, ...);

/**
 \brief parses a yaml string to an cJSON object
 \param opaque reserved for testing
 \param env_vars cJSON object with evironment variables, to substitude double quoted key:values
 \param fileName file to import and parse, printf like args
 \return a cJSON array of the yaml documents
 */
cJSON * yaml_file_2_cJSON_env ( __attribute__((unused))void * opaque ,cJSON * env_vars,const char * fileName, ...);

/**
 \brief parses a yaml file to an cJSON object
 \param errormsg pointer to errro string if not NULL;
 \param fileName the file to parse in format of printf()
 \return a cJSON object of first document of the yaml documents
 \note caller is responsible for freeing the cJSON object
 */
cJSON * yaml_file_doc0_2_cJSON ( char ** errormsg ,const char * fileName, ...);


#endif /* defined(__deployctl__yaml2cjson__) */
