/*
 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

/*! \file exec.c
    \brief The Job executer
    \author Danny Goossen, Gioxa Ltd
    \date 4/3/17
*/


#include "deployd.h"
#include "wordexp_var.h"
#include "Environment.h"

/*! \def PIPE_READ
 \brief defines the read end of a pipe
 */
#define PIPE_READ 0

/*! \def PIPE_WRITE
 \brief defines the write end of a pipe
 */
#define PIPE_WRITE 1

/**
 \brief structure for the pseudoterminal executer
 */
struct script_control {
	
	word_exp_var_t *result; /**< wordexp result in case no shell */
   const char *shell;		       /**< shell to be used if any */
   const char *shellcommand;		 /**< command to be executed */
   Environment * env; /**< Environment */
   int master;		                /**< pseudoterminal master file descriptor */
   int slave;		                /**< pseudoterminal slave file descriptor */
   pid_t child;
   #if !HAVE_LIBUTIL
      char *line;		             /**< terminal line (for Mac/BSD) */
   #endif
   int env_pipe;                  /**< pipe file descripter to transfer environment */
   int childstatus;	             /**< child process exit value */
   int timeout;                  /**< stale timeout on receiving on stdout/stderr */
	const char * commandpath; /**< excact path how to start executor */
   unsigned int
die:1;			                   /**< terminate program */
};


/*! \fn static int socket_nonblocking (int sock)
 *  \brief helper function to make socket non blocking
 *  \param sock the socket
 *  \return -1 on failure,
 *  \note on failure, caller is responsible to close socket
 */
static int socket_nonblocking (int sock)
{
   int flags;

   if(sock != INVALID_SOCKET)
   {
      flags = fcntl (sock, F_GETFL, 0);
      return fcntl (sock, F_SETFL, flags | O_NONBLOCK | O_CLOEXEC | SO_KEEPALIVE);//| O_NDELAY);
   }
   return -1;

}

/**
 *  \brief helper function to set option close on exec
 *  \param sock the socket
 *  \return -1 on failure,
 *  \note on failure, caller is responsible to close socket
 */
static int socket_O_CLOEXEC (int sock)
{
   int flags;

   if(sock != INVALID_SOCKET)
   {
      flags = fcntl (sock, F_GETFL, 0);
      return fcntl (sock, F_SETFL, flags | O_CLOEXEC);
   }
   return -1;

}


void * init_data_exchange(parameter_t * parameters ,struct trace_Struct* trace, Environment * Env )
{
   // set structure
   data_exchange_t * temp=calloc(1, sizeof(data_exchange_t));
   temp->parameters=parameters;
   temp->env=Env;
   temp->timeout=parameters->timeout;
   temp->trace=trace;
	temp->commandpath=parameters->commandpath;
   return (temp);
}

void clr_data_exchange(void ** data_exchange)
{
//	if (((data_exchange_t*)(*data_exchange))->env) Environment_clear(&((data_exchange_t*)(*data_exchange))->env);
   free(*data_exchange);
   *data_exchange=NULL;
}

char * check_shell(void)
{

   char * shell=NULL;
   const char * shname=NULL;
   
   shell = getenv("SHELL");
   debug("environment shell: %s\n",shell);
   if (shell && access(shell, X_OK) == 0 )
   {
      shname = strrchr(shell, '/');
      if (shname)
         shname++;
      else
         shname = shell;
      debug("shellname %s\n",shname);
      if (strcmp(shname,"nologin")==0)
      {
         shell = NULL;
      }
   }
   else
      shell=NULL;
   
   if (!shell)
   {
      debug("Finding shell..\n");
      if (access("/bin/bash", X_OK) == 0 )
      {
         shell=strdup("/bin/bash");
      }
      else if (access("/bin/sh", X_OK) == 0 )
      {
         shell=strdup("/bin/sh");
      }
      else if (access("/usr/bin/bash", X_OK) == 0 )
      {
         shell=strdup("/usr/bin/bash");
      }
      else if (access("/usr/bin/sh", X_OK) == 0 )
      {
         shell=strdup("/usr/bin/sh");
      }
   }
   debug("SHELL: %s\n",shell);
   setenv("SHELL",shell,0);
   setenv("PATH", "/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/local/sbin", 0);
   return shell;
}

// New PTY concept

static void parse_env_string(char **buff,struct script_control *ctl )
{	
   if (*buff && strlen(*buff)>0)
   {
	   cJSON * newenv=cJSON_Parse(*buff);
	   
	   if (newenv && cJSON_GetArraySize(newenv)>0 )
	   {
		   cJSON * pos=NULL;
		   cJSON_ArrayForEach(pos, newenv)
		   {
			   if (cJSON_IsString(pos))
			   {
				  //printf("-->%s=%s\n",pos->string,pos->valuestring);
				   
				   const char * value=pos->valuestring;
				   const char * vs=pos->string;
				   if (vs )
				   {
					   int overwrite=1;
					   if (
						   strcmp(vs,"CI_PROJECT_PATH"  )==0 ||
						   strcmp(vs,"CI_PROJECT_URL"   )==0 ||
						   strcmp(vs,"GITLAB_USER_NAME" )==0 ||
						   strcmp(vs,"GITLAB_USER_EMAIL")==0 ||
						   strcmp(vs,"GITLAB_USER_ID"   )==0 ||
						   strcmp(vs,"GITLAB_USER_LOGIN")==0 ||
						   strcmp(vs,"CI_REPOSITORY_URL")==0 ||
						   strcmp(vs,"CI_REGISTRY"      )==0 ||
						   strcmp(vs,"CI_REGISTRY_IMAGE")==0
						   )
						   overwrite=0;
					   Environment_setenv(ctl->env, vs, value,overwrite);
				   }
			   }
		   }
	   }
	   if (newenv) cJSON_Delete(newenv);
   }
   if(*buff)
   {
      free(*buff);
      *buff=NULL;
   }
}



static void getmaster(struct script_control *ctl)
{
  ctl->master=-1;
#if defined(HAVE_LIBUTIL) && defined(HAVE_PTY_H)
   int rc=-1;
   rc = openpty(&ctl->master, &ctl->slave, NULL, NULL, NULL);

   if (rc < 0) {
      error("openpty failed");
      kill(0, SIGTERM);
   }
#else
   char *pty, *bank, *cp;

   pty = &ctl->line[strlen("/dev/ptyp")];
   for (bank = "pqrs"; *bank; bank++) {
      int ptypos=(int)strlen("/dev/pty");
      ctl->line[ptypos] = *bank;
      *pty = '0';
      if (access(ctl->line, F_OK) != 0)
         break;
      for (cp = "0123456789abcdef"; *cp; cp++) {
         *pty = *cp;
         ctl->master = open(ctl->line, O_RDWR | O_CLOEXEC);
         if (ctl->master >= 0) {
            char *tp = &ctl->line[strlen("/dev/")];
            int ok;

            /* verify slave side is usable */
            *tp = 't';
            ok = access(ctl->line, R_OK | W_OK) == 0;
            *tp = 'p';
            if (ok) return;
            close(ctl->master);
            ctl->master = -1;
         }
      }
   }
   ctl->master = -1;
   error("out of pty's\n");

   kill(0, SIGTERM);
#endif // have libutil
}

static void getslave(struct script_control *ctl)
{
#ifndef HAVE_LIBUTIL
   ctl->line[strlen("/dev/")] = 't';
   ctl->slave = open(ctl->line, O_RDWR | O_CLOEXEC);
   if (ctl->slave < 0) {
      printf(("cannot open %s"), ctl->line);
      kill(0, SIGTERM);
   }
	
#endif
   setsid();
	//make sure slave is blocking
		// ioctl(ctl->slave, TIOCSCTTY, 0);
}

static void do_shell(struct script_control *ctl)
{
	const char *shname=NULL;
  printf("start child\n");
	getslave(ctl);
	if (ctl->slave==-1)
	{
		printf("\n"ANSI_RED"No slave terminal"ANSI_RESET"\n");
		exit (-5);
	}
	/* close things irrelevant for this process */
	close(ctl->master);

	int fd = open("/dev/null", O_WRONLY | O_CLOEXEC);
	if (dup2( fd , STDIN_FILENO) == -1) {sock_error("redirecting stdin");exit(3);}
	
	dup2(ctl->slave, STDOUT_FILENO);
	dup2(ctl->slave, STDERR_FILENO);
	close(ctl->slave);

	ctl->master = -1;

	/* Let's restore the default behavior for signals
	*/
	signal(SIGHUP,SIG_DFL);
	signal(SIGTERM,SIG_DFL);
	signal(SIGINT,SIG_DFL);
	signal(SIGSTOP,SIG_DFL);
	signal(SIGQUIT,SIG_DFL);
	signal(SIGSEGV,SIG_DFL);
	int varoffs=0;
	
	// GO
	int res=chdir(Environment_getenv(ctl->env, "PWD"));
	if (res)
	{
		printf("\n"ANSI_RED"%s"ANSI_RESET,strerror(errno));
		exit (-1);
	}
	cJSON * env_vars=NULL;
	if (ctl->env->type==environment_type_cJSON) env_vars=(cJSON*)ctl->env->environment;
	//setdebug();
	//print_json(env_vars);
	if (env_vars && cJSON_GetArraySize(env_vars)>0 )
	{
		cJSON * pos=NULL;
		cJSON_ArrayForEach(pos, env_vars)
		{
			if (cJSON_IsString(pos))
			{
				//printf("-->%s=%s\n",pos->string,pos->valuestring);
				setenv(pos->string,pos->valuestring,1);
			}
			//else debug("skipped non string %s\n",pos->string);
		}
	}
   if (ctl->shell)
   {
      // todo need aloocation of memory in line with size of shell command
	   size_t len_command=strlen(ctl->shellcommand) + 150;
	   char * bash_command=calloc(1, len_command);
	   // need to check if not end in ;
	   if (strstr(ctl->shellcommand,"export ")!=NULL)
		   snprintf(bash_command, len_command,"%s\n%s --pipe_env=%d --exit=$?",ctl->shellcommand,ctl->commandpath,ctl->env_pipe);
	   else if (strstr(ctl->shellcommand,"cd")!=NULL || strstr(ctl->shellcommand,"pushd")!=NULL || strstr(ctl->shellcommand,"popd")!=NULL)
		   snprintf(bash_command, len_command,"%s\n%s --pipe_pwd=%d --exit=$?",ctl->shellcommand,ctl->commandpath,ctl->env_pipe);
	   else
		   snprintf(bash_command, len_command,"%s",ctl->shellcommand);
      shname = strrchr(ctl->shell, '/');
      if (shname)
         shname++;
      else
         shname = ctl->shell;
         //printf("%s %s -c %s %s\n",ANSI_CYAN,shname,bash_command,ANSI_RESET);
         if(execl(ctl->shell, shname, "-c", bash_command, NULL)!=0)
         {
            printf("\n"ANSI_RED"%s"ANSI_RESET,strerror(errno));
         }
   }
   else if (ctl->result!=NULL)
   {
      // process first local variables declarations
      while (ctl->result->argc >varoffs && strchr(ctl->result->argv[varoffs],'=')!=NULL)
      {
          char * endstr=NULL;
          char * par=strtok_r(ctl->result->argv[varoffs], "=", &endstr);
          if (par && strlen(par)>0)
          {
             char * val=strtok_r(NULL, "=", &endstr);
             setenv(par, val, 1);
          }
          varoffs++;
      }
      setenv("_", ctl->result->argv[varoffs], 1);
      word_exp_var_free(ctl->result);
      //and reevaluate
	   wordexp_t we;
      switch (wordexp (ctl->shellcommand,&we, WRDE_SHOWERR))
      {
            case 0:			/* Successful.  */
               break;
            case WRDE_BADCHAR:
               printf("\n%sIllegal occurrence of newline or one of |, &, ;, <, >, (, ),{, } in script command\n%s",ANSI_RED,ANSI_RESET );
               exit(EXIT_FAILURE);
            case WRDE_SYNTAX:
               printf("%s\nShell syntax error, such as unbalanced parentheses or unmatched quotes.\n%s",ANSI_RED,ANSI_RESET);
               exit(EXIT_FAILURE);
            case WRDE_NOSPACE:
               /* If the error was WRDE_NOSPACE,
                then perhaps part of the result was allocated.  */
               printf("%s\nOut of space expanding command.\n%s",ANSI_RED,ANSI_RESET);
               exit(EXIT_FAILURE);
            default:                    /* Some other error.  */
               printf("%s\nExec Error expanding command\n%s",ANSI_RED,ANSI_RESET );
               exit(EXIT_FAILURE);
      }
       //
      // Let do it if something left to do
      if (we.we_wordc >varoffs && we.we_wordv[varoffs])
      {
         // add some more commands ???
         printf("%sNo Shell %s\n",ANSI_CYAN,ANSI_RESET);
         
         if (execvp(we.we_wordv[varoffs], &we.we_wordv[varoffs])<0)
         {
            printf("\n%sError: execvp: %s%s",ANSI_RED,strerror(errno),ANSI_RESET);
         }
      }
    }
   else
   {
      printf(ANSI_RED"no shell and wordexp is NULL\n"ANSI_RESET);
   }
   //printf(ANSI_BOLD_RED"failed to execute %s\n"ANSI_RESET, ctl->shellcommand);
   //kill(0, SIGTERM);
   exit(EXIT_FAILURE);
}

static int do_io(struct script_control *ctl, struct trace_Struct * trace)
{
   // parent wait's and return result
   // close unused file descriptors, these are for child only
   size_t buf_size=0x80000;
   char * output_buf=calloc(1,buf_size);
   fd_set rds;
   int s_ret=0;
   int s_err=0;
   ssize_t o_read=0;
   int o_r_error=0;
   struct timeval tv;
   tv.tv_sec = ctl->timeout;
   tv.tv_usec = 0;
   int timeout=0;
   int exitcode=0;
   int maxfds=ctl->master;
   socket_nonblocking(ctl->master);
   if (ctl->env_pipe > ctl->master) maxfds=ctl->env_pipe;
   socket_nonblocking(ctl->env_pipe);
   //init dynbuff for env
  dynbuffer * mem=dynbuffer_init();
   struct timeval tv_start;
   struct timeval tv_now;
   struct timeval tv_last_update;
   gettimeofday(&tv_start,NULL);
   gettimeofday(&tv_last_update,NULL);
	// make sure we got immediate output
	tv_last_update.tv_sec=tv_last_update.tv_sec-21;
   int tracedata=0;
   do
   {
      FD_ZERO( &rds );
      FD_SET( ctl->master,  &rds );
      FD_SET( ctl->env_pipe,&rds );
      
     
      tv.tv_sec = 20;
      s_ret=select(maxfds+1, &rds, NULL, NULL, &tv);
      if (s_ret==SOCKET_ERROR) s_err=errno;
      else if (s_ret>0)
      {
        gettimeofday(&tv_start,NULL);
        if (FD_ISSET(ctl->master, &rds ))
        {
			   memset(output_buf,0,buf_size);
			   o_read=read(ctl->master, output_buf,buf_size-1);
			   if (o_read==0)
			   {
				   debug("trace: EOF\n");
				   ;if (!ctl->shell) break;
			   }
			   else if (o_read >0)
			   {
				  debug ("trace: Received %d bytes\n",(int)o_read);
				  Write_dynamic_trace_n (output_buf,o_read,trace);
				  gettimeofday(&tv_now,NULL);
				  if ((int)(tv_now.tv_sec-tv_last_update.tv_sec)>20)
				  {
					 update_details(trace);
					 gettimeofday(&tv_last_update,NULL);
					 tracedata=0;
				  }
				  else
				  tracedata=1;
			   }
			   else if ((o_r_error=errno)!=EINTR && o_r_error!=EAGAIN)
			   {
				   sock_error_no("pty read", o_r_error);
				   break;
			   }
			
         }
         // if environment feed back, read environment
         if (FD_ISSET(ctl->env_pipe, &rds ))
         {
            debug("event on the env_pipe\n");
            //needs a while, we want to read all at once
            do
            {
               memset(output_buf,0,buf_size);
               o_read=read(ctl->env_pipe, output_buf,buf_size-1);
               if (o_read==0) {debug("env: EOF\n");;break;} // pipe closed
               else if (o_read >0)
               {
                  debug ("env: Received %d bytes\n",(int)o_read);
                  dynbuffer_write_n(output_buf, o_read, mem);
               }
            }
            while (o_read>0 || ( o_read=SOCKET_ERROR && ((o_r_error=errno)==EINTR || o_r_error==EAGAIN )));
			char *env_buff=NULL;
			dynbuffer_pop(&mem, &env_buff, NULL);
            parse_env_string(&env_buff,ctl );
            if(o_read==0)
            {
               debug("env_pipe break\n");
               break;
            }
         }
      }
      else
      {
         gettimeofday(&tv_now,NULL);
         if ((int)(tv_now.tv_sec-tv_start.tv_sec) > ctl->timeout)
         {
         	Write_dyn_trace(trace, red,"System Error: timeout executing command!!!\n");
         	timeout=1;
         	break;
         }
         else
         {
            if (tracedata)
            {
               if ((int)(tv_now.tv_sec-tv_last_update.tv_sec)>20)
               {
                  update_details(trace);
                  gettimeofday(&tv_last_update,NULL);
                  tracedata=0;
               }
               else
                  tracedata=1;
            }
         }
      }
   } while (s_ret!=SOCKET_ERROR || ( s_ret==SOCKET_ERROR && (s_err==EINTR || s_err==EAGAIN || s_err==EWOULDBLOCK)));
	dynbuffer_clear(&mem);
   //printf("outputbuf p %p\n",output_buf);
	if (ctl->master!=-1)close(ctl->master);
	if (ctl->env_pipe!=-1)close(ctl->env_pipe);
   usleep(10000);
   int status;
   if (output_buf) free(output_buf);
   output_buf=NULL;
   if (timeout)
   {
      debug("Timeout, kill child\n");
      kill(ctl->child,SIGKILL); // TODO need to check with WHOANG
   }
   pid_t w=0;
   int this_error=0;
   while ((w= waitpid(ctl->child,&status, 0)) ==-1 && (this_error=errno)==EINTR )
   {
      debug("exec: interupted Syscall\n");
   }
   if (w==-1) {
      sock_error_no("waitpid",this_error);
      //if (!exitcode)
      Write_dyn_trace(trace, red,"System error: error on 'waitpid()': %s!!!\n",strerror(this_error));
      exitcode=1;
   }
   else if (WIFEXITED(status))
   {
      exitcode=WEXITSTATUS(status);
      if (exitcode)
      {
         debug("exit cmd: %s w error: %d\n",ctl->shellcommand, exitcode);
      }
      else
      {
         //debug("[OK]\n");
         //Write_dyn_trace(trace, green,"\t[OK]\n");
      }
   }
   else if (WIFSIGNALED(status) || WIFSTOPPED(status)) {
      alert("command stopped/killed by signal %d : %s\n", WTERMSIG(status),strsignal( WTERMSIG(status)));
      exitcode=1;
      if (!timeout)
         Write_dyn_trace(trace, red,"\ncommand stopped/killed by signal %d : %s\n", WTERMSIG(status),strsignal( WTERMSIG(status)));
   }
   else
   {    /* Non-standard case -- may never happen */
      error("Unexpected status child (0x%x)\n", status);
      exitcode=status;
   }

   //restore_tty(ctl, TCSADRAIN);
   return exitcode;
}

#ifndef HAVEPIPE2
#define pipe2(x,y) ({ int res=pipe(x); if (res==0) { fcntl(x[0], F_SETFL, y); fcntl(x[1], F_SETFL, y);} res;})
#endif


int exec_color(void * opaque)
{
   debug("start command color exec\n");

   data_exchange_t * data_exchange=((data_exchange_t *)opaque);
   int exit_code=0;
   struct trace_Struct *trace=data_exchange->trace;

   char line[20];
   sprintf(line ,"/dev/ptyXX");
   struct script_control ctl =
   {
#if !HAVE_LIBUTIL
            .line = line,
#endif
      .env_pipe=-1,
      .master = -1,
      .timeout=data_exchange->timeout,
      .shellcommand=data_exchange->shellcommand,
	   .env=data_exchange->env, /**< Environment */
	   .commandpath=data_exchange->commandpath /**< excact path how to start executor */
	};
   ctl.shell = data_exchange->parameters->shell;
	const char * dir=Environment_getenv(ctl.env,"PWD");
   debug("shell=%s, workdir=%s, cmd %s\n",ctl.shell,dir,ctl.shellcommand);
	//print_json(data_exchange->env->environment);
	
	word_exp_var_t wev;
	memset(&wev, 0, sizeof(word_exp_var_t));
   if (ctl.shell==NULL)
   {
	   int res=word_exp_var_dyn(&wev, data_exchange->shellcommand, data_exchange->env->environment,Environment_getenv(data_exchange->env, "PWD"));
	   if (res)
	   {
		   Write_dyn_trace(trace, red, "\nERROR:");
		   Write_dyn_trace(trace, none, "%s",wev.errmsg);
		   word_exp_var_free(&wev);
		   return -1;
	   }
	   else
      {
		 int i;
         for (i = 0; i < wev.argc; i++)
           debug("%s\n", wev.argv[i]);
		  
		  if (strcmp(wev.argv[0],"export")==0 )
		  {
			 if (wev.argc==2 && strchr(wev.argv[1],'=')!=NULL)
			 {
				char * endstr=NULL;
				char * par=strtok_r(wev.argv[1], "=", &endstr);
				if (par && strlen(par)>0)
				{
				   char * val=strtok_r(NULL, "=", &endstr);
					if (val)
				   		Environment_setenv(ctl.env,par, val, 1);
					else
					{
						Environment_setenv(ctl.env,par, "", 1);
					}
				}
			 }
			 word_exp_var_free(&wev);
			 return 0;
		  }
		  if (wev.argc>0 && strcmp(wev.argv[0],"cd")==0)
		  {
			 if (wev.argc>1)
			 {
				char result_path[PATH_MAX];
				memset(result_path,0,PATH_MAX);
				char * normalised_dir=normalizePath(dir,wev.argv[1],result_path);
				if (normalised_dir)
				{
				   Environment_setenv(ctl.env,"OLDPWD",dir , 1);
				   Environment_setenv(ctl.env,"PWD",normalised_dir , 1);
				}
			 }
			 else if (wev.argc==1)
			 {
				const char * homedir=Environment_getenv(ctl.env,"HOME");
				if (homedir)
				{
				   debug("cd '' to %s\n",homedir);
					Environment_setenv(ctl.env,"OLDPWD",dir , 1);
					Environment_setenv(ctl.env,"PWD",homedir , 1);
				}
			 }
			 word_exp_var_free(&wev);
			  
			  res=xis_dir(Environment_getenv(ctl.env, "PWD"));
			  if (!res)
			  {
				  Write_dyn_trace(trace, none,"Changed directory to %s\n",Environment_getenv(ctl.env, "PWD"));
			  }
			  else
			  {
				  Write_dyn_trace(trace, red,"\n Error: ");
				  if(res==-1)
				  {
					  Write_dyn_trace(trace, none,"No such directory: %s\n",Environment_getenv(ctl.env, "PWD"));
				  }
				  else if (res==-2)
				  {
					  Write_dyn_trace(trace, none,"Is not a directory: %s\n",Environment_getenv(ctl.env, "PWD"));
				  }
			  }
			  return res;
		  }
		  int varoffs=0;
		  while (wev.argc>varoffs && strchr(wev.argv[varoffs],'=')!=NULL)
		  {
			//do nothing, need to set env after fork since these are not exports
			 varoffs++;
		  }
		  if (wev.argc >varoffs && wev.argv[varoffs])
		  {
			 if(access(wev.argv[varoffs], X_OK) == 0)
				ctl.result=&wev;
			 else
			 {
				Write_dyn_trace(trace, red,"\n Error: cannot execute command \'%s\'\n",wev.argv[varoffs] );
				word_exp_var_free(&wev);
				return -1;
			 }
		  }
		  else
		  {
			 Write_dyn_trace(trace, red,"\n Error: unknown problem expanding command\n" );
			 word_exp_var_free(&wev);
			 return -1;
		  }
	  }
   }
   else
   {
      ctl.result=NULL;
   }

   getmaster(&ctl);

   if (ctl.master==INVALID_SOCKET)
   {
    Write_dyn_trace(trace, red,"\nsystem Error no PTY\n");
    error("system Error no PTY\n");
     return (-1);
   }
   // create env pipe
   int envPipe[2];
   if (pipe2(envPipe,FD_CLOEXEC) < 0)
   {
      sock_error("allocating pipe for child environment feedback");
      Write_dyn_trace(trace, red,"System error allocating env pipe\n");
      return(15);
   }

   fflush(stdout);
   debug("forking\n");
   ctl.child = fork();
   switch (ctl.child) {
      case -1: /* error */
         close(envPipe[PIPE_WRITE]);
         close(envPipe[PIPE_READ]);
         error("Fork failed \n");
         Write_dyn_trace(trace, red,"\nsystem Error FORK failed\n");
         kill(0, SIGTERM);
         exit_code=EXIT_FAILURE;
         break;
      case 0: /* the child */
         close(envPipe[PIPE_READ]);
         socket_O_CLOEXEC(envPipe[PIPE_WRITE]);
         ctl.env_pipe=envPipe[PIPE_WRITE];
         fflush(stdout);
         fflush(stderr);
         do_shell(&ctl);
         break;
      default: /* parent */
         close(envPipe[PIPE_WRITE]);
         socket_nonblocking(envPipe[PIPE_READ]);
         ctl.env_pipe=envPipe[PIPE_READ];
         exit_code=do_io(&ctl,trace);
         break;
   }
   if (ctl.shell==NULL) word_exp_var_free(&wev);
   close(ctl.master);
   /*
    need to change exit code on a "set +e", for that we need a var indicating that
   char exit_str[5];
   itoa(exit_code, exit_str, 10);
   setenv("?", exit_str, 1);
   */
   return exit_code;
}
