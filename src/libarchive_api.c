/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.

 This file is part of the oc-runner

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


/*! \file rootfs_tar.c
 *  \brief create docker layer from file system
 *  \author Created by Danny Goossen on 03/04/2018.
 *  \copyright 2018, Danny Goossen. MIT License
 *
 */

#define _GNU_SOURCE
#include "libarchive_api.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <archive.h>
#include <archive_entry.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "openssl.h"
#include <curl/curl.h>
#include "error.h"
#include "utils.h"
#include "cJSON_deploy.h"
#include "specials.h"

/* Forward local static declarations */

/**
 \brief init client data for callback funtions
 \param blob buffer for sha256 result
 \param [out] len stream length pointer
 \param inputPath archive filename or url for upload
 \param buff_size size of buffer chunks for libarchive stream
 \return pointer to structure
 */
void * init_client_data_cb( char * blob,size_t * len, const char * inputPath, size_t buff_size);
/**
 \brief set the autputh path and auth for stream archive
 \param mode non/file or url
 \param auth the authorisation for output url
 \param outpath pointer to url of path name
 */
void * set_output( void * userp, output_mode_t mode,const char * auth, char ** outpath );


/**
 \brief deinit client data for callback funtions
 \param userp pointer to structure, returns *userp=NULL
 */
static void deinit_client_data_cb(void ** userp);

/**
 \brief Callback to init the write callback
 \param a the archive
 \param userptr callback client_data
 */
static int archive_write_open_callback_sha256(struct archive * a,void * userptr);

/**
 \brief Callback to init the read callback
 \param a the archive
 \param userptr callback client_data
 */
static int archive_read_open_callback_sha256(struct archive * a,void * userptr);

/**
 \brief WRITE Achive Callback
 \param a the archive
 \param userptr callback client_data
 */
static ssize_t archive_write_callback_sha256(struct archive * a,void * userptr, const void* data,size_t len);

/**
 \brief READ Archive Callback
 \param a the archive
 \param userptr callback client_data
 */
static ssize_t archive_read_callback_sha256(struct archive * a,void * userptr, const void** data);

/**
 \brief curl header callback data for archive_close_callback_sha256() and callback function curl_headers_cb()
 */
struct curl_headers_struct{
	char * Location; /**< Location header where blob can be downloaded*/
	char * Docker_Content_Digest;/**< Content Digest returned from registry for verification*/
};

/**
 \brief curl header Callback function for archive_close_callback_sha256()
 \param [in]contents header line
 \param [in]size in conjuntion with \bnmemb defines the size of contents
 \param [in]nmemb in conjuntion with \bsize defines the size of contents
 \param [in,out]userp pointer to the callback data struct curl_headers_struct{}
 \return realsize=size * nmemb
 */
static size_t curl_headers_cb(void *contents, size_t size, size_t nmemb, void *userp);

/**
\brief Callback Finish R/W
\param a the archive
\param userptr callback client_data
*/
static int archive_close_callback_sha256(struct archive * a,void * userptr);

/**
 \brief Callback to get root as username/group
 \param userp callback client_data
 \param [in] gu group or user gid/uid
 \return user or group name of "root"
 */
static const char * get_ugname(void * userp,int64_t gu);


/* Public Functions */

layer_data_t * init_layer_data(size_t copy_buff_size)
{
   layer_data_t * layer_data =calloc(1,sizeof(layer_data_t));
   layer_data->error_msg=NULL;
   layer_data->buff_size=copy_buff_size;
   layer_data->copy_buffer=calloc(1, copy_buff_size);
   return layer_data;
}

void free_layer_data(layer_data_t ** layer_data)
{
   if (layer_data && *layer_data)
   {
      if ((*layer_data)->error_msg) free((*layer_data)->error_msg);
      if ((*layer_data)->copy_buffer) free((*layer_data)->copy_buffer);
      free(*layer_data);
      *layer_data=NULL;
   }
}

/**
 \brief macro to replace if (!exit_code)
 */
#define IS_OK if (!exit_code)
/**
 \brief macro to test if exit_code==0 if so executes command X and set exet_code to return value of command X
 */
#define IS_OK_ARCHIVE(X) if (!exit_code) exit_code= ( X !=ARCHIVE_OK);




/**
 \brief client callback data

 data structure used for the callback functions
 */
typedef struct client_data{
	output_mode_t mode;       /**< defines the output mode (none/file/stream to url), ignored for read_archive() */
	int fd_in;                 /**< the file-desciptor for the archive to create or read */
    int fd_out;                 /**< the file-desciptor for the archive to create or read */
	char * buff;            /**< the chunck stream buffer for libarchive set by init_client_data_cb() */
	size_t buff_size;       /**< the size of the chunck stream buffer set by  init_client_data_cb()*/
	SHA256_CTX sha256;      /**< the sha256 context data (libcrypto) */
	unsigned char hash[SHA256_DIGEST_LENGTH]; /**< the sha256 sum of the archive stream */
	char * blobsha256;      /**< the docker blobsum in format "sha256:<64*base16>\0" of the archive stream, zero terminated, the client has to provide the buffer with a size of SHA256_DIGEST_LENGTH_BLOBSUM or ((SHA256_DIGEST_LENGTH*2)+8) or 72 bytes */
	const char * input_path; /**< the filename of the Archive to read from */
	//const char * error_msg; /**< the error message in case of error */
	size_t *len;            /**< pointer to store the length of the stream */
	size_t len_NULL;        /**< in case len == NULL, use this to store len */
	int Open_error;         /**< set on errro in archive_write_open_callback_sha256() and archive_write_open_callback_sha256(), so archive_close_callback_sha256() can set correct error */
	const char * Open_error_for; /**< indicates on \bOpen_error if it was read or write operation */
	CURL * curl;            /**< curlhandle for upload streaming with curl and PATCH / and finalize upload with PUT**/
	char * Docker_Content_Digest; /**< Digest feedback from registry on curl stream upload **/
	const char* curl_tx_buff;
	size_t curl_tx_len;
	size_t curl_tx_off;
	int error;
	output_stream_source_t s2s;
	const char * auth;      /**< WWW auth bearer **/
	char ** outpath; /**< pointer to output path **/
}client_data_t ;


/**
 \brief loop archive and copy decompressed input to content
 */
static int loop_archive(layer_data_t * layer_data, struct archive *a_input,struct archive *a_content,void * data_content,int * a_content_need_close);

/**
 \brief loop rootfs and push to compressor and  to content
 */
static int loop_rootfs( layer_data_t * layer_data, struct archive *a_input,struct archive *a_content,struct archive *a_compressed, chmod_t chmod);

struct callback_rec_dir_data{
	struct archive *a_content;
	struct archive *a_compressed;
};

void callback_rec_dir(void * userp, char *path);

void callback_rec_dir(void * userp,char *path)
{
	struct callback_rec_dir_data *data=(struct callback_rec_dir_data *)userp;

	struct archive_entry *entry;
	entry = archive_entry_new(); // Note 2
	//char * path=strndup(file, newlen);


	archive_entry_set_pathname(entry, path);
	archive_entry_set_filetype(entry, AE_IFDIR);
	archive_entry_set_perm(entry, 0775);
	archive_write_header(data->a_content, entry);
	archive_write_header(data->a_compressed, entry);
	archive_entry_free(entry);
	debug("%s\n",path);

}

/**
 \brief loop file recursivly in reverse and if a dir path has not been added, create an archive entry
 */
static void recursive_dir(const char * file, size_t len, cJSON * list,struct archive *a_content,struct archive *a_compressed)
{
	struct callback_rec_dir_data data;
	data.a_compressed=a_compressed;
	data.a_content=a_content;
	char *path=NULL;
	// if file starts with /, prepend with .
	if (file[0]=='/')
		asprintf(&path, ".%s",file);
	else
		path=strdup(file);
	if (path)
	{
		recursive_dir_extract(file,len,list,callback_rec_dir,(void*)&data);
		free(path);
	}
}

char * stream_archive(char * const * pathnames, size_t pathnames_cnt,const char * workdir, stream_source_t ss, layer_data_t * layer_data, output_mode_t output_mode,chmod_t chmod,const char* output_auth,const char * output,...)
{
	char *output_path=NULL;
	if (output)
	   output_path=CSPRINTF(output);
	else if (ss==stream_source_archive && !output_path && pathnames_cnt==1 && *pathnames)
		output_path=strdup(*pathnames);
	if(!output_path && output_mode!=output_mode_none)
	{
	   layer_data->error_msg=strdup("Could not allocate buffer for tar filename");
	   return NULL;
	}
	int exit_code=0;
	struct archive *a_input=NULL;
	struct archive *a_content=NULL;
	struct archive *a_compressed=NULL;

	int a_content_need_close=0;
	int a_input_need_close=0;
	int a_compressed_need_close=0;

	// struct client data callback


	void * data_content=NULL;
	void * data_compressed=NULL;
	void * data_input=NULL;
	const char pwd[1024];

	IS_OK
	{
		IS_OK data_content=init_client_data_cb(layer_data->sha256_content,&layer_data->content_len,NULL,layer_data->buff_size);

		IS_OK a_content = archive_write_new();
		exit_code=exit_code || (a_content ==NULL);

		IS_OK_ARCHIVE(archive_write_add_filter_none(a_content));
		//IS_OK_ARCHIVE(archive_write_set_format_ustar(a_tar));
		//
		IS_OK_ARCHIVE(archive_write_set_bytes_per_block(a_content, (int)layer_data->buff_size));
		IS_OK_ARCHIVE(archive_write_set_bytes_in_last_block(a_content, 1));
	}

	if (ss==stream_source_rootfs)
	{
		data_input=init_client_data_cb(NULL,NULL,NULL,layer_data->buff_size);
		data_compressed=init_client_data_cb(layer_data->sha256_archive,&layer_data->archive_len,NULL,layer_data->buff_size);
		set_output(data_input, output_mode_none, NULL, NULL);
		set_output(data_compressed, output_mode, output_auth, &output_path);
		a_compressed = archive_write_new();
		exit_code=(a_compressed ==NULL);

			//set compression gzip for Archive
		//IS_OK_ARCHIVE(archive_write_add_filter_gzip(a_compressed));
		IS_OK_ARCHIVE(archive_write_add_filter_bzip2(a_compressed));
			// set fromat gnutar
		IS_OK_ARCHIVE(archive_write_set_format_ustar(a_compressed));

		IS_OK_ARCHIVE(archive_write_set_bytes_per_block(a_compressed, (int)layer_data->buff_size));
		IS_OK_ARCHIVE(archive_write_set_bytes_in_last_block(a_compressed, 1));

		IS_OK_ARCHIVE(archive_write_open(a_compressed, data_compressed, archive_write_open_callback_sha256, archive_write_callback_sha256, archive_close_callback_sha256));

		if (!layer_data->error_msg && exit_code && a_compressed && archive_errno(a_compressed)!=ARCHIVE_OK )
		{

			layer_data->error_msg=strdup(archive_error_string(a_compressed));

		}

		IS_OK a_compressed_need_close=1;
		IS_OK
		{
			getcwd((char*)pwd, 1024);
			int res=0;
			if (workdir)
			{
				res=chdir(workdir);
			}
			if (res)
			{
				exit_code=-1;
				if (!layer_data->error_msg )
				{
					const char what[]="Error change directory to <rootfs>:\n";
					const char * errmsg=strerror(errno);
					layer_data->error_msg=calloc(1,strlen(what)+strlen(errmsg)+1);
					sprintf(layer_data->error_msg,"%s%s",what,errmsg);
				}
			}
		}
		IS_OK
		{
			// no compression for tar
			IS_OK_ARCHIVE(archive_write_set_format_ustar(a_content));
			IS_OK_ARCHIVE(archive_write_open(a_content, data_content, archive_write_open_callback_sha256, archive_write_callback_sha256, archive_close_callback_sha256));
			IS_OK a_content_need_close=1;
		}

		IS_OK
		{
			a_input=archive_read_disk_new();
			exit_code=exit_code || (a_input ==NULL);

			// IS_OK_ARCHIVE(archive_read_disk_set_standard_lookup(a_disk));
			// just dummy function that returns "root"
			IS_OK_ARCHIVE(archive_read_disk_set_gname_lookup(a_input, NULL, get_ugname, NULL));
			IS_OK_ARCHIVE(archive_read_disk_set_uname_lookup(a_input, NULL, get_ugname, NULL));

			//TODO figure this out
			IS_OK_ARCHIVE(archive_read_disk_set_symlink_physical(a_input));
			//IS_OK_ARCHIVE(archive_read_disk_set_symlink_logical(<#struct archive *#>)(a_input));
			// if no filename was given default to .


		}


	}
	else if (ss==stream_source_archive)
	{


		data_input=init_client_data_cb(layer_data->sha256_archive,&layer_data->archive_len,*pathnames,layer_data->buff_size);
		set_output(data_input, output_mode, output_auth, &output_path);
		// we keep below in, when option for different compression/format, activate!!!
		//data_compress=init_client_data_cb(NULL,NULL,NULL,layer_data->buff_size);
		//set_output(data_compress, output_mode_none, NULL, NULL);
		IS_OK a_input = archive_read_new();
		exit_code=exit_code || (a_input ==NULL);

		IS_OK_ARCHIVE(archive_read_support_filter_all(a_input));
		IS_OK_ARCHIVE(archive_read_support_format_empty(a_input));
		//IS_OK_ARCHIVE(archive_read_support_format_all(a_input));
		IS_OK_ARCHIVE(archive_read_support_format_raw(a_input));


		IS_OK_ARCHIVE(archive_read_open(a_input, data_input, archive_read_open_callback_sha256, archive_read_callback_sha256, archive_close_callback_sha256));
		IS_OK a_input_need_close=1;
		if (!layer_data->error_msg && exit_code && a_input && archive_errno(a_input)!=ARCHIVE_OK )
		{
			const char *errmsg=archive_error_string(a_input);
			if (errmsg)
			layer_data->error_msg=strdup(errmsg);
			else
			layer_data->error_msg=strdup("archive_error_string returns NULL");
		}

	}
	else
	{
		if (!layer_data->error_msg && exit_code && a_input && archive_errno(a_input)!=ARCHIVE_OK )
		{
			layer_data->error_msg=strdup("Error: Unknown Source selection");
		}
		free(output_path);
		if (data_content) free(data_content);
		return NULL;
	}



		int close_result=0;
	IS_OK
	{
	if (ss==stream_source_rootfs)
	{


		if (!pathnames || !(*pathnames))
		{
			alert("stream archive path: '.'\n");
			IS_OK_ARCHIVE(archive_read_disk_open(a_input, "."));
			IS_OK a_input_need_close=1;
			IS_OK_ARCHIVE(loop_rootfs(layer_data, a_input, a_content, a_compressed, chmod));
			if (a_input_need_close) archive_read_close(a_input);
		}
		else
		{
			int i=0;
			cJSON * dir_list=cJSON_CreateArray();
			for (i=0;i<pathnames_cnt && !exit_code; i++)
			{
				if (pathnames[i])
				{
					alert("stream archive path: %s\n",pathnames[i]);
					//if (chmod==chmod_u2g)
					{
						// if we define pathnames for a cache, we need to make sure the whole path has been add seperatly to ensure writebilty
						recursive_dir(pathnames[i], strlen(pathnames[i]), dir_list, a_content, a_compressed);
					}
					IS_OK_ARCHIVE(archive_read_disk_open(a_input,pathnames[i]));
					IS_OK a_input_need_close=1;

					IS_OK_ARCHIVE(loop_rootfs(layer_data, a_input, a_content, a_compressed, chmod));
					if (a_input_need_close) archive_read_close(a_input);
				}
			}
			cJSON_Delete(dir_list);
		}

		if (workdir) chdir(pwd);
		if (exit_code && !layer_data->error_msg)
		{
			if (archive_errno(a_compressed))
				layer_data->error_msg=strdup(archive_error_string(a_compressed));
			else if (archive_errno(a_content))
				layer_data->error_msg=strdup(archive_error_string(a_content));
			else layer_data->error_msg=strdup("Undefined Error");
		}
		if (a_compressed_need_close) close_result=archive_write_close(a_compressed);
		a_compressed_need_close=0;
		int error_compressed=archive_errno(a_compressed);
		if ((close_result || error_compressed)&& !layer_data->error_msg)
		{
			layer_data->error_msg=strdup(archive_error_string(a_compressed));
			exit_code=-1;
		}

	}
	else if(ss==stream_source_archive)
	{
		IS_OK_ARCHIVE(loop_archive(layer_data,a_input,a_content,data_content,&a_content_need_close));
		if (exit_code && !layer_data->error_msg) {
			if (archive_errno(a_input))
			{
				const char *errmsg=archive_error_string(a_input);
				if (errmsg)
					layer_data->error_msg=strdup(errmsg);
				else
					layer_data->error_msg=strdup("archive_error_string returns NULL after loop for input");
			}
			else if (archive_errno(a_content))
			{
				const char *errmsg=archive_error_string(a_content);
				if (errmsg)
					layer_data->error_msg=strdup(errmsg);
				else
					layer_data->error_msg=strdup("archive_error_string returns NULL after loop archive for content");
			}
			else layer_data->error_msg=strdup("Undefined Error");
		}
		// close content
		if (a_content_need_close)archive_write_close(a_content);
		a_content_need_close=0;

		//close input
		if (a_input_need_close) close_result=archive_read_close(a_input);
		a_input_need_close=0;
		int error_input=archive_errno(a_input);
		if ((close_result || error_input)&& !layer_data->error_msg)
		{
			const char *errmsg=archive_error_string(a_input);
			if (errmsg)
				layer_data->error_msg=strdup(errmsg);
			else
				layer_data->error_msg=strdup("archive_error_string returns NULL after loop for input");
			exit_code=-1;
		}

	}
	}
	// close input
	if (a_input_need_close) archive_read_close(a_input);

	// free input
	if (a_input) archive_read_free(a_input);
	a_input=NULL;

	// free input cb data
	if (data_input) deinit_client_data_cb(&data_input);

	// close content
	if (a_content_need_close)archive_write_close(a_content);

	// free content
	if (a_content)archive_write_free(a_content);
	a_content=NULL;

	//free content data
	if (data_content) deinit_client_data_cb(&data_content);

	// close compressor
	if (a_compressed_need_close) archive_write_close(a_compressed);

	//free compressor

	if (a_compressed) archive_write_free(a_compressed);
	a_compressed=NULL;

	if (data_compressed) deinit_client_data_cb(&data_compressed);

	if (exit_code)
	{
		if (output_path) free(output_path);
		output_path=NULL;
	}
	return output_path;
}

/**
 \brief copy decompressed data from archive to content
*/
static int copy_data(struct archive *ar, struct archive *aw,char* buff,int buff_size);



int loop_archive( layer_data_t * layer_data, struct archive *a_input,struct archive *a_content, void * data_content,int * a_content_need_close)
{
	int exit_code=0;
	int first=1;
	int r=0;
	struct archive_entry *entry=NULL;
    for (;!exit_code;)
	{
		r = archive_read_next_header(a_input, &entry);
		if (r == ARCHIVE_EOF)
			break;
		if (r != ARCHIVE_OK) {
			exit_code=1;
		}
		else if (first)
		{
			const char * format_name=archive_format_name(a_input);
			debug("info Format=%s\n",format_name);
			//int fmt=archive_format(a_input);
			//IS_OK_ARCHIVE(archive_write_set_format(a_content, fmt));
			IS_OK_ARCHIVE(archive_write_set_format_raw(a_content));
			IS_OK_ARCHIVE(archive_write_open(a_content, data_content, archive_write_open_callback_sha256, archive_write_callback_sha256,archive_close_callback_sha256 ));
			IS_OK (*a_content_need_close)=1;

			first=0;
		}
		IS_OK_ARCHIVE(archive_write_header(a_content, entry));
		IS_OK_ARCHIVE(copy_data(a_input, a_content,layer_data->copy_buffer,(int)layer_data->buff_size));
	}
	return(exit_code);
}


int loop_rootfs( layer_data_t * layer_data, struct archive *a_input,struct archive *a_content,struct archive *a_compressed, chmod_t chmod)
{
	int exit_code=0;
	struct archive_entry *entry=NULL;
	ssize_t len=0;
	int fd=-1;
	int r=0;
	for (;;)
	{

		entry = archive_entry_new();
		r = archive_read_next_header2(a_input, entry);
		if (r == ARCHIVE_EOF)
		{
			debug("\nEOF read a_input\n");
			break;
		}
		else if (r!=ARCHIVE_OK && r >= ARCHIVE_FAILED)
		{

			// don't care warnings
		}
		else if (r==ARCHIVE_FATAL || r == ARCHIVE_FAILED )
		{
			debug("Archive Fatal or Failed: %s",archive_error_string(a_input));
			layer_data->error_msg=strdup(archive_error_string(a_input));
			// stop on fail
			exit_code=-1;
			break;
		}
		else
		{
			//start work
			// but if name starts with '/', prepend with '.'
			if (archive_entry_pathname(entry))
			{
				if (archive_entry_pathname(entry)[0]=='/')
				{
					char * path=NULL;
					asprintf(&path,".%s",archive_entry_pathname(entry));
					if (path)
					{
						archive_entry_set_pathname(entry, path);
						free(path);
						path=NULL;
					}
				}
			}
			//debug("utf8:%s, %s\n",archive_entry_pathname_utf8(entry),archive_entry_pathname(entry));
			archive_read_disk_descend(a_input);

			mode_t m;

			/* Make everything owned by root:root. */
			archive_entry_set_uid(entry, 0);
			archive_entry_set_uname(entry, "root");
			archive_entry_set_gid(entry, 0);
			archive_entry_set_gname(entry, "root");

			if (chmod==chmod_u2g)
			{
				/* copy Owner Permissions to Group.
				 e.g. 744 becomes 774, 600 becomes 660
				 to allow non-root to edit content of layer
				 */
				m = archive_entry_mode(entry);
				mode_t g;
				g=(m & 0700) >> 3;
				archive_entry_set_mode(entry, (m | g));
			}
			r = archive_write_header(a_content, entry);
			if (r!=ARCHIVE_OK){
				archive_entry_free(entry);

				exit_code=-1;
				break;
			}
			r = archive_write_header(a_compressed, entry);
			if (r < ARCHIVE_OK) {
				if (!layer_data->error_msg)
					layer_data->error_msg=strdup(archive_error_string(a_compressed));
				archive_entry_free(entry);
				debug("loop rootfs: error write compressed header\n");
				exit_code=-1;
				break;
			}
			if (r > ARCHIVE_FAILED) {
				/* For now, we use a simpler loop to copy data
				 * into the target archive. */
				fd = open(archive_entry_sourcepath(entry), O_RDONLY);
				len = read(fd, layer_data->copy_buffer, layer_data->buff_size);
				ssize_t e_write=ARCHIVE_OK;
				while (len > 0 && (e_write>=ARCHIVE_OK))
				{
					e_write= archive_write_data(a_compressed, layer_data->copy_buffer, len);
					if (e_write>=ARCHIVE_OK)
						e_write=archive_write_data(a_content, layer_data->copy_buffer, len);
					else if (!layer_data->error_msg)
					{
						debug("loop rootfs: error, e_write %s\n",archive_error_string(a_compressed));
						layer_data->error_msg=strdup(archive_error_string(a_compressed));
					}
					else debug("loop rootfs: already error, new error: %s\n",archive_error_string(a_compressed));
					if (e_write>=ARCHIVE_OK)
						len = read(fd, layer_data->copy_buffer, layer_data->buff_size);
				}
				close(fd);
				if (e_write<ARCHIVE_OK)
				{
					debug("loop rootfs: ewrite<ARCHIVE_OK\n");
					archive_entry_free(entry);
					exit_code=-1;
					break;
				}
			}
			archive_entry_free(entry);
			//debug("<<<");
		}
	}
	debug("loop_rootfs: exit code %d\n",exit_code);
	return exit_code;
}

int copy_data(struct archive *ar, struct archive *aw,char* buff,int buff_size)
{
    ssize_t read_size=0;

    for (;;) {
        read_size= archive_read_data(ar, buff, buff_size);
        if (read_size==0) return ARCHIVE_OK;
        else if (read_size<0) return ARCHIVE_FATAL;
        else
        {
            ssize_t write_size=0;
            size_t write_size_sum=0;
            while (write_size_sum<read_size)
            {
                write_size=archive_write_data(aw, buff, read_size);
                if  (write_size<0) return ARCHIVE_FATAL;
                else
                    write_size_sum=write_size_sum+write_size;
            }
        }
    }
}

/* local private static functions code */

/**
 \brief constant char * to return "root" by get_ugname()
 */
static const char * ROOT=NULL;
//static const char ROOT[]="root";


/**
 \brief constant to return "write" for error handling

  Used in archive_write_open_callback_sha256() to indicate it was write_open since we only have one function for close.
 Needed as a workaround to get the correct error on an open, that is set in the archive_close_callback_sha256()

 */
static const char OPEN_FOR_WRITE[]="write";
/**
 \brief constant to return "write" for error handling

 Used in archive_read_open_callback_sha256() to indicate it was read_open since we only have one function for close.
 Needed as a workaround to get the correct error on an open, that is set in the archive_close_callback_sha256().

 */
static const char OPEN_FOR_READ[]="read";


static ssize_t output_stream_curl(struct archive * a,void * userptr);

static int output_stream_curl_close(struct archive * a,void * userptr);



void * init_client_data_cb( char * blob,size_t * len, const char * inputPath, size_t buff_size)
{
   client_data_t *cd= calloc(1,sizeof(client_data_t));
   cd->blobsha256=blob;
   //cd->error_msg=NULL;
   cd->input_path=inputPath;
   if (len==NULL)
		cd->len=&cd->len_NULL;
   else
        cd->len=len;
   cd->buff=NULL;
   cd->buff_size=buff_size;
   *cd->len=0;
   cd->fd_in=-1;
	cd->fd_out=-1;
   cd->Open_error=0;
	cd->mode=output_mode_none;
	cd->error=0;
	cd->auth=NULL;

   return cd;
}

void * set_output( void * userp, output_mode_t mode,const char * auth, char ** outpath )
{
	client_data_t *cd=(client_data_t *)userp;
	cd->outpath=outpath;
	cd->auth=auth;
	cd->mode=mode;
	return cd;
}

void deinit_client_data_cb(void **userptr)
{
    client_data_t *cd=*(client_data_t **)userptr;
   if (*userptr)
   {
	   if (cd->curl) curl_easy_cleanup(cd->curl);
	   cd->curl=NULL;
      if (cd->buff) free(cd->buff);
      if (cd->fd_in!=-1) close(cd->fd_in);
	   if (cd->fd_out!=-1) close(cd->fd_out);
      cd->buff=NULL;
      cd->fd_in=-1;
	   cd->fd_out=-1;
      cd->Open_error=0;
      free(*userptr);
      *userptr=NULL;
   }
}


int archive_write_open_callback_sha256(struct archive * a,void * userptr)
{
   client_data_t *cd=(client_data_t *)userptr;
   SHA256_Init(&cd->sha256);
   if (cd->mode==output_mode_file)
   {
      cd->fd_out = open ( *cd->outpath,  O_WRONLY|O_CREAT|O_TRUNC, 0644 );
      if (cd->fd_out ==-1)
      {
         int err=errno;
         archive_set_error(a,err, "Open write archive \'%s\' : %s", *cd->outpath,strerror(err));
         cd->Open_error=err;
         cd->Open_error_for=OPEN_FOR_WRITE;
       //  cd->error_msg=strerror(err);
		  cd->error=1;
         return ARCHIVE_FATAL;
      }
   }
   else if (cd->mode==output_mode_curl)
   {
	   cd->curl=curl_easy_init();
	   if(cd->curl)
	   {
	   const char * default_ca_bundle=getenv("DEFAULT_CA_BUNDLE");
	   if (default_ca_bundle)
	     curl_easy_setopt(cd->curl, CURLOPT_CAINFO, default_ca_bundle);
	   curl_easy_setopt(cd->curl, CURLOPT_SSL_VERIFYPEER, 1L);
	   curl_easy_setopt(cd->curl, CURLOPT_FOLLOWLOCATION, 0L);
	   curl_easy_setopt(cd->curl, CURLOPT_NOPROGRESS, 1L);
	   curl_easy_setopt(cd->curl, CURLOPT_TIMEOUT,300L); // 1min for transfer
	   curl_easy_setopt(cd->curl, CURLOPT_CONNECTTIMEOUT,60L); // 5 sec on connection
	   curl_easy_setopt(cd->curl, CURLOPT_URL,*cd->outpath);
	   curl_easy_setopt(cd->curl, CURLOPT_CUSTOMREQUEST,"PATCH");
	   curl_easy_setopt(cd->curl,CURLOPT_VERBOSE,0L);
		   curl_easy_setopt(cd->curl, CURLOPT_USERAGENT, PACKAGE_STRING);
	   }
	   else
	   {
		    archive_set_error(a,1, "Init Curl failed");
		   return ARCHIVE_FATAL;
	   }
   }
   else
      cd->mode=output_mode_none;
   return 0;
}

int archive_read_open_callback_sha256(struct archive * a,void * userptr)
{
   client_data_t *cd=(client_data_t *)userptr;
   SHA256_Init(&cd->sha256);
   if (cd->input_path)
   {
      cd->fd_in = open ( cd->input_path,  O_RDONLY );
      if (cd->fd_in <0)
      {
         int err=errno;
         archive_set_error(a,err, "Open_Read_Archive \'%s\': %s",  cd->input_path,strerror(err));

         cd->Open_error=err;
         cd->Open_error_for=OPEN_FOR_READ;
        // cd->error_msg=strerror(err);
		  cd->error=1;
         return ARCHIVE_FATAL;
      }


   }
   else
   {
      archive_set_error(a,EINVAL,"Open_Read_Archive: Missing Archive_Filename");
      return (ARCHIVE_FATAL);
   }
	if (cd->mode==output_mode_file)
	{
		cd->fd_out = open ( *cd->outpath,  O_WRONLY|O_CREAT|O_TRUNC, 0644 );
		if (cd->fd_out ==-1)
		{
			int err=errno;
			archive_set_error(a,err, "Open write archive \'%s\' : %s", *cd->outpath,strerror(err));
			cd->Open_error=err;
			cd->Open_error_for=OPEN_FOR_WRITE;
			//cd->error_msg=strerror(err);
			cd->error=1;
			return ARCHIVE_FATAL;
		}
	}
	else if (cd->mode==output_mode_curl)
	{
		cd->curl=curl_easy_init();
		if (cd->curl)
		{
		const char * default_ca_bundle=getenv("DEFAULT_CA_BUNDLE");
		if (default_ca_bundle)
			curl_easy_setopt(cd->curl, CURLOPT_CAINFO, default_ca_bundle);
		curl_easy_setopt(cd->curl, CURLOPT_SSL_VERIFYPEER, 1L);
		curl_easy_setopt(cd->curl, CURLOPT_FOLLOWLOCATION, 0L);
		curl_easy_setopt(cd->curl, CURLOPT_NOPROGRESS, 1L);
		curl_easy_setopt(cd->curl, CURLOPT_TIMEOUT, 300L); // 1min for transfer
		curl_easy_setopt(cd->curl, CURLOPT_CONNECTTIMEOUT,60L); // 5 sec on connection
		curl_easy_setopt(cd->curl, CURLOPT_URL,*cd->outpath);
		curl_easy_setopt(cd->curl, CURLOPT_CUSTOMREQUEST,"PATCH");
		curl_easy_setopt(cd->curl,CURLOPT_VERBOSE,0L);
			curl_easy_setopt(cd->curl, CURLOPT_USERAGENT, PACKAGE_STRING);
		}
		else
		{
			archive_set_error(a,1, "Init Curl failed");
			return ARCHIVE_FATAL;
		}
	}
	cd->buff=calloc(1,cd->buff_size);
    return ARCHIVE_OK;
}

static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream)
{
    client_data_t *cd=(client_data_t *)stream;
	size_t len=size*nmemb;
	if ((len+cd->curl_tx_off)>cd->curl_tx_len)
		len=len-((len+cd->curl_tx_off) - cd->curl_tx_len);
	memcpy(ptr, cd->curl_tx_buff+cd->curl_tx_off,len);
	cd->curl_tx_off=cd->curl_tx_off+len;
	return len;
}



ssize_t output_stream_curl(struct archive * a,void * userptr)
{
	ssize_t	 size_writen=0;
	client_data_t *cd=(client_data_t *)userptr;
	char range[1024];
	snprintf(range,1024,"Range: %zu-%lu",*(cd->len),*(cd->len)+cd->curl_tx_len-1);
	char content_length[1024];
	snprintf(content_length,1024,"Content-Length: %zu",cd->curl_tx_len);

	curl_easy_setopt(cd->curl, CURLOPT_UPLOAD, 1L);
	curl_easy_setopt(cd->curl, CURLOPT_READFUNCTION,read_callback );
	curl_easy_setopt(cd->curl, CURLOPT_READDATA,userptr );

	//curl_easy_setopt(cd->curl, CURLOPT_POSTFIELDS,cd->curl_tx_buff);
	curl_easy_setopt(cd->curl, CURLOPT_INFILESIZE_LARGE,(curl_off_t)cd->curl_tx_len);
	//curl_easy_setopt(cd->curl, CURLOPT_POSTFIELDSIZE,cd->curl_tx_len);
	//curl_easy_setopt(cd->curl,CURLOPT_FAILONERROR,1L);

	struct curl_slist *headers = NULL;

	headers = curl_slist_append(headers, "Expect:");
	headers = curl_slist_append(headers, "Content-Type: application/octet-stream");

	char * auth=NULL;
	if (cd->auth)
	{
		auth=calloc(1,strlen("Authorization: ")+1+strlen(cd->auth));
		sprintf(auth,"Authorization: %s",cd->auth);
		headers = curl_slist_append(headers, auth);

	}
	headers = curl_slist_append(headers, content_length);
	headers = curl_slist_append(headers, range);
	curl_easy_setopt(cd->curl, CURLOPT_HTTPHEADER, headers);
	struct curl_headers_struct headers_res;
	headers_res.Location=NULL;
	headers_res.Docker_Content_Digest=NULL;

	curl_easy_setopt(cd->curl, CURLOPT_HEADERFUNCTION,curl_headers_cb);
	curl_easy_setopt(cd->curl, CURLOPT_WRITEHEADER, &headers_res);
	CURLcode res=curl_easy_perform(cd->curl);
	if (headers)curl_slist_free_all(headers);
	headers=NULL;
	if (auth) free(auth);
	auth=NULL;
	headers=NULL;
	long http_code=0;
	if (res!=CURLE_OK)
	{
		archive_set_error(a,res, "Write_archive_to_url: %s", curl_easy_strerror(res));
		cd->error=1;
		return ARCHIVE_FATAL;
	}
	else
	{
		curl_easy_getinfo (cd->curl, CURLINFO_RESPONSE_CODE, &http_code);
		if (http_code==202)
		{
			size_writen=cd->curl_tx_len;
			//if (cd->auth) free(cd->auth);
			//cd->auth=NULL;
			char * tmp=*cd->outpath;
			if (headers_res.Location)
			{
				*cd->outpath=headers_res.Location;
				headers_res.Location=NULL;
				free(tmp);
				tmp=NULL;
				curl_easy_setopt(cd->curl, CURLOPT_URL,*cd->outpath);
			}
			if (headers_res.Docker_Content_Digest)
			{
				if (cd->Docker_Content_Digest) free(cd->Docker_Content_Digest);
				cd->Docker_Content_Digest=headers_res.Docker_Content_Digest;
				headers_res.Docker_Content_Digest=NULL;
			}

		}
		else
		{
			archive_set_error(a,(int)http_code, "Write_archive_to_url http: %ld", http_code);
			cd->error=1;
			if(headers_res.Location) free(headers_res.Location);
			if(headers_res.Docker_Content_Digest) free(headers_res.Docker_Content_Digest);

			return ARCHIVE_FATAL;
		}
	}
	if(headers_res.Location) free(headers_res.Location);
	if(headers_res.Docker_Content_Digest) free(headers_res.Docker_Content_Digest);
	return (size_writen);
}

ssize_t archive_write_callback_sha256(struct archive * a,void * userptr, const void* data,size_t len)
{
   client_data_t *cd=(client_data_t *)userptr;
   ssize_t	 size_writen=len;

   if (cd->mode!=output_mode_none)
   {
	  if (cd->mode==output_mode_file)
	  {
         size_writen= write(cd->fd_out, data,len);
	  }
	  else if (cd->error)
	  {
		  return ARCHIVE_FATAL;
	  }
	   else
	  {
		  cd->curl_tx_buff=data;
		  cd->curl_tx_len=len;
		  cd->curl_tx_off=0;
		  size_writen=output_stream_curl(a, cd);
		  cd->curl_tx_buff=NULL;
		  debug("PATCH wrote %zu\n",len);
	  }
   }
   if (size_writen>0)
   {
      SHA256_Update(&cd->sha256, data, size_writen);
      *(cd->len)=(*(cd->len))+size_writen;
      return size_writen;
   }
   else
   {
	  if (archive_errno(a)==ARCHIVE_OK)
   	  {
        int err=errno;
		if (*cd->outpath)
        	archive_set_error(a,err, "Write_archive_to_file \'%s\': %s", *cd->outpath,strerror(err));
		  else
		 	 archive_set_error(a,err, "Write_archive: %s",strerror(err));
      }
      return ARCHIVE_FATAL;
   }
}

ssize_t archive_read_callback_sha256(struct archive * a,void * userptr, const void** data)
{
   client_data_t *cd=(client_data_t *)userptr;
   *data=cd->buff;
   if (!cd->buff)
   {
      archive_set_error(a,ENOBUFS, "Read_Archive: \'%s\' %s", cd->input_path, strerror(ENOBUFS));
      return ARCHIVE_FATAL;
   }
   ssize_t read_size= read(cd->fd_in, cd->buff,cd->buff_size);
   if (read_size>0)
   {
	   ssize_t size_writen=read_size;
	   if (cd->mode!=output_mode_none)
	   {
		   if (cd->mode==output_mode_file)
		   {
			   size_writen= write(cd->fd_out, data,read_size);
		   }
		   else if (cd->error)
		   {
			   return ARCHIVE_FATAL;
		   }
		   else
		   {
			   cd->curl_tx_buff=cd->buff;
			   cd->curl_tx_len=read_size;
			   cd->curl_tx_off=0;
			   size_writen=output_stream_curl(a, cd);
			   cd->curl_tx_buff=NULL;
			}
	   }
       if (size_writen>0)
	   {
		  *(cd->len)=(*(cd->len))+read_size;
		  SHA256_Update(&cd->sha256, cd->buff, read_size);
		  return read_size;
	   }
	   else
	   {
		   if (archive_errno(a)==ARCHIVE_OK)
		   {
              int err=errno;
              archive_set_error(a,err, "Write_Archive \'%s\': %s", *cd->outpath, strerror(err));
			   cd->error=err;
		   }
		   return ARCHIVE_FATAL;
	   }
   }
   else if (read_size==0)
   {
      return read_size;
   }
   else if (read_size<0)
   {
	   if (archive_errno(a)==ARCHIVE_OK)
	   {
          int err=errno;
          archive_set_error(a,err, "Read_Archive \'%s\': %s", cd->input_path, strerror(err));
	   }
		return ARCHIVE_FATAL;
   }
   return read_size;
}



/*------------------------------------------------------------------------
 * Helper function for Curl request
 *------------------------------------------------------------------------*/
size_t curl_headers_cb(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	struct curl_headers_struct * s_header=(struct curl_headers_struct*)userp;

	char * e=contents;
	int len=(int)realsize;
	int pos = 0;
	while (*e++ != ':' && pos < len) pos++;
	if (pos != len && pos >1)
	{
		char * buf=strndup(contents+pos+2, len-pos-4);
		if (buf)
		{
			if (memcmp(contents,"Location",pos)==0)
			{

				if (s_header->Location) free(s_header->Location);
				s_header->Location=buf;
				buf=NULL;

			}
			else if (memcmp(contents,"Docker_Content_Digest",pos)==0)
			{
				if (s_header->Docker_Content_Digest) free(s_header->Docker_Content_Digest);
				s_header->Docker_Content_Digest=buf;
				buf=NULL;
			}
			else free(buf);
		}
	}
	return realsize;
}


static int output_stream_curl_close(struct archive * a,void * userptr);

int output_stream_curl_close(struct archive * a,void * userptr)
{
	 client_data_t *cd=(client_data_t *)userptr;
	if (cd->error || cd->Open_error || !cd->curl)
	{
		if (cd->curl)curl_easy_cleanup(cd->curl);
		cd->curl=NULL;
		return ARCHIVE_FATAL;
	}
	else
	{
		char * digest_escaped=NULL;
		char *p=NULL;
		if ((p=strchr(cd->blobsha256, ':')))
		{
			char * t=calloc(1, strlen(cd->blobsha256)+1+2);
			int len=(int)(p-cd->blobsha256);
			sprintf(t, "%.*s%%3A%s",len,cd->blobsha256,p+1);
			digest_escaped=t;
		}
		else
			digest_escaped=strdup(cd->blobsha256);

		char seperator=0;
		if (strchr(*cd->outpath,'?')!=NULL)
			seperator='&';
		else seperator='?';

		char * url=calloc(1, strlen(*cd->outpath)+1+7+strlen(digest_escaped)+1);
		sprintf(url,"%s%cdigest=%s",*cd->outpath,seperator,digest_escaped);
		curl_easy_setopt(cd->curl, CURLOPT_POSTFIELDS, "");
		curl_easy_setopt(cd->curl, CURLOPT_POSTFIELDSIZE,0);
		curl_easy_setopt(cd->curl, CURLOPT_URL,url);
		curl_easy_setopt(cd->curl, CURLOPT_CUSTOMREQUEST,NULL);
		curl_easy_setopt(cd->curl, CURLOPT_PUT,1L);
		curl_easy_setopt(cd->curl,CURLOPT_VERBOSE,0L);
		curl_easy_setopt(cd->curl, CURLOPT_UPLOAD, 1L);
		curl_easy_setopt(cd->curl, CURLOPT_READFUNCTION, NULL);
		curl_easy_setopt(cd->curl, CURLOPT_READDATA,NULL);

		char content_length[1024];
		snprintf(content_length,1024,"Content-Length: 0");

		curl_easy_setopt(cd->curl, CURLOPT_INFILESIZE_LARGE,(curl_off_t)0);


		struct curl_slist *headers = NULL;
		char range[1024];
		snprintf(range,1024,"Range: %zu-%lu/",*(cd->len),*(cd->len));

		headers = curl_slist_append(headers, "Expect:");
		headers = curl_slist_append(headers, "Content-Type: application/octet-stream");
		headers = curl_slist_append(headers, content_length);
		char * auth=NULL;
		if (cd->auth)
		{
			auth=calloc(1,strlen("Authorization: ")+1+strlen(cd->auth));
			sprintf(auth,"Authorization: %s",cd->auth);
			headers = curl_slist_append(headers, auth);

		}
		curl_easy_setopt(cd->curl, CURLOPT_HTTPHEADER, headers);
		struct curl_headers_struct headers_res;
		headers_res.Location=NULL;
		headers_res.Docker_Content_Digest=NULL;

		curl_easy_setopt(cd->curl, CURLOPT_HEADERFUNCTION,curl_headers_cb);
		curl_easy_setopt(cd->curl, CURLOPT_WRITEHEADER, &headers_res);

		CURLcode res=curl_easy_perform(cd->curl);

		if (headers)curl_slist_free_all(headers);
		headers=NULL;
		if (auth) free(auth);
		auth=NULL;
		headers=NULL;

		long http_code=0;
		if (res!=CURLE_OK)
		{
			archive_set_error(a,res, "Finalize_Write_archive_to_url: %s", curl_easy_strerror(res));
		}
		else
		{
			curl_easy_getinfo (cd->curl, CURLINFO_RESPONSE_CODE, &http_code);
			if (http_code==201)
			{
				//success
				// feedback Location url to filename

				if (headers_res.Location)
				{
					char * tmp=*cd->outpath;
					*cd->outpath=headers_res.Location;
					headers_res.Location=NULL;
					free(tmp);
					tmp=NULL;
				}
				if (headers_res.Docker_Content_Digest)
				{
					char * tmp=cd->Docker_Content_Digest;
					cd->Docker_Content_Digest=headers_res.Docker_Content_Digest;
					headers_res.Docker_Content_Digest=NULL;
					if (tmp)free(tmp);
					tmp=NULL;
				}
			}
			else
			{
				archive_set_error(a,(int)http_code, "Finalize_Write_archive_to_url http code: %ld", http_code);
			}
		}
		if(headers_res.Location) free(headers_res.Location);
		if(headers_res.Docker_Content_Digest) free(headers_res.Docker_Content_Digest);
		if (url) free(url);
		if (cd->curl)curl_easy_cleanup(cd->curl);

		cd->curl=NULL;

		if (res!=CURLE_OK || http_code!=201 )
		{

			return ARCHIVE_FATAL;
		}

	}

	return 0;
}


int archive_close_callback_sha256(struct archive * a,void * userptr)
{
   client_data_t *cd=(client_data_t *)userptr;
   SHA256_Final(cd->hash, &cd->sha256);
   sha256_digest_string(cd->hash, cd->blobsha256);
   if (cd->fd_in!=-1)
   {
      close(cd->fd_in);
      cd->fd_in=-1;
   }
	if (cd->fd_out!=-1)
	{
		close(cd->fd_out);
		cd->fd_out=-1;
	}
   if(cd->buff)
   {
      free(cd->buff);
      cd->buff=NULL;
   }
   if (cd->Open_error)
   {
	   const char *errmsg=archive_error_string(a);
		if (errmsg)
		 debug("archive error=%s\n",archive_error_string(a));
		else
			 debug("archive error=%d , NULL on Error string\n",archive_errno(a));

	   //TODO need change !!! what if open file did not work, need more detail where error was created, not here!
      //archive_set_error(a,cd->Open_error, "Open for %s: %s", cd->Open_error_for,cd->error_msg);
   }

   if (cd->mode==output_mode_curl&& cd->curl)
   {
	   return(output_stream_curl_close(a,cd));

   }
	return 0;
}

const char * get_ugname(void * userp,int64_t gu)
{
	if (userp && gu)
   return (ROOT);
	else
		return (ROOT);
}
