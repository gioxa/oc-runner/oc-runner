/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/*! \curl_headers.c
 *  \brief helper callback to retrieve curl headers into cJSON object on the fly
 *  \author Created by Danny Goossen on  8/8/18.
 *  \copyright 2018, Danny Goossen. MIT License
 */

#include "curl_headers.h"
#include "cJSON_deploy.h"
#include "error.h"

void curl_headers_init( void *userp)
{
	struct curl_headers_struct * s_header=(struct curl_headers_struct*)userp;
	s_header->header=cJSON_CreateObject();
}

void curl_headers_free( void *userp)
{
	struct curl_headers_struct * s_header=(struct curl_headers_struct*)userp;
	if (s_header->header) cJSON_Delete(s_header->header);
	s_header->header=NULL;
}


size_t curl_headers_cb(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	struct curl_headers_struct * s_header=(struct curl_headers_struct*)userp;
	
	char * e=contents;
	int len=(int)realsize;
	int pos = 0;
	while (*e++ != ':' && pos < len) pos++;
	if (pos != len && pos >1)
	{
		if (memcmp(contents,"Content-Length",pos)==0)
		{
			debug("Received Header Content-Length\n");
			cJSON * tmp=NULL;
			char * errCheck;
			char * buf=cJSON_strdup_n(contents+pos+2, len-pos-4);
			int i = (int)strtol(buf, &errCheck,(10));
			free(buf);
			if(errCheck == buf)
				tmp=cJSON_CreateString_n(contents+pos+2, len-pos-4);
			else
			{
				tmp=cJSON_CreateNumber(i);
				debug("Received Header Content-Length= %d\n",i);
			}
			if (tmp) cJSON_AddItemToObject_n(s_header->header, contents, pos,tmp);
		}
		else
			cJSON_AddItemToObject_n(s_header->header, contents, pos, cJSON_CreateString_n(contents+pos+2, len-pos-4));
		
	}
	return realsize;
}
