//
//  jose_error.h
//  cjose_cjson
//
//  Created by Danny Goossen on 22/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

/**
* \file
* \brief
* Datatypes and functions for error reporting.
*
* Copyrights
*
* Portions created or assigned to Cisco Systems, Inc. are
* Copyright (c) 2014-2016 Cisco Systems, Inc.  All Rights Reserved.
*/

#ifndef __cjose_cjson__jose_error__
#define __cjose_cjson__jose_error__


#ifdef __cplusplus
extern "C" {
#endif
   
   /**
    * Temporarily disable compiler warnings, if possible (>=gcc-4.6).
    *
    * In some cases (particularly within macros), certain compiler warnings are
    * unavoidable.  In order to allow these warnings to be treated as errors in
    * most cases, these macros will disable particular warnings only during
    * specific points in the compilation.
    */
#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
#define GCC_END_IGNORED_WARNING _Pragma("GCC diagnostic pop")
   
#define GCC_BEGIN_IGNORED_WARNING_ADDRESS \
_Pragma("GCC diagnostic push");       \
_Pragma("GCC diagnostic ignored \"-Waddress\"")
#define GCC_END_IGNORED_WARNING_ADDRESS GCC_END_IGNORED_WARNING
#else
#define GCC_BEGIN_IGNORED_WARNING_ADDRESS
#define GCC_END_IGNORED_WARNING_ADDRESS
#endif /* defined(__GNUC__) && (__GNUC__ > 3) && (__GNUC_MINOR__ > 5) */
   
   /**
    * Enumeration of defined error codes.
    */
   typedef enum {
      /** No error */
      JOSE_ERR_NONE = 0,
      
      /** argument was invalid (beyond invariants) */
      JOSE_ERR_INVALID_ARG,
      
      /** context is not in a valid state */
      JOSE_ERR_INVALID_STATE,
      
      /** out of memory */
      JOSE_ERR_NO_MEMORY,
      
      /** an error returned from the crypto libraries */
      JOSE_ERR_CRYPTO,
      
   } jose_errcode;
   
   /**
    * An instance of an error context. Unlike other structures, it
    * is the API user's responsibility to allocate the structure; however
    * the values provided are considered constants, and MUST NOT be
    * deallocated.
    */
   typedef struct
   {
      /** The error code */
      jose_errcode code;
      
      /** The human readable message for the error code */
      const char *message;
      
      /** The function where the error occured, or "<unknown>"
       if it cannot be determined */
      const char *function;
      
      /** The file where the error occured */
      const char *file;
      
      /** The line number in the file where the error occured */
      unsigned long line;
      
   } jose_err;
   
   /**
    * Retrieves the error message for the given error code.
    *
    * \param code The error code to lookup
    * \retval const char * The message for {code}
    */
   const char *jose_err_message(jose_errcode code);
   
   /**
    * \def JOSE_ERROR(err, code)
    *
    * Macro to initialize an error context.
    *
    * \param err The pointer to the error context, or NULL if none
    * \param errcode The error code
    */
#define JOSE_ERROR(err, errcode)                      \
GCC_BEGIN_IGNORED_WARNING_ADDRESS                  \
if ((err) != NULL && (errcode) != JOSE_ERR_NONE)  \
{                                                  \
(err)->code = (errcode);                       \
(err)->message = jose_err_message((errcode)); \
(err)->function = __func__;                    \
(err)->file = __FILE__;                        \
(err)->line = __LINE__;                        \
}                                                  \
GCC_END_IGNORED_WARNING_ADDRESS
   
#ifdef __cplusplus
}
#endif

#endif /* defined(__cjose_cjson__jose_error__) */
