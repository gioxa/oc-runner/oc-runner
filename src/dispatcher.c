/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! @file executer.c
 *  @brief main of oc_executer
 *  @author danny@gioxa.com
 *  @date 15/10/17
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

#include "deployd.h"
#include "registry.h"
#include <ares.h>
#include "c-ares.h"
#include "virgin.h"

/*! \var parameters
    \brief global settings parameters
    \note defined here so signal handler can access it.
 */
static parameter_t * parameters=NULL;

/*! \fn static void signal_handler(sig)
    \brief signal handler for gracefull shutdown
    \param sig signal
 */
static void signal_handler(int sig);

/*! \fn static void init_signals(void)
 * \brief initialize signal redirection
 */
static void init_signals(void);

/*! \fn static void show_usage( int exitcode)
 *  \brief print the program usage on the screen
 *  \param exitcode program exitcode
 */
static void show_usage( int exitcode);

/*! \fn static int process_options(int argc, char **argv, parameter_t *parameter)
 *  \brief process program options into parameters
 *  \param argc parameter count
 *  \param argv program arguments
 *  \param parameter global parameter sttings of dispatcher
 *  \return -1 on errors
 */
static int process_options(int argc, char **argv, parameter_t *parameter);

/*! \fn static void set_missing_parms(parameter_t *parameter)
    \brief set's missing parameters to default values
    \param parameter dispatcher settings
*/
static void set_missing_parms(parameter_t *parameter);


// ********************************************************************
//          MAIN
// ********************************************************************

/*! \fn int main(int argc, char ** argv)
    \brief main
 */
int main(int argc, char ** argv, char ** envp)
{
	
	
   parameters= calloc(sizeof(parameter_t),1);
   parameters->shell_exec=0;
	if ((optind=process_options(argc, argv,parameters)) < 0)
   {
      error(" *** ERRORin command line options, exit \n");
      return (-1);
   }
   if (argc > 1 && optind < argc) {
      alert("ignoring non option arguments\n");
   }
	if (parameters->pipe_env)
	{
		cJSON * env_json= cJSON_Create_env_obj(envp);
		char * envjsonstr=cJSON_Print(env_json);
		
		FILE * pipe=fdopen((parameters->pipe_env-1), "w");
		fwrite(envjsonstr, 1, strlen(envjsonstr), pipe);
		fclose(pipe);
		exit (parameters->exit);
	}
	else if (parameters->pipe_pwd)
	{
		char * envjsonstr=NULL;
		char path[PATH_MAX];
		asprintf(&envjsonstr, "{ \"PWD\":\t\"%s\" }\n",getcwd(path, PATH_MAX));
		FILE * pipe=fdopen((parameters->pipe_pwd-1), "w");
	
		fwrite(envjsonstr, 1, strlen(envjsonstr), pipe);
		fclose(pipe);
		exit (parameters->exit);
	}
	alert("start main dispatcher\n");
	if (argv[0][0]=='/')
	{
		asprintf(&parameters->commandpath, "%s",argv[0]);
	}
	else
	{
		char path[PATH_MAX];
		asprintf(&parameters->commandpath, "%s/%s",getcwd(path, PATH_MAX),argv[0]);
	}
	printf("Command_path=%s\n",parameters->commandpath);
   set_missing_parms(parameters);
   // parse environment regarding debug
	
	
   if (parameters->verbose) setverbose();
   if (parameters->debug) setdebug();

   init_signals();
   thread_setup(); //TODO move this to main
   curl_global_init(CURL_GLOBAL_ALL); //TODO move this to main
#ifdef run_local
   git_libgit2_init();
#endif
#ifdef __APPLE__
   ares_library_init(ARES_LIB_INIT_ALL );
#endif
	alert("oc-dispatcher version %s.\n",PACKAGE_VERSION);
	vmkdir("/bin-runner/rootfs/builds/project_dir");
	chmod("/bin-runner/rootfs/builds", 0770);
	chmod("/bin-runner/rootfs/builds/project_dir", 0770);
	
	vmkdir("/bin-runner/rootfs/etc");
	chmod("/bin-runner/rootfs/etc", 0770);
	
	vmkdir("/bin-runner/image");
	
   alert("\nmoving ca-bundle.crt in place\n");
   char cert_file_dest[1024];
   snprintf(cert_file_dest, 1024, "/bin-runner/rootfs%s",DEFAULT_CA_BUNDLE);
   vmkdir( "/bin-runner/rootfs%s",DEFAULT_CA_BUNDLE_DIR);

	
   int size_file=0;
   if ((size_file= cp_file("/etc/pki/tls/certs/ca-bundle.crt", cert_file_dest))<=0)
      error("\nERROR: copying ca-bundle.crt\n");
   else
   {
      alert (",copied ca-bundle.crt, %d bytes\n",size_file);
      if ((size_file= append_file_2_file("/var/run/secrets/kubernetes.io/serviceaccount/ca.crt", cert_file_dest))<0)
         error("\nERROR: copying serviceaccount/ca.crt\n");
      else
      {
         alert (",append serviceaccount/ca.crt, %d bytes\n",size_file);
         
         if ((size_file= append_file_2_file("/var/run/secrets/kubernetes.io/serviceaccount/service-ca.crt", cert_file_dest))<0)
            error("\nERROR: copying serviceaccount/service-ca.crt\n");
         else
         {
            alert (",append serviceaccount/service-ca.crt, %d bytes\n",size_file);
         }
      }
   }
   //add OC_REGISTRY_CERT
   
   char * oc_registry_cert=getenv("OC_REGISTRY_CERT");
   if (oc_registry_cert)
   {
      if ((size_file= append_data_2_file(oc_registry_cert,  cert_file_dest))<=0)
      	error("\nERROR: appending OC_REGISTRY_CERT\n");
      else
      {
         alert (",append HELP_REGISTRY_CERT, %d bytes\n",size_file);
      }
      
   }
   
   alert("\nmoving oc-executer in place");
 
   if ((size_file= cp_file("/oc-executer", "/bin-runner/rootfs/oc-executer"))<=0)
      error("\nERROR: copying oc-executer\n");
   else
      alert (",copied %d bytes\n",size_file);
   chmod("/bin-runner/rootfs/oc-executer", 0755);
   
	// add passwd, for now take same uid as dispatcher
   // TODO put this in oc_do_job, with uid that we're going to spin of
   
   char * token=read_a_file_v( "%s/token",parameters->service_dir);
   char *namespace=read_a_file_v("%s/namespace",parameters->service_dir);

	char * master_url=NULL;
	const char * master_url_env=getenv("OC_MASTER_URL");
	if (master_url_env)
		master_url=strdup(master_url_env);
	else
		master_url=strdup("https://openshift.default.svc.cluster.local:443");
	
   snprintf(cert_file_dest, 1024, "/bin-runner/rootfs%s",DEFAULT_CA_BUNDLE);
   char hostname[1024];
   gethostname(hostname, 1024);
   uid_t start_uid=0;
   uid_t range_uid=1;
   char * scc=NULL;
   struct oc_api_data_s *oc_api_data=NULL;
   // init oc_api
   debug ("checking api uid range and scc\n");
   oc_api_init(&oc_api_data, cert_file_dest, master_url, token);
   oc_api_get_restrictions(oc_api_data,namespace, hostname, &start_uid, &range_uid, &scc,NULL);
	
   
   char  passwd_data[1024];
   snprintf(passwd_data, 1024, "root:x:0:0:root:/root:/bin/bash\nnonroot:x:%d:0:non-root:/builds:/bin/bash\n",start_uid);
   
   write_file_v(passwd_data, strlen(passwd_data), "/bin-runner/rootfs/etc/passwd");
   chmod("/bin-runner/rootfs/etc/passwd", 0774);
   
	void * trace=NULL;
	init_dynamic_trace(&trace, "", "", 0, NULL);
//	int res=rootfs_2_image(NULL,trace,5,(char *[]){"rootfs_2_image","--rootfs=/bin-runner/rootfs","--image_dir=/bin-runner/image","--layer=layer.tar.gz","--content=."});
//	debug("result rootfs_2layer: %d\n",res);
	//extract layer.tar.gz.digest
	
	
	int res=rootfs_2_archive(trace, "/bin-runner/image", "/bin-runner/rootfs", parameters->digest_tar, parameters->digest_tar_gz, &parameters->layer_size, "layer.tar.gz");
	debug("result rootfs_2layer: %d\n",res);
	
	char * ct =create_config_json_file(parameters->digest_tar, & parameters->config_size,  parameters->digest_config, "/bin-runner/image/config.json");
	debug("result config: %s\n",ct);
	if (ct) free(ct);
	ct=create_manifest_json( parameters->digest_tar_gz,  parameters->digest_config,  parameters->layer_size,  parameters->config_size, "/bin-runner/image/manifest.json");
	debug("result manifest: %s\n",ct);
	if (ct) free(ct);
	
	char * ImageStream_domain=NULL;
	char * registry_ip=getip("docker-registry.default.svc.cluster.local");
	char * tmp_is=getenv("OC_REGISTRY_IP");
	if (tmp_is)
	{
		ImageStream_domain=strdup(tmp_is);;
	}
	else if (registry_ip)
	{
		ImageStream_domain=calloc(1,strlen(registry_ip)+1+5+1);
		sprintf(ImageStream_domain,"%s:%d", registry_ip,5000);
	}
	else
	{
		ImageStream_domain=strdup("172.30.1.1:5000");
	}
	if (registry_ip) free(registry_ip);
	
	int ImageStream_Insecure=0;
	
	int ImageStream_v1Only=0;
	char * is_registry_v1only=getenv("OC_REGISTRY_V1only");
	if (is_registry_v1only)
	{
		debug("OC_REGISTRY_V1only=%s\n",is_registry_v1only);
		char * tmp=strdup(is_registry_v1only);
		is_registry_v1only=tmp;
		
		lower_string(is_registry_v1only);
		if(strcmp(is_registry_v1only,"true")==0 || strcmp(is_registry_v1only,"yes")==0 || strcmp(is_registry_v1only,"1")==0)
			ImageStream_v1Only=1;
		free(is_registry_v1only);
		is_registry_v1only=NULL;
	}
	int ImageStream_v2support=0;
	char * is_registry_v2support=getenv("OC_REGISTRY_V2support");
	if (is_registry_v2support)
	{
		debug("OC_REGISTRY_V2support=%s\n",is_registry_v2support);
		char * tmp=strdup(is_registry_v2support);
		is_registry_v2support=tmp;
		
		lower_string(is_registry_v2support);
		if(strcmp(is_registry_v2support,"true")==0 || strcmp(is_registry_v2support,"yes")==0 || strcmp(is_registry_v2support,"1")==0)
			ImageStream_v2support=1;
		free(is_registry_v2support);
		is_registry_v2support=NULL;
	}
	
	struct dkr_api_data_s * apidata=NULL;
	
	char * this_executer_image=NULL;
	dkr_api_init(&apidata, cert_file_dest); // DEFAULT_CA_BUNDLE);
		
	int reg_check=dkr_api_check_registry_secure( ImageStream_domain, cert_file_dest,trace);
	if (reg_check==1)
	{
		ImageStream_Insecure=1;
		res=0;
	}
	else if (reg_check==0)
	{
		ImageStream_Insecure=0;
		res=0;
	}
	else
	{
		ImageStream_Insecure=0;
		res=-1;
	}
    if (ImageStream_Insecure && !res)
		setenv("OC_REGISTRY_INSECURE", "true", 1);
	
    // add ImageStream registry
	int add_reg_res=dkr_api_add_registry(apidata, ImageStream_domain, NULL,"",token, NULL,ImageStream_Insecure);
	
	debug("%d\n",add_reg_res);
	
	res=oc_api(oc_api_data, "GET", NULL, NULL, "/oapi/v1/namespaces/%s/imagestreams/%s",namespace,"is-executer-image");
	debug("result imagestream GET %d \n",res);
	if (res==404)
	{
		debug("make imagestream template\n");
		cJSON * imagestream=make_imagestream_template( namespace,"is-executer-image");
		if (imagestream)
		{
			res=oc_api(oc_api_data, "POST", imagestream, NULL, "/oapi/v1/namespaces/%s/imagestreams",namespace);
			debug("result imagestream POST %d ",res);
		}
	}
	
	debug("ready to push is-imagestream : res=%d\n",res);
	asprintf(&this_executer_image, "%s/%s/is-executer-image",ImageStream_domain,namespace);

#ifndef __APPLE__
	if (res==0 || res==201 || res==200 ) // TODO but first need more implementation downstream&& !layers_pull)
	{
		debug("image_2_IS: is-scratch\n");
		res=image_2_ISR_int(apidata, trace, this_executer_image,"/bin-runner/image",ImageStream_v1Only);
		debug("back from push image %d\n",res);
		update_details(trace);
	}
#endif
	
	if ( !ImageStream_v1Only && !ImageStream_v2support)
	{
		cJSON * manifest=NULL;
		res=dkr_api_get_manifest(apidata, "application/vnd.docker.distribution.manifest.v2+json", this_executer_image, &manifest);
	    if (manifest)
		{
			res=0;
			cJSON * schema=cJSON_GetObjectItem(manifest, "schemaVersion");
			if (schema && cJSON_IsNumber(schema))
			{
				int schemaVerson=schema->valueint;
				if (schemaVerson==2)
				{
					setenv("OC_REGISTRY_V2support","true",1);
					unsetenv("OC_REGISTRY_V1only");
				}
				else
				{
					unsetenv("OC_REGISTRY_V2support");
					setenv("OC_REGISTRY_V1only","true",1);
				}
			}
			cJSON_Delete(manifest);
			manifest=NULL;
		}
		else debug("no manifest after upload\n");
	}
	if(ImageStream_domain) free(ImageStream_domain);
	if(master_url) free(master_url);
	if(token) free(token);
	if(namespace) free(namespace);
	ImageStream_domain=NULL;
	master_url=NULL;
	token=NULL;
	namespace=NULL;
	dkr_api_cleanup(&apidata);
	
	if (parameters->virgin)
	{
		alert ("starting virgin\n");
		res=virgin_process(oc_api_data,parameters);
		oc_api_cleanup(&oc_api_data);
		
		free_dynamic_trace(&trace);
		curl_global_cleanup(); //TODO move this to main
		thread_cleanup(); //TODO move this to main
#ifdef __APPLE__
		if (ARES_SUCCESS==ares_library_initialized()) ares_library_cleanup();
#endif
		alert("exit oc-dispatcher virgin code %d\n",res);
		return res;
	}
	
	oc_api_cleanup(&oc_api_data);
	dkr_api_cleanup(&apidata);
	free_dynamic_trace(&trace);
	
   runners(parameters);
   curl_global_cleanup(); //TODO move this to main
   thread_cleanup(); //TODO move this to main
#ifdef run_local
   git_libgit2_shutdown();
#endif
#ifdef __APPLE__
   if (ARES_SUCCESS==ares_library_initialized()) ares_library_cleanup();
#endif
   alert("exit oc-dispatcher\n");
   // TODO free parameters!!!

   return res;
}

static void signal_handler(sig)
int sig;
{
    alert ("\nEXIT: Signal %d : %s !!!\n",sig,strsignal(sig));
    switch(sig) {
        case SIGHUP:
        {
            parameters->hubsignal=1;
            debug ("set parameters->hubsignal\n ");
            break;
        }
        case SIGSEGV:
        {
            parameters->exitsignal=1;
            debug ("set parameters->exitsignal\n ");
            usleep(3000000);
            exit(-1);
        }
        case SIGSTOP:
        case SIGQUIT:
        case SIGINT:
        case SIGTERM:
        {
            parameters->exitsignal=1;
            debug ("set parameters->exitsignal\n ");
            break;
        }
    }
}

void init_signals()
{
    signal(SIGHUP,&signal_handler); /* catch hangup signal */
    signal(SIGTERM,&signal_handler); /* catch kill signal */
    signal(SIGINT,&signal_handler); /* catch kill signal */
    signal(SIGSTOP,&signal_handler); /* catch kill signal */
    signal(SIGQUIT,&signal_handler); /* catch kill signal */
    signal(SIGSEGV,&signal_handler); /* catch kill signal */
    
}
// add no return compiler deirective
static void show_usage( int exitcode)
{
    error("Usage: oc-dispatcher [--options]\n\n");
    error("     --options=value:\n");
    error("version        : Shows the version\n");
    error("help           : Shows the version and usage\n");
    error("verbose        : turns on verbose output mode\n");
    error("quiet          : quiet down output\n");
    error("debug          : debug output\n");
    error("daemon         : runs deployd in background as a service\n");
    error("exec           : disable daemon\n");
    //      error("sock_name=sock : specifies on which unix socket we're listening\n");
    error("pid=file.pid   : specifies the pid  file to be used when daemon\n");
    //      error("secret=secret  : specifies the secret command\n");
    //      error("timeout=10     : specifies the timeout in secondsfor a command\n");
    error("\n");
    error("Defaults: verbose    = %d\n",   VERBOSE_YN);
    error("\n");
    exit(exitcode);
}

static int process_options(int argc, char **argv, parameter_t *parameter)
{
    
    struct option long_options[] = {
        { "verbose",     0, NULL, 'v' },
        { "quiet",       0, NULL, 'q' },
        { "sock_name",   1, NULL, 'n' },
        { "daemon",      0, NULL, 'd' },
        { "exec",        0, NULL, 'e' },
        { "pid",         1, NULL, 'i' },
        { "help",        0, NULL, 'j' },
        { "version",     0, NULL, 'V' },
        { "secret",      1, NULL, 's' },
        { "debug",       0, NULL, 'D' },
        { "uid_user",    1, NULL, 'u' },
        { "gid_user",    1, NULL, 'g' },
		{ "local_shell", 0, NULL, 'w' },
        { "current_usr", 0, NULL, 'C' },
        { "config",      1, NULL, 'c' },
        { "prefix",      1, NULL, 'p' },
        { "timeout",     1, NULL, 't' },
		{ "pipe_env",    1, NULL, 'P' },
		{ "pipe_pwd",    1, NULL, 'Q' },
		{ "exit",        1, NULL, 'E' },
		{ "virgin",      0, NULL, 'I' },
		{ "service",     1, NULL, 'S' },
        { NULL,          0, NULL, 0   } };
    
    int           which;
    optind=1;  // reset if called again, needed for fullcheck as we call twice (server and main)
    // ref : http://man7.org/linux/man-pages/man3/getopt.3.html
    if (argc>1)
    {
        /* for each option found */
        while ((which = getopt_long(argc, argv, "+", long_options, NULL)) > 0) {
            
            /* depending on which option we got */
            switch (which) {
                    /* --verbose    : enter verbose mode for debugging */
                case 'v':
                {
                    if (parameter->quiet)
                    {
                        error("Invalid option, choose quiet or verbose\n");
                        return -1;
                    }
                    else
                    {
                        parameter->verbose = 1;
                    }
                }
                    break;
				case 'P':
			 {
					parameter->pipe_env=atoi(optarg)+1;
			 }
					break;
				case 'Q':
			 {
					parameter->pipe_pwd=atoi(optarg)+1;
			 }
					break;
				case 'E':
			 {
					parameter->exit=atoi(optarg) ;
			 }
					break;
                case 'q':
                {
                    if (parameter->verbose)
                    {
                        error("Invalid option, choose quiet or verbose\n");
                        return -1;
                    }
                    else
                        parameter->quiet = 1;
                }
                    break;
                    /* --v4         : disable IPv6 mode */
                    
                case 'p': {
                    parameter->prefix =malloc(strlen(optarg)+1);
                    sprintf(parameter->prefix,"%s",optarg);
                }
                    break;
               case 't': {
                    parameter->timeout   = atoi(optarg);
                }
                    break;
				case 'w': {
					parameter->shell_exec=1;
				}
					break;
                case 'D': {
                    parameter->debug = 1;
                }
					break;
				case 'I': {
					parameter->virgin = 1;
				}
					break;
                case 'c': {
                    parameter->config_file=calloc (strlen(optarg)+1,1);
                    sprintf(parameter->config_file,"%s",optarg);
                }
                    break;
				case 'S': {
					parameter->service_dir=calloc (strlen(optarg)+1,1);
					sprintf(parameter->service_dir,"%s",optarg);
				}
					break;
                case 'V':
                {
                    error("%s\n",GITVERSION);
                    exit(0);
                }
					
				case 'Z':
				{
					alert("OK\n");
					exit(0);
				}

                case 'j': {
                    error("\ndeloyd Git Version: %s\n \n",GITVERSION);
                    show_usage(0);
                }
                    /* otherwise    : display usage information */
                default:
                    ;
                    show_usage(1);
            }
        }
    }
    // if NULL then no option was given and we default define the dir where tblastd was started !!!!!!
    //value = getenv(name);
    return optind;
}

void set_missing_parms(parameter_t *parameter)
{
    parameter->hubsignal=0;
    parameter->exitsignal=0;
  
    if (parameter->config_file == NULL)
    {
        parameter->config_file=calloc(strlen(CONFIG_FILE)+1,1);
        sprintf(parameter->config_file,"%s",CONFIG_FILE);
    }
   if (parameter->prefix == NULL)
    {
        parameter->prefix=calloc(strlen(PREFIX)+1,1);
        sprintf(parameter->prefix,"%s",PREFIX);
    }
    if (parameter->ca_bundle == NULL)
    {
        parameter->ca_bundle=strdup("/etc/pki/tls/certs/ca-bundle.crt");
    }
    if (parameter->timeout==0) parameter->timeout  = TIME_OUT_CHILD;
    if (!parameters->service_dir)
		asprintf(&parameters->service_dir, "/var/run/secrets/kubernetes.io/serviceaccount");
    if (! parameter->verbose && !parameter->quiet) parameter->verbose = VERBOSE_YN;
    parameter->testprefix=calloc(strlen("")+1,1);
    sprintf(parameter->testprefix,"%s","");
    return;
}

