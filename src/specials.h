//
//  specials.h
//  oc-runner
//
//  Created by Danny Goossen on 16/10/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __oc_runner__specials__
#define __oc_runner__specials__

#include "common.h"

/**
 \brief change all non a-z, A-Z , - , 0-9 characters to -, with max len of 63, no sequential -, no start nor stop with -
 */
char * slug_it(const char * input_str, size_t len);

/**
 \brief resolve variables used in image
 */
int resolve_image_var(char ** image,const cJSON * env_vars);

/**
 \brief create a full image path name
 */
char * process_image_nick(char ** image,const cJSON * env_vars,const char * Imagestream_ip, const char * oc_name_space,const char * docker_registry);


/**
 \brief change image string tag to a given tag
 */
int change_image_tag(char ** image, const char *new_tag);

/**
 \brief chop image into \b registry_name / \b namespace / \b and reference
 \param image the image name to parse
 \param registry_name the registry
 \param namespace name
 \param reference ref tag
 \return 0 on success, I guess
 */
int chop_image(const char *image,char ** registry_name,char **namespace,char **reference);

/**
 \brief chop namespace and reverse last 2 items and slugit
 \param namespace name
 \returns refname on success, NULL on failure
 \note client responsible for freeing memory
 */
char * name_space_rev_slug(const char * namespace);

/**
 \brief chop namespace and reverse last 2 items
 \param namespace name
 \returns refname on success, NULL on failure
 \note client responsible for freeing memory
 */
char * name_space_rev(const char * namespace);

/**
 \brief loop file string recursivly in reverse and if a dir path has not been added, do a call fn(void * userp,char *path)
 */
void recursive_dir_extract(const char * file, size_t len, cJSON * list, void(*fn)(void * userp,char *path),void * userp );

/**
 \brief create an cJSON array from a comma separated list
 */
cJSON * comma_sep_list_2_json_array(const char * list);


/**
 \brief check if an env var would indicate true (case insensitive)
 \param name environment variable
 \returns 1 on true
 */
int env_var_is_true(const char *name);

/**
 \brief check if an env var would indicate false (case insensitive)
 \param name environment variable
 \returns 1 on false
 */
int env_var_is_false(const char *name);

char * resolve_const_vars(const char * s,const cJSON * env_vars);
#endif /* defined(__oc_runner__specials__) */
