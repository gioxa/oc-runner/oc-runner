/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! \file autolabel.c
 *  \brief all functions for autolabeling of images
 *  \author Created by Danny Goossen on  29/7/18.
 *  \copyright 2018, Danny Goossen. MIT License
 *
 */

#include "autolabel.h"
#include "deployd.h"
#include "cJSON_deploy.h"
#include "docker_config.h"
#include "image_parameters.h"

void parse_faulty_centos_labels(cJSON * labels)
{
	const char *schemaversion=cJSON_get_key(labels, "org.label-schema.schema-version");
	if (
		 !schemaversion ||
		 strlen(schemaversion)<6 ||
	 	 strncmp(schemaversion, "= 1.0 ", 6)!=0 ||
		 strstr(schemaversion,"org.label-schema.")==NULL
		)
	{
		return;
	}
	// work to do
	char * walk= strstr(schemaversion,"org.label-schema.");
	char * next_walk=NULL;
	while (walk)
	{
		next_walk=strstr(walk+1,"org.label-schema.");
		char * eq=strchr(walk,'=');
		if (eq)
		{
			size_t len_data=0;
			if (next_walk) len_data=(next_walk-1)-(eq+1);
			else len_data=&walk[strlen(walk)]-(eq+1);
			size_t len_key=eq-walk;
			char* key=strndup(walk,len_key);
			char* value=strndup(eq+1, len_data);
			char *p=NULL;
			while ((p=memrchr(value, ' ', len_data))==&value[strlen(value)-1])
				p[0]=0;
			if (value && key)
			{
				cJSON_add_string(labels, key, value);
			}
			if (value) free(value);
			if (key) free(key);
		}
		else break;
		walk=next_walk;
	}
	cJSON_add_string(labels, "org.label-schema.schema-version", "1.0");
}

#define unsetifzero(x) { if (x && strlen(x)==0) { free(x); x==NULL;}}

void autolabel(cJSON**docker_config ,const cJSON * job,dkr_parameter_t * parameters_from)
{
	debug("autolabels: start\n");
	cJSON * oc_docker_config=cJSON_CreateObject();
	cJSON * oc_docker_config_labels=cJSON_CreateObject();
	cJSON_AddItemToObject(oc_docker_config,"Labels", oc_docker_config_labels);
	
	if (parameters_from && parameters_from->image)
	{
		cJSON_add_string(oc_docker_config_labels,"io.oc-runner.build.from.docker-repo",parameters_from->image);
		cJSON_add_string(oc_docker_config_labels,"io.oc-runner.build.from.nick-name",parameters_from->image_nick);
		cJSON_add_string(oc_docker_config_labels,"io.oc-runner.build.from.tag",parameters_from->reference);
		cJSON_add_string(oc_docker_config_labels,"io.oc-runner.build.from.digest",parameters_from->image_sha);
	}
	
	cJSON * env_vars=cJSON_GetObjectItem(job, "env_vars");
	print_json(env_vars);
	const char * commit_sha=cJSON_get_key(env_vars,"CI_COMMIT_SHA");
	
	char * git_commit=NULL;
	char * refname=slug_it(cJSON_get_key(env_vars,"OC_INFO_IMAGE_REFNAME"),0);
	char * git_version=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_VERSION");
	
	char * image_title=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_TITLE");
	char * vendor=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_VENDOR");
	char * url=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_URL");
	char * documentation=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_DOCUMENTATION");
	char * description=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_DESCRIPTION");
	char * authors=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_AUTHORS");
	char * licenses=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_LICENSES");
	if (!licenses)
		licenses=cJSON_get_stringvalue_dup(env_vars,"OC_INFO_IMAGE_LICENSE");
	
	unsetifzero(refname);
	unsetifzero(git_version);
	unsetifzero(image_title);
	unsetifzero(vendor);
	unsetifzero(url);
	unsetifzero(documentation);
	unsetifzero(description);
	unsetifzero(authors);
	unsetifzero(licenses);
	
	if (commit_sha) git_commit=cJSON_strdup_n((const unsigned char*)commit_sha, 7);
	
	cJSON_add_string_v(oc_docker_config_labels,"io.oc-runner.build.commit.author", "%s <%s>",cJSON_get_key(env_vars, "GITLAB_USER_NAME"),cJSON_get_key(env_vars,"GITLAB_USER_EMAIL" ));
	
	cJSON_add_string(oc_docker_config_labels,     "io.oc-runner.build.commit.id",git_commit );
	cJSON_add_string_from_object(oc_docker_config_labels,     "io.oc-runner.build.commit.message",env_vars,"CI_COMMIT_MESSAGE");
	cJSON_add_string_from_object(oc_docker_config_labels,     "io.oc-runner.build.commit.ref",env_vars,"CI_COMMIT_REF_NAME");
	cJSON_add_string_from_object(oc_docker_config_labels,     "io.oc-runner.build.commit.tag",env_vars,"CI_COMMIT_TAG");
	
	
	
	cJSON_add_string_from_object(oc_docker_config_labels,     "io.oc-runner.build.environment",env_vars,"CI_ENVIRONMENT_NAME");
	cJSON_add_string_v(oc_docker_config_labels,   "io.oc-runner.build.job-url","%s/-/jobs/%s",cJSON_get_key(env_vars, "CI_PROJECT_URL"),cJSON_get_key(env_vars, "CI_JOB_ID") );
	
	cJSON_add_string_from_object(oc_docker_config_labels, "io.oc-runner.build.image",env_vars, "OC_BUILD_IMAGE");
	cJSON_add_string_from_object(oc_docker_config_labels, "io.oc-runner.build.image-nick",env_vars, "OC_BUILD_IMAGE_NICK");
	cJSON_add_string_from_object(oc_docker_config_labels, "io.oc-runner.build.image-tag",env_vars, "OC_BUILD_IMAGE_TAG");
	cJSON_add_string_from_object(oc_docker_config_labels, "io.oc-runner.build.image-digest",env_vars, "OC_BUILD_IMAGE_SHA");
	
	cJSON_add_string_v(oc_docker_config_labels,"io.oc-runner.build.source.tree","%s/tree/%.*s",cJSON_get_key(env_vars, "CI_PROJECT_URL"),8,cJSON_get_key(env_vars, "CI_COMMIT_SHA"));
	
	cJSON_add_string(oc_docker_config_labels,"io.oc-runner.builder-version", PACKAGE_VERSION);
	
	cJSON * dc_labels=NULL;
	if (docker_config && *docker_config)
	{
		dc_labels=cJSON_GetObjectItem(*docker_config, "Labels");
	}
	
	
	if (dc_labels)
	{
		if (!image_title)
			image_title=cJSON_get_stringvalue_dup(dc_labels, "org.opencontainers.image.title");
		if (!image_title)
			image_title=cJSON_get_stringvalue_dup(dc_labels, "name");
		if (!image_title)
			image_title=cJSON_get_stringvalue_dup(dc_labels, "title");
		
		if (!refname)
			refname=slug_it(cJSON_get_key(dc_labels, "name"), 0);
		
		if(!git_version)
			git_version=cJSON_get_stringvalue_dup(dc_labels, "org.opencontainers.image.version");
		
		if (!git_version)
			git_version=cJSON_get_stringvalue_dup(dc_labels, "version");
		if(!vendor)
			vendor=cJSON_get_stringvalue_dup(dc_labels, "org.opencontainers.image.vendor");
		if(!vendor)
			vendor=cJSON_get_stringvalue_dup(dc_labels, "vendor");
		if (!authors)
			authors=cJSON_get_stringvalue_dup(dc_labels, "org.opencontainers.image.authors");
		if(!authors)
			authors=cJSON_get_stringvalue_dup(dc_labels, "authors");
		if(!authors)
			authors=cJSON_get_stringvalue_dup(dc_labels, "maintainer");
		if(!description)
			description=cJSON_get_stringvalue_dup(dc_labels, "org.opencontainers.image.description");
		if(!description)
			description=cJSON_get_stringvalue_dup(dc_labels, "description");
		if (!url)
			url=cJSON_get_stringvalue_dup(dc_labels, "org.opencontainers.image.url");
		if(!url)
			url=cJSON_get_stringvalue_dup(dc_labels, "url");
		if (!documentation)
			documentation=cJSON_get_stringvalue_dup(dc_labels, "org.opencontainers.image.documentation");
		if(!documentation)
			documentation=cJSON_get_stringvalue_dup(dc_labels, "documentation");
		if(!documentation)
			documentation=cJSON_get_stringvalue_dup(dc_labels, "usage");
		if(!licenses)
			licenses=cJSON_get_stringvalue_dup(dc_labels, "org.opencontainers.image.licenses");
		if(!licenses)
			licenses=cJSON_get_stringvalue_dup(dc_labels, "licenses");
		if(!licenses)
			licenses=cJSON_get_stringvalue_dup(dc_labels, "license");
	}
	
	unsetifzero(refname);
	unsetifzero(git_version);
	unsetifzero(image_title);
	unsetifzero(vendor);
	unsetifzero(url);
	unsetifzero(documentation);
	unsetifzero(description);
	unsetifzero(authors);
	unsetifzero(licenses);
	
	if (!refname)
	{
		const char * nr=cJSON_get_key(env_vars, "CI_PROJECT_PATH");
		refname=name_space_rev_slug(nr);
		debug("\n***************\nCI_PROJECT_PATH=%s, refname=%s\n\n",nr,refname);
	}
	if (!image_title)
		image_title=cJSON_get_stringvalue_dup(env_vars, "OC_INFO_IMAGE_REFNAME");
	if (!image_title)
		image_title=name_space_rev(cJSON_get_key(env_vars, "CI_PROJECT_PATH"));
	
	
	if (!git_version)
		git_version=cJSON_get_stringvalue_dup(env_vars,"VERSION");
	if (!git_version)
		git_version=read_a_file_v(".VERSION");
	if (!git_version)
		git_version =cJSON_get_stringvalue_dup(env_vars,"OC_GITVERSION");
	// Get GitVersion
	if (!git_version)
		git_version =cJSON_get_stringvalue_dup(env_vars,"OC_GIT_VERSION");
	if (!git_version)
		git_version=cJSON_get_stringvalue_dup(env_vars,"GITVERSION");
	if (!git_version)
		git_version=cJSON_get_stringvalue_dup(env_vars,"GIT_VERSION");
	if(!git_version)
		git_version=read_a_file_v(".GITVERSION");
	if(!git_version)
		git_version=read_a_file_v(".GIT_VERSION");
	if(!git_version)
		git_version=read_a_file_v(".tarball-version");
	
	
	debug("remove newlines\n");
	
	// Remove posible newlines created by using echo

	strip_nl(git_version);
	
	debug("Read Authours file if not authors\n");
	// read from project
	if (!authors)
	{
		authors=read_a_file_v("%s/AUTHORS",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
		if (authors)
		{
			// flatten AUTHORS
			char *walk=authors;
			char *end=authors+ strlen(authors);
			while (walk!=end)
			{
				if (walk[0]=='\n') walk[0]=';';
				if (walk[0]=='\r') walk[0]=' ';
				if (walk[0]=='\t') walk[0]=' ';
				walk++;
			}
		}
	}
	//set labels : org.opencontainers.image
	debug("Set Labels accoeding what we have\n");
	cJSON_AddTimeStampToObject(oc_docker_config_labels,"org.opencontainers.image.created");
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.url",url);
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.version",  git_version);
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.revision",    git_commit);
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.vendor", vendor);
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.title",       image_title);
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.description",description);
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.documentation", documentation);
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.authors",authors);
	cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.licenses",    licenses);
	cJSON_add_string_from_object(oc_docker_config_labels,"org.opencontainers.image.source",    env_vars, "CI_PROJECT_URL");
	debug("Set ref.name?\n");
	if (git_version && refname)
		cJSON_add_string_v(oc_docker_config_labels,"org.opencontainers.image.ref.name", "%s-%s",refname,git_version);
	
	// cleanup

	if (git_commit)    free(git_commit);
	if (image_title)   free(image_title);
	if (refname)       free(refname);
	if (git_version)   free(git_version);
	if (url)           free(url);
	if (documentation) free(documentation);
	if (description)   free(description);
	if (authors)       free(authors);
	
;
	debug ("merge auto user with config:\n");
	docker_merge_config(docker_config, oc_docker_config);
	debug ("merged config:\n");
	if (docker_config) {print_json(*docker_config);}
	cJSON_Delete(oc_docker_config);
	debug("success autolabel!\n");
}

int autolabel_need_v1(cJSON*manifest_config ,const cJSON * manifest,const char * image)
{
	char * rn=NULL;
	char * ns=NULL;
	char * ir=NULL;
	int need_v1=0;
	
	int schemaVersion=cJSON_safe_GetNumber(manifest, "schemaVersion");
	cJSON* config_config=cJSON_GetObjectItem(manifest_config, "config");
	cJSON * Labels=cJSON_GetObjectItem(config_config, "Labels");
	debug("label auto, no processing\n");
	print_json(Labels);

		const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.version");
		const char *item_os=cJSON_get_key(Labels, "org.label-schema.version");
		const char *item_ns=cJSON_get_key(Labels, "version");
	if (!item_oi && !item_os && !item_ns)
	{
		if (image) chop_image(image, &rn, &ns, &ir);
		if (!(ir && strcmp(ir,"latest")!=0) && schemaVersion!=1)
			need_v1=1;
	}
	if (ns) free(ns);
	ns=NULL;
	if (ir) free(ir);
	ir=NULL;
	if (rn) free(rn);
	rn=NULL;
	return need_v1;
}

void autolabel_update(cJSON*manifest_config ,const cJSON * manifest,const char * image)
{
	char * rn=NULL;
	char * ns=NULL;
	char * ir=NULL;
	char *version=NULL;
	chop_image(image, &rn, &ns, &ir);
	int deducted_version=0;
	int schemaVersion=cJSON_safe_GetNumber(manifest, "schemaVersion");
	cJSON* config_config=cJSON_GetObjectItem(manifest_config, "config");
	cJSON * Labels=cJSON_GetObjectItem(config_config, "Labels");
	debug("autolabel_update start\n");
	print_json(Labels);
	if (!Labels)
	{
		Labels=cJSON_CreateObject();
		cJSON_AddItemToObject(config_config, "Labels", Labels);
	}
	if (Labels)
	{
		debug("label auto, before centos parsing\n");
		print_json(Labels);
		parse_faulty_centos_labels(Labels);
		debug("label auto, after centos parsing\n");
		print_json(Labels);
		{ // Name
			
			const char *name_oi=cJSON_get_key(Labels, "org.opencontainers.image.title");
			const char *name_ol=cJSON_get_key(Labels, "org.label-schema.name");
			const char *name_ns=cJSON_get_key(Labels, "name");
			if (!name_oi && !name_ol && !name_ns)
			{
				// no info
			} else if (!name_oi && !name_ol)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.title",name_ns);
				
			}
			else if (!name_oi && !name_ns)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.title",name_ol);
				
			}
		}
		{ // Build-date
			char *item=NULL;
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.created");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.build-date");
			const char *item_ns=cJSON_get_key(Labels, "build-date");
			if (!item_oi && !item_os && !item_ns)
			{
				if(schemaVersion==2)
					item=cJSON_get_stringvalue_dup(manifest_config, "created");
				else if(schemaVersion==1)
					item=cJSON_get_stringvalue_dup(manifest, "created");
				cJSON_add_string(Labels, "org.opencontainers.image.created",item);
			}else if (!item_oi && !item_os)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.created",item_ns);
			}
			else if (!item_oi && !item_ns)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.created",item_os);
		    }
		}
		{ //vendor
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.vendor");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.vendor");
			const char *item_ns=cJSON_get_key(Labels, "vendor");
			if (!item_oi && !item_os && !item_ns)
			{
				// no info
			} else if (!item_oi && !item_os)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.vendor",item_ns);
			}else if (!item_oi)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.vendor",item_os);
			}
		}
		{ //license
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.licenses");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.license");
			const char *item_ns=cJSON_get_key(Labels, "license");
			if (!item_oi && !item_os && !item_ns)
			{
				
				// no info
			} else if (!item_oi && !item_os)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.licenses",item_ns);
			}else if (!item_oi && !item_ns)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.licenses",item_os);
				
			}
		}
		
		{ //config::author/ config::Labels::maintainer / config::Labels::org.opencontainers.image.authors
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.authors");
			const char *item_cc=cJSON_get_key(Labels, "maintainer");
			const char *item_mc=cJSON_get_key(manifest_config, "author");
			if (!item_oi && !item_cc && !item_mc)
			{
				// no info
			}
			else if (!item_oi && !item_cc)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.authors",item_mc);
			}
			else if (!item_oi && !item_mc)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.authors",item_cc);
			}
		}
		
		{ //version <=> version
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.version");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.version");
			const char *item_ns=cJSON_get_key(Labels, "version");
			if (!item_oi && !item_os && !item_ns)
			{
				if (ir && strcmp(ir,"latest")!=0)
				{
					cJSON_add_string(Labels, "org.opencontainers.image.version",ir);
					deducted_version=1;
				}
				else
				{
					if(schemaVersion==1 && cJSON_get_key(manifest, "tag"))
					{
						cJSON_add_string(Labels, "org.opencontainers.image.version",cJSON_get_key(manifest, "tag"));
						deducted_version=1;
					}
				}
				
				// no info
			} else if (!item_oi && !item_os)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.version",item_ns);
			
			}else if (!item_oi && !item_ns)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.version",item_os);
			}
			version=cJSON_get_stringvalue_dup(Labels, "org.opencontainers.image.version");
		}
		
		{//vcs-url <=> source
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.source");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.vcs-url");
			const char *item_ns1=cJSON_get_key(Labels, "vcs-url");
			const char *item_ns2=cJSON_get_key(Labels, "source");
			if (!item_oi && !item_os && !item_ns1 && !item_ns2)
			{
				
				// no info
			} else if (!item_oi && !item_os)
			{
				if(item_ns1)
				{
					cJSON_add_string(Labels, "org.opencontainers.image.source",item_ns1);
				}
				if(item_ns2)
				{
					cJSON_add_string(Labels, "org.opencontainers.image.source",item_ns2);
				}
			}else if (!item_oi)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.source",item_os);
			}
		}
		
		{ //vcs-ref <=> revision  :: git-commit
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.revision");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.vcs-ref");
			const char *item_ns=cJSON_get_key(Labels, "git-commit");
			if (!item_oi && !item_os && !item_ns)
			{
				
				// no info
			} else if (!item_oi && !item_os)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.revision",item_ns);
				
			}else if (!item_oi)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.revision",item_os);
			}
		}
		
		{ //url     >=> url
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.url");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.url");
			const char *item_ns=cJSON_get_key(Labels, "url");
			if (!item_oi && !item_os && !item_ns)
			{
				
				// no info
			} else if (!item_oi && !item_os)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.url",item_ns);
			}else if (!item_oi)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.url",item_os);
				
			}
		}
		
		
		{ //usage =>documentation only if start with http
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.documentation");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.usage");
			const char *item_ns2=cJSON_get_key(Labels, "usage");
			const char *item_ns1=cJSON_get_key(Labels, "documentation");
			if (!item_oi && !item_os && !item_ns1 & !item_ns2)
			{
				
				// no info
			} else if (!item_oi && !item_os && item_ns1)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.documentation",item_ns1);
				
			}else if (!item_oi && item_os && strstr(item_os,"http")==item_os) // only if it starts with http
			{
				cJSON_add_string(Labels, "org.opencontainers.image.documentation",item_os);
				
			}
			else if (!item_oi && item_ns2 && strstr(item_ns2,"http")==item_ns2) // only if it starts with http
			{
				cJSON_add_string(Labels, "org.opencontainers.image.documentation",item_ns2);
				
			}
			
		}
		
		
		{ //description
			const char *item_oi=cJSON_get_key(Labels, "org.opencontainers.image.description");
			const char *item_os=cJSON_get_key(Labels, "org.label-schema.description");
			const char *item_ns=cJSON_get_key(Labels, "description");
			if (!item_oi && !item_os && !item_ns)
			{
				
				// no info
			} else if (!item_oi && !item_os)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.description",item_ns);
				
			}else if (!item_oi)
			{
				cJSON_add_string(Labels, "org.opencontainers.image.description",item_os);
				
			}
		}
		
		cJSON_DeleteItemFromObject(Labels, "name");
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.name");
		
		cJSON_DeleteItemFromObject(Labels, "build-date");
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.build-date");
		
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.vendor");
		cJSON_DeleteItemFromObject(Labels, "vendor");

		cJSON_DeleteItemFromObject(Labels, "license");
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.license");
		
		cJSON_DeleteItemFromObject(Labels, "maintainer");
		cJSON_DeleteItemFromObject(manifest_config, "author");
		
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.version");
		cJSON_DeleteItemFromObject(Labels, "version");
		
		cJSON_DeleteItemFromObject(Labels, "vcs-url");
		cJSON_DeleteItemFromObject(Labels, "source");
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.vcs-url");

		cJSON_DeleteItemFromObject(Labels, "org.label-schema.version");
		cJSON_DeleteItemFromObject(Labels, "version");
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.vcs-ref");
		cJSON_DeleteItemFromObject(Labels, "git-commit");

		cJSON_DeleteItemFromObject(Labels, "org.label-schema.usage");
		cJSON_DeleteItemFromObject(Labels, "usage");
		cJSON_DeleteItemFromObject(Labels, "documentation");
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.url");
		cJSON_DeleteItemFromObject(Labels, "url");

		cJSON_DeleteItemFromObject(Labels, "description");
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.description");
		cJSON_DeleteItemFromObject(Labels, "org.label-schema.schema-version");
		
		// RFC-3339 date compliance
		const char * build_time=cJSON_get_key(Labels, "org.opencontainers.image.created");
		if (build_time && strlen(build_time)==8 && strchr(build_time,'-')==NULL && strchr(build_time,':')==NULL && strchr(build_time,'/')==NULL) // build-date="20160602"
		{
			char *created=NULL;
			asprintf(&created, "%.*s-%.*s-%.*sT00:00:00Z",4,build_time,2,build_time+4,2,build_time+6);
			cJSON_add_string(Labels, "org.opencontainers.image.created",created);
			if (created) free(created);
		} else if (build_time && strlen(build_time)==10 && build_time[4]=='-' && build_time[7]=='-') //build-date="2016-06-02"
		 {
			 char *created=NULL;
			 asprintf(&created, "%.*s-%.*s-%.*sT00:00:00Z",4,build_time,2,build_time+5,2,build_time+8);
			 cJSON_add_string(Labels, "org.opencontainers.image.created",created);
			 if (created) free(created);
		 }
		 build_time=cJSON_get_key(Labels, "org.opencontainers.image.created");
		char * short_date=NULL;
		if (deducted_version && build_time)
		{
			//create short date to add to ref
			asprintf(&short_date, "%.*s%.*s%.*s",4,build_time,2,build_time+5,2,build_time+8);
		}
		if (version && ns && !cJSON_get_key(Labels, "org.opencontainers.image.ref.name"))
		{
			char * ref_name=name_space_rev_slug(ns);
			if (ref_name)
			{
				if (short_date) // if short date was set, add to ref.name
					cJSON_add_string_v(Labels, "org.opencontainers.image.ref.name", "%s-%s-%s",ref_name,version,short_date);
				else
					cJSON_add_string_v(Labels, "org.opencontainers.image.ref.name", "%s-%s",ref_name,version);
				free(ref_name);
				ref_name=NULL;
			}
		}
		if (short_date) free(short_date);
		short_date=NULL;
	}
	if (version) free(version);
	version=NULL;
	if (ns) free(ns);
	ns=NULL;
	if (ir) free(ir);
	ir=NULL;
	if (rn) free(rn);
	rn=NULL;
	debug("autolabels_update after update\n");
	print_json(Labels);
}
void autolabel_limited(cJSON**docker_config ,dkr_parameter_t * parameters_from)
{
	debug("preparing auto labels limited\n");
	cJSON * oc_docker_config=cJSON_CreateObject();
	cJSON * oc_docker_config_labels=cJSON_CreateObject();
	cJSON_AddItemToObject(oc_docker_config,"Labels", oc_docker_config_labels);
	
	if (parameters_from && parameters_from->image)
	{
		cJSON_add_string(oc_docker_config_labels,"io.oc-runner.build.from.docker-repo",parameters_from->image);
		cJSON_add_string(oc_docker_config_labels,"io.oc-runner.build.from.nick-name",parameters_from->image_nick);
		cJSON_add_string(oc_docker_config_labels,"io.oc-runner.build.from.tag",parameters_from->reference);
		cJSON_add_string(oc_docker_config_labels,"io.oc-runner.build.from.digest",parameters_from->image_sha);
	}
	cJSON_add_string(oc_docker_config_labels,"io.oc-runner.builder-version", PACKAGE_VERSION);
	
	/*
	 cJSON * env_vars=cJSON_GetObjectItem(job, "env_vars");
	 cJSON_add_string(oc_docker_config_labels,"org.label-schema.schema-version","1.0");
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.label-schema.name",env_vars, "OC_INFO_IMAGE_NAME");
	 
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.label-schema.license",env_vars, "OC_INFO_LICENSE");
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.label-schema.vendor",env_vars, "OC_INFO_VENDOR");
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.label-schema.description",env_vars, "OC_INFO_IMAGE_SUMMARY");
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.label-schema.usage",env_vars, "OC_INFO_IMAGE_USAGE");
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.label-schema.url",env_vars, "OC_INFO_URL");
	 
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.opencontainers.image.vendor",     env_vars, "OC_INFO_VENDOR");
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.opencontainers.image.title",       env_vars, "OC_INFO_IMAGE_NAME");
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.opencontainers.image.description", env_vars,"OC_INFO_IMAGE_SUMMARY");
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.opencontainers.image.documentation",      env_vars, "OC_INFO_IMAGE_USAGE");
	 char * authors=read_a_file_v("%s/AUTHORS",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
	 if (authors)
	 {
		// flatten AUTHORS
		char *walk=authors;
		char *end=authors+ strlen(authors);
		while (walk!=end)
		{
	 if (walk[0]=='\n') walk[0]=';';
	 if (walk[0]=='\r') walk[0]=' ';
	 if (walk[0]=='\t') walk[0]=' ';
	 walk++;
		}
	 }
	 cJSON_add_string(oc_docker_config_labels,"org.opencontainers.image.authors",authors);
	 if (authors) free(authors);
	 cJSON_add_string_from_object(oc_docker_config_labels,"org.opencontainers.image.licenses",    env_vars, "OC_INFO_LICENSE");
	 const char * title= cJSON_get_key(env_vars, "OC_INFO_IMAGE_NAME");
	 char * nameslug=NULL;
	 if (title)
		nameslug=slug_it(title,0);
	 const char* version=cJSON_get_key(oc_docker_config_labels,"io.oc-runner.build.version");
	 if (version && nameslug)
		cJSON_add_string_v(oc_docker_config_labels,"org.opencontainers.image.ref.name", "%s-%s",title,version);
	 
	 cJSON_add_string_from_object(oc_docker_config_labels,"io.k8s.description",env_vars, "OC_INFO_IMAGE_SUMMARY");
	 */
	print_json(oc_docker_config);
	debug ("merge user with auto:\n");
	docker_merge_config(docker_config, oc_docker_config);
	print_json(*docker_config);
	cJSON_Delete(oc_docker_config);
	debug("success autolabel!\n");
}

