/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/**
 * \file autolabel.h
 * \brief headers for all functions of autolabeling of images
 * \author Created by Danny Goossen on  29/7/18.
 * \copyright 2018, Danny Goossen. MIT License
 */

#ifndef __oc_runner__autolabel__
#define __oc_runner__autolabel__

#include "common.h"
#include "image_parameters.h"

/**
 * \brief autolabel according to OCI
 */
void autolabel(cJSON**docker_config ,const cJSON * job,dkr_parameter_t * parameters_from);

/**
 */
void autolabel_limited(cJSON**docker_config ,dkr_parameter_t * parameters_from);

/**
 */
void autolabel_update(cJSON*manifest_config ,const cJSON * manifest,const char * image);

/**
 */
void parse_faulty_centos_labels(cJSON * labels);

/**
 \return 1 if we need a v1 to get tag reference for version calculation
 */
int autolabel_need_v1(cJSON*manifest_config ,const cJSON * manifest,const char * image);

#include <stdio.h>

#endif /* defined(__oc_runner__autolabel__) */
