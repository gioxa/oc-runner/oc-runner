/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.

 This file is part of the oc-runner

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


/*! \file rootfs_tar.h
 *  \brief header file for rootfs_tar.c
 *  \author Created by Danny Goossen on 03/04/2018.
 *  \copyright 2018, Danny Goossen. MIT License
 *
 */


#ifndef rootfs_tar_h
#define rootfs_tar_h

#define _GNU_SOURCE
#include <stdio.h>
#include "openssl.h"

#define NO_LOOKUP
#define NO_7ZIP
#define NO_DEPRECATED
#include <archive.h>



/**
 * \brief Defines output_modes for create_archive()
 *
 */
typedef enum {
	/** None donot output, only calculate sha256 */
	output_mode_none=0,

	/** output to file */
	output_mode_file,

	/** stream PATCH with curl */
	output_mode_curl,

} output_mode_t;

/**
 * \brief Defines output_modes for create_archive()
 *
 */
typedef enum {
	/** compress rootfs while stream */
	stream_source_rootfs,

	/** stream from source archive  */
	stream_source_archive

} stream_source_t;

/**
 * \brief Defines output_modes for create_archive()
 *
 */
typedef enum {
	/** stream from input read and calculate sha256 */
	output_stream_source_na=0,

	/** stream from input read and calculate sha256 */
	output_stream_source_read,

	/** stream from output write */
	output_stream_source_write

} output_stream_source_t;

/**
 * \brief Defines chmod for archive modification for create_archive()
 *
 */
typedef enum {
	/** None donot output, only calculate sha256 */
	chmod_none,

	/** copy permissions of user to group, usefull to create an image layer that needs to be r/w, when chmod_u2g */
	chmod_u2g,

} chmod_t;

/**
 \brief data structure containing all arguments for creating or reading an archive
 */
typedef struct layer_data{
    size_t content_len;     /**< stream length of the content (the .tar)*/
    size_t archive_len; /**< stream length of the compressed content ( the .tar.gz) */
    char sha256_archive[SHA256_DIGEST_LENGTH_BLOBSUM]; /**< Digest of the content retuned in format "sha256:<64*base16>\0", zero terminated*/
    char sha256_content[SHA256_DIGEST_LENGTH_BLOBSUM]; /**< Digest of the compressed blob retuned in format "sha256:<64*base16>\0", zero terminated */
    char * error_msg; /**< contains on error an error message, freed by calling free_layer_data() */
    size_t buff_size; /**< the size of stream chunks to be used by libarchive, set by init_layer_data() */
    char * copy_buffer; /**< buffer for stream chunks to be used by libarchive, allocated by init_layer_data() with size \bbuff_size and freed by calling free_layer_data() */
} layer_data_t;

/**
 \brief init layer data ( layer_data_t )
 \return pointer to \blayer_data_t
 \note Client is responsible for freeing the pointer, use free_layer_data()
*/
layer_data_t * init_layer_data(size_t copy_buff_size);

/**
 \brief free the layer data returned from init_layer_data()
 \param layer_data Pointer to layer_data, *layer_data will be set to NULL
 */
void free_layer_data(layer_data_t ** layer_data);


/**
 * \brief traverse an archive or rootfs, stream archive to nothing/file/url and retun size of archive, archive sha256 digest and content sha256 digest
 * \param pathnames pathnames to include
 * \param pathname_cnt number of items in pathnames
 * \param workdir directory to change to before traversing the rootfs with input_path filter
 * \param ss streamsource (archive or rootfs)
 * \param layer_data exchange data
 * \param output_mode (none/file/curl)
 * \param output_auth authorisation
 * \param output path of the archive to stream to, a printf like variable arguments
 * \return absolute filename , NULL on failure
 */
char * stream_archive(char * const * pathnames,size_t pathname_cnt, const char * workdir, stream_source_t ss, layer_data_t * layer_data, output_mode_t output_mode,chmod_t chmod,const char* output_auth,const char * output,...);



#endif /* rootfs_tar_h */
