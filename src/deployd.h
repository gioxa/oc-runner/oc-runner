/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! @file deployd.h
 *  @brief main header file
 *  @author danny@gioxa.com
 *  @date 18/2/17
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

#ifndef deployctl_deployd_h
#define deployctl_deployd_h

#ifndef VERBOSE_YN
/**
 \brief default verbose setiing
 */
#define VERBOSE_YN 1
#endif

#ifndef APP_NAME
#define   APP_NAME "oc-runner"
#endif

#ifndef CONFIG_FILE
/**
 \brief location of the config file
 \todo no longer common, should have one for executer, point to the config map
 */
#define CONFIG_FILE "/etc/oc-runner/runners.yaml"
#endif

#ifndef PREFIX
/**
 \brief output workdir prefix, not sure if still used
 */
#define PREFIX "/opt"
#endif

/**
 \brief default stale timeout
 */
#define TIME_OUT_CHILD 600

#include "common.h"
#include "cJSON_deploy.h"
#include "dyn_buffer.h"
#include "error.h"
#include "gitlab_api.h"
#include "oc_do_job.h"
#include "do_job.h"
#include "deploy-runner.h"
#include "dyn_buffer.h"
#include "yaml2cjson.h"
#include "utils.h"
#include "downloads.h"
#include "exec.h"
#include "libgit2.h"
#include "downloads.h"
#include "openssl.h"
#include "dpl_zip.h"
#include "dyn_trace.h"
#include "specials.h"
#include "openssl.h"
#include "dkr_api.h"
#include "oc_api.h"

#endif
