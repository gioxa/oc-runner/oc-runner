//
//  downloads.h
//  deploy-runner
//
//  Created by Danny Goossen on 23/7/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deploy__zip__
#define __deploy__zip__

#include "common.h"

int unzip(const char *archive, const char *dest);
int list_zip_it(char * upload_zip_name,char * CI_PROJECT_DIR,cJSON * filelist);

#endif /* defined(__deploy__zip__) */
