//
//  manifest.h
//  cjose_cjson
//
//  Created by Danny Goossen on 23/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#ifndef __cjose_cjson__manifest__
#define __cjose_cjson__manifest__

#include <stdio.h>
#include "cJSON_deploy.h"

char * SignManifest(const cJSON *Payload);
char * strip_manifest_v1(const char * manifest);

#endif /* defined(__cjose_cjson__manifest__) */
