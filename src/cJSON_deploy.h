/*
 cJSON_deploy.h
 Created by Danny Goossen, Gioxa Ltd on 11/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
/**
    \file cJSON_deploy.h
    \brief Header cJSON extensions
    \author Danny Goossen
    \date 11/2/17
    \copyright 2017 Danny Goossen. All rights reserved.
 */

#ifndef __deployctl__cJSON_deploy__
#define __deployctl__cJSON_deploy__


#include "common.h"
#include <cJSON.h>
#include "cJSON_deploy.h"

/**
 \brief is debug is set, safe printf cJSON object
 */
#define print_json(x) if (x) {char * printit=cJSON_Print(x);debug("%s\n",printit);free (printit);} else { debug("cJSON is NULL\n");}

/**
 \brief add an time stamp to an object with key as key
 */
void cJSON_AddTimeStampToObject(cJSON * object,const char *key);

/**
 \brief create cJSON environment object form system environment
 */
extern cJSON * cJSON_Create_env_obj(char** envp);

/**
 \brief append a docker config Env to cJSON environment object
 \param target pointer to environment object
 \param dkr_env cjson array of docker config Env
 */
void cJSON_append_env_obj_from_dkr_Env(cJSON**target,const cJSON * dkr_env);

/**
 \brief parse a cJSON environment object to a **env for execenvp()
 */
extern int parse_env(cJSON * env_json, char *** newevpp);

/**
  \brief free **env
 */
extern void free_envp(char *** envp);

/**
 \brief obsolete function
 */
int get_key(const cJSON * item, const char * key,char ** value );

/**
 \brief CJSON create string from non null terminated string
 */
extern cJSON *cJSON_CreateString_n(const char *string, size_t n);

/**
 \brief Add item to object with non null terminated key string
 */
extern void   cJSON_AddItemToObject_n(cJSON *object, const char *string,size_t n , cJSON *item);

/**
 \brief add an item to the beginning of an array
*/
void   cJSON_AddItemToBeginArray(cJSON *array, cJSON *item);

/**
 \brief safe get string as const char* from key/value object list
 */
const char * cJSON_get_key(const cJSON * item, const char * key);

/**
 \brief safe get a string dupplicate as from key/value object list
 \note client is responsible for freeing the returned value
 */
char * cJSON_get_stringvalue_dup(const cJSON * item, const char * key);

/**
 \brief safe get string as char* from key/value object list
 \note user cannot free char*, it still belongs to the cJSON object
 */
char * cJSON_get_stringvalue_as_var(const cJSON * item, const char * key);

/**
 \brief checks if for a given key in an object equals a value and returns 0 on success
 \param item the object to retrieve the key and verify value
 \param key to check
 \param value value to compaire
 \return 0 on success, -1 on failure
 */
int validate_key(const cJSON * item, const char * key,const char * value );

int check_presence(cJSON * env_json, char ** list, char ** error_msg);

char* cJSON_strdup_n(const unsigned char * str, size_t n);

cJSON *cJSON_CreateString_n(const char *string, size_t n);

int in_string_array(const cJSON * json_list, char * search);

/**
 \brief check if a key in an object is set to True
 \returns 1 on set, 0 if not present, 0 on set False
 */
int cJSON_safe_IsTrue(const cJSON *object,char * key);

/**
 \brief check if a key in an object is set to False
 \returns 1 on False, 0 if not present, 0 on set True
 */
int cJSON_safe_IsFalse(const cJSON *object,char * key);

int32_t cJSON_safe_GetNumber(const cJSON *object,char * key);

#define cJSON_AddStringToObject_n(object,name,s,n) cJSON_AddItemToObject(object, name, cJSON_CreateString_n(s,n))

#define cJSON_safe_addString2Obj(object,key,z) if (!cJSON_get_key(object,key)) {cJSON_AddStringToObject(object,key,z);} else {cJSON_ReplaceItemInObject(object,key,cJSON_CreateString(z));}

#define cJSON_safe_addNumber2Obj(x,y,z) if (!cJSON_GetObjectItem(x,y)) {cJSON_AddNumberToObject(x,y,z); } else {cJSON_ReplaceItemInObject(x,y,cJSON_CreateNumber(z));}

#define cJSON_safe_addBool2Obj(x,y,z) if (!cJSON_GetObjectItem(x,y)) { if (z!=0) { cJSON_AddTrueToObject(x,y); } else { cJSON_AddFalseToObject(x,y);} } else { cJSON_ReplaceItemInObject(x,y,cJSON_CreateBool(z!=0));}

/**
 \brief add key/value object to target if value !=NULL
 
 */
void cJSON_add_string(cJSON * target,const char * key,const char * value);

/**
 \brief add key/value with non null terminated value object to target if value !=NULL
 
 */
void cJSON_add_string_n(cJSON * target,const char * key,const char * value,size_t len);

/**
 \brief add key/value object to target as prinf like args if not NULL
 
 */
void cJSON_add_string_v(cJSON * target,const char * key,const char * content,...);

/**
 \brief add newkey/value object to target from value if not null obtained from source object with key entry
 
 
 */
void cJSON_add_string_from_object(cJSON * target,const char * newkey,const cJSON * source,const char * key);

/**
 \brief check if a key in an object is set to True
 \returns 1 on set, 0 if not present, 0 on set False
 */
int cJSON_findinstringarry_n(cJSON * sa,const char * content,size_t n);

/**
 \brief yaml string for true, case insensitive
 
 True can be [True,1,ON,YES] case insensitive
 */
int cJSON_IsTrue_string(const char *valuestring);

/**
 \brief get stringvalue of key and check if it contains match, case insensitive
 \returns 1 on a match was found, case insensitive
 */
int cJSON_validate_contains_match(const cJSON * json,const char *key,const char * match);

/**
 \brief read a given printf like filename and return an cJSON object on success.
 \param errormsg pointer to loacation to put error msg in when not NULL
 \param vpath printf like filename to read
 \return a cJSON object read from file
 \note caller is responsible for freeing cJSON object.
 */
cJSON * read_file_json_v( char ** errormsg,const char * vpath,...);


void cJSON_add_Array_string(cJSON * target,const char * value);

void cJSON_add_Array_string_v(cJSON * target,const char * string,...);

void cJSON_add_Array_string_n(cJSON * target,const char * string,size_t n);



#endif /* defined(__deployctl__cJSON_deploy__) */
