
/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file dkr_api.c
 *  \brief Basic docker registry interactions, without daemon nor need for root access
 *  \author Created by Danny Goossen on 10/10/17.
 *  \copyright Copyright (c) 2017 Danny Goossen. All rights reserved.
 *  \code
 */

#include "deployd.h"
#include "dkr_api.h"
#include "curl_headers.h"


#define RETRY_HTTP(X) ({ int res=0; int count=0;do{ if (count==0) usleep(2000000LL*count*count); res=X; count++; }while(((res<100 && res>0) || res>=500) && count<6);res;})


// Internals type defs

/**
 * \brief extract the bearer token from the headers
 * \param headers cJSON header struct
 * \returns cJSON struct with realm/scope/service ...etc
 */
static cJSON * bearer_extract_header(cJSON * headers);

/**
 * \brief get Bearer token
 */
static long dkr_api_get_Bearer_token( CURL * curl, cJSON ** result,const char * realm, const cJSON* realm_data, const char * basic_auth);



/**
 \brief do the actual requests to the docker registry
 
 Internal funtion that can handle all docker requests and deals with the authorisation.
 
 \note Up to the caller to make sure registry is added for the image with the proper authorisation and having the correct registry_id
 \note Caller is reponsible for releasing memory \b result
 \code 
  \\ requestv example:
     dkr_api_do(....,"http://%s/%s",domain,argx);
 \endcode
 \param dkr_api_data docker api data struct
 \param registry_number the registry id according to registry add sequence
 \param methode http methode (GET/HEAD/POST...)
 \param filename to save http payload in
 \param filename_out to sent binary data with POST or PUT, if any otherwise NULL will do
 \param accept http Accept or "" or NULL
 \result buffer pointer to store http payload in if no \b filename defined, caller is responsible for freeing data
 \param data to sent if no filename or NULL
 \param data_len integer pointer with the length of bytes to sent if any, if NULL and \b data strlen(data) is used(non binary data is ok)
 \param Content_type of data to sent if any or NULL or "" will do
 \param Check_v2_only if 1 no authorisation will be done, just checking the http-header for Docker v2 registry presence
 \param requestv varg constant like printf of the url request use like printf
 \return http code or CURLE-code on failure
 */
static int dkr_api_do( struct dkr_api_data_s * dkr_api_data ,int registry_number,const char *methode, const char * filename,const char * filename_out,const char *accept,char ** result,const char* data, size_t * data_len, char ** Content_type, int Check_v2_only,const char * requestv, ...);



//***************************************
// dkr_api global functions / dkr_api.h
//***************************************

int verify_image(struct dkr_api_data_s * apidata, char * image,struct trace_Struct * trace, char **image_sha, char ** tag)
{
	// TODO change to HEAD and move to dkr api
	int image_check_result=0;
	// Check image
	if (strlen(image)>0)
	{
		image_check_result=(int)dkr_api_manifest_exists(apidata,image,image_sha,tag);
	}
	if (image && image_check_result!=200)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		if (image_check_result<0)
		{
			Write_dyn_trace(trace, magenta, "\n\tError image check: %s\n",curl_easy_strerror(image_check_result));
		}
		else
		{
			if (image_check_result<90)
			{
				Write_dyn_trace(trace, magenta, "\n\tERROR:(%d)  %s\n",image_check_result,curl_easy_strerror(image_check_result));
			}
			else
				switch (image_check_result) {
					case 404:
						Write_dyn_trace(trace, magenta, "\n\tImage Not Found (HTTP %d)\n",image_check_result);
						break;
					case 500:
						Write_dyn_trace(trace, magenta, "\n\tRegistry server error (HTTP %d)\n",image_check_result);
						break;
					case 403:
						Write_dyn_trace(trace, magenta, "\n\tNot Authorised (HTTP %d)\n",image_check_result);
						break;
						
					default:
						Write_dyn_trace(trace, magenta, "\n\tHttp-Error : HTTP %d\n",image_check_result);
						break;
				}
		}
		Write_dyn_trace(trace, red, "\nImage pre check failed, exit!\n");
	}
	else
		Write_dyn_trace(trace, green, "  [OK]\n");
	update_details(trace);
	return image_check_result;
}


int dkr_api_init(struct dkr_api_data_s ** api_data, const char *ca_cert_bundle)
{
    //if (*api_data) dkr_api_cleanup(api_data);
	
    (*api_data)=calloc(1,sizeof(struct dkr_api_data_s));
    (*api_data)->curl = curl_easy_init();
    if (ca_cert_bundle)(*api_data)->default_ca_bundle=strdup(ca_cert_bundle);
    
    if ((*api_data)->curl)
    {
        if ((*api_data)->default_ca_bundle)
		curl_easy_setopt((*api_data)->curl, CURLOPT_CAINFO, (*api_data)->default_ca_bundle);
        curl_easy_setopt((*api_data)->curl, CURLOPT_SSL_VERIFYPEER, 1L);
        curl_easy_setopt((*api_data)->curl, CURLOPT_FOLLOWLOCATION, 0L);
        curl_easy_setopt((*api_data)->curl, CURLOPT_VERBOSE, 0L);
        curl_easy_setopt((*api_data)->curl, CURLOPT_NOPROGRESS, 1L);
        curl_easy_setopt((*api_data)->curl, CURLOPT_TIMEOUT, 300L); // 10min
        curl_easy_setopt((*api_data)->curl, CURLOPT_CONNECTTIMEOUT,60L); // 30 sec on connection
		curl_easy_setopt((*api_data)->curl, CURLOPT_USERAGENT, PACKAGE_STRING);
        return 0;
    }
    else
    {
        dkr_api_cleanup(api_data);
        return 1;
    }
}

void dkr_api_cleanup(struct dkr_api_data_s ** api_data)
{
    if (*api_data)
    {
       curl_easy_setopt((*api_data)->curl, CURLOPT_CAINFO, NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_READDATA,NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEHEADER, NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEDATA, NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEFUNCTION, NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_HEADERFUNCTION,NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_READDATA, NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_UPLOAD, 0L);
       curl_easy_setopt((*api_data)->curl, CURLOPT_INFILESIZE_LARGE,0L);
       curl_easy_setopt((*api_data)->curl, CURLOPT_POSTFIELDS,NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_READFUNCTION, NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_CUSTOMREQUEST,NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_URL,NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_HTTPHEADER, NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_PROGRESSDATA, NULL);
       curl_easy_setopt((*api_data)->curl, CURLOPT_PROGRESSFUNCTION, NULL);
       
        if ((*api_data)->registries) cJSON_Delete((*api_data)->registries);
        if ((*api_data)->default_ca_bundle) free((*api_data)->default_ca_bundle);
       //debug("Cleanup (*api_data)->curl\n");
       if ((*api_data)->curl) curl_easy_cleanup((*api_data)->curl);
        (*api_data)->curl=NULL;
        (*api_data)->registries=NULL;
        (*api_data)->default_ca_bundle=NULL;
        //debug("Cleanup *api_data\n");
        free(*api_data);
       debug("Done Cleanup *api_data\n");
        *api_data=NULL;
    }
    return ;
}

void debug_registry_print(cJSON * registry )
{
	if (registry)
	{
		const char *reg=cJSON_get_key(registry, "registry_name");
		const char *ns=cJSON_get_key(registry, "registry_namespace");
		const char *ba=cJSON_get_key(registry,"Bearer_auth");
		const char *basic=cJSON_get_key(registry,"Basic_auth");
		int number=cJSON_safe_GetNumber(registry, "id");
		if (number==INT32_MIN) number=-1;
		size_t ba_len=0;
		if (ba)
		{
			ba_len=strlen(ba);
			debug("** REGISTRY **\n*************\n reg # %d: %s\n ns: %s\n bearer: %.*s (%d)\n\n",number,reg,ns,28,ba,ba_len);
		}
		if (basic)
		{
			ba_len=strlen(basic);
			debug("** REGISTRY **\n*************\nreg # %d: %s\n ns: %s\n Basic: %.*s (%d)\n\n",number,reg,ns,18,basic,ba_len);
		}
		if (!ba && !basic)
		{
			debug("** REGISTRY **\n*************\nreg # %d: %s\n ns: %s =>NO AUTH\n\n",number,reg,ns);
		}
		
	}
}

int dkr_api_check_registry(struct dkr_api_data_s * dkr_api_data, const char * registry_name)
{
    // returns 0 on faulure, 1 on success
    alert("check registry name %s\n",registry_name);
    int registry_number=0;
    const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,registry_name,&registry_number,NULL,NULL,NULL);
    if(!registry)
    {
        error("Not found in registry : %s\n",registry_name);
        return -1;
    }
    
    char * http_s=NULL;
    cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
    if (insecure && cJSON_IsTrue(insecure))
        http_s=strdup("http://");
    else
        http_s=strdup("https://");
   
   int result=dkr_api_do(dkr_api_data,registry_number ,"HEAD", NULL,NULL,NULL,NULL,NULL, 0, NULL,1, "%s%s/v2/",http_s,registry_name);
   free(http_s);
   debug("Result registry ip=%d",result);
      if (result==200 || result==401 || result==400 || result==404) result=1;
    return (result);
}



int dkr_api_add_registry(struct dkr_api_data_s * api_data, const char * registry_name, const char * namespace ,const char * user,const char * pwd, const char *auth, int insecure)
{
    cJSON * registry=NULL;
    char* basic_auth=NULL;
    alert("adding registry : %s\n",registry_name);
    if (!registry_name) return -1;
    else
    {
        registry=cJSON_CreateObject();
        cJSON_AddStringToObject(registry, "registry_name",registry_name);
    }
   if (namespace)
   {
      debug(" with registry namespace %s\n",namespace);
      cJSON_AddStringToObject(registry, "registry_namespace",namespace);
   }
    if (user && pwd )
    {
        //if (user && strlen(user)>0)
        basic_auth=Base64Encode_v("Basic ","%s:%s",user,pwd);
        
        if (basic_auth)
        {
            debug("%s authorisation: %.*s ... (%d)\n",registry_name,16,basic_auth,strlen(basic_auth));
            //debug("%s \nAuthorisation: %s\n\n",name,basic_auth,strlen(basic_auth));
            cJSON_AddStringToObject(registry, "Basic_auth",basic_auth);
            
        }
    }else if(!auth)
    {
        alert("add registry: authorisation: none\n");
    }
	   
    if (auth)
    {
		char * tmp=calloc(1, strlen(auth)+strlen("Basic ")+1);
		sprintf(tmp, "Basic %s",auth);
		cJSON_AddStringToObject(registry, "Basic_auth",tmp);
    }
    if (insecure)
    {
        cJSON_AddTrueToObject(registry, "insecure");
        debug("added registry as insecure\n");
    }
    
    if (basic_auth) free(basic_auth);
   
    if (!api_data->registries) api_data->registries=cJSON_CreateArray();
    cJSON_AddItemToArray(api_data->registries, registry);
	cJSON_AddNumberToObject(registry, "id", cJSON_GetArraySize(api_data->registries)-1);
    
    // now verify registry /v2
   
    debug("added registry %s/%s\n",registry_name,namespace);
    return 0;
}

int dkr_api_blob_get(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, char ** result)
{
	int registry_number=0;
	char * registry_name=NULL;
	char * namespace=NULL;
	char * http_s=NULL;
	int res=0;
	const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,NULL);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
	char * digest_escaped=NULL;
	char *p=NULL;
	if ((p=strchr(blobSum, ':')))
	{
		char * t=calloc(1, strlen(blobSum)+1+2);
		int len=(int)(p-blobSum);
		sprintf(t, "%.*s%%3A%s",len,blobSum,p+1);
		digest_escaped=t;
	}
	else
		digest_escaped=strdup(blobSum);
	
	// check if layer exist? return 20o if exists, 404 if not,
	char *content_type=strdup("");
	char * accept=strdup("application/octet-stream");
	if (result && *result) {free(*result);result=NULL;}
	res=dkr_api_do(dkr_api_data,registry_number , "GET",NULL,NULL,accept,result, NULL, 0,&content_type,0, "%s%s/v2/%s/blobs/%s",http_s,registry_name,namespace,digest_escaped);
	if(content_type) free(content_type);
	if (accept) free(accept);
	if (digest_escaped) free(digest_escaped);
   if (http_s) free(http_s);
   if(registry_name) free(registry_name);
   if(namespace) free(namespace);
	return ((int)res);
}



int dkr_api_blob_get_file(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, const char * filename,...)
{
	
	char * filepath=CSPRINTF(filename);
	
	if(!filepath)
	{
		error("processing filepath, no memory???\n");
		return -1;
	}
	
	int registry_number=0;
	char * registry_name=NULL;
	char * namespace=NULL;
	char * http_s=NULL;
	int res=0;
	const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,NULL);
	
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
	char * digest_escaped=NULL;
	char *p=NULL;
	if ((p=strchr(blobSum, ':')))
	{
		char * t=calloc(1, strlen(blobSum)+1+2);
		int len=(int)(p-blobSum);
		sprintf(t, "%.*s%%3A%s",len,blobSum,p+1);
		digest_escaped=t;
	}
	else
		digest_escaped=strdup(blobSum);
	
	// check if layer exist? return 20o if exists, 404 if not,
	
	res=dkr_api_do(dkr_api_data,registry_number , "GET",filepath,NULL,NULL,NULL, NULL, 0, NULL,0, "%s%s/v2/%s/blobs/%s",http_s,registry_name,namespace,digest_escaped);
	if (digest_escaped) free(digest_escaped);
   if(http_s) free(http_s);
   if(registry_name) free(registry_name);
   if(namespace) free(namespace);
	if(filepath) free(filepath);
	return ((int)res);
}


int dkr_api_blob_check(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum)
{
   int registry_number=0;
   char * registry_name=NULL;
   char * namespace=NULL;
   char * http_s=NULL;
   int res=0;
   const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,NULL);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
   char * digest_escaped=NULL;
   char *p=NULL;
   if ((p=strchr(blobSum, ':')))
   {
        char * t=calloc(1, strlen(blobSum)+1+2);
        int len=(int)(p-blobSum);
        sprintf(t, "%.*s%%3A%s",len,blobSum,p+1);
        digest_escaped=t;
   }
   else
      digest_escaped=strdup(blobSum);
    
    // check if layer exist? return 20o if exists, 404 if not,
    res=dkr_api_do(dkr_api_data,registry_number , "HEAD",NULL,NULL,NULL,NULL, NULL, 0, NULL,0, "%s%s/v2/%s/blobs/%s",http_s,registry_name,namespace,digest_escaped);
    if (digest_escaped) free(digest_escaped);
   if (http_s) free(http_s);
   if(registry_name) free(registry_name);
   if(namespace) free(namespace);
    return ((int)res);
}

int dkr_api_blob_pull_get_url(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum,char ** url,char ** Auth)
{
   int registry_number=0;
   char * registry_name=NULL;
   char * namespace=NULL;
   char * http_s=NULL;
   const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,NULL);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://"); // TODO change to http and do a ping, does ping exists in V2???
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
   char * digest_escaped=NULL;
   char *p=NULL;
   if ((p=strchr(blobSum, ':')))
   {
      char * t=calloc(1, strlen(blobSum)+1+2);
      int len=(int)(p-blobSum);
      sprintf(t, "%.*s%%3A%s",len,blobSum,p+1);
      digest_escaped=t;
   }
   else
      digest_escaped=strdup(blobSum);
   
   // TODO if registry does not exist, try this first
   
   char *tmp=NULL;
   asprintf(&tmp,"%s%s/v2/%s/blobs/%s",http_s,registry_name,namespace,digest_escaped);
   if (tmp && url) *url=strdup(tmp);
    if(tmp)free(tmp);
   const char *auth_temp=cJSON_get_key(registry, "Bearer_auth");
   if (Auth && auth_temp) *Auth=strdup(auth_temp);
   
   if (digest_escaped) free(digest_escaped);
   if (http_s) free(http_s);
   if(registry_name) free(registry_name);
   if(namespace) free(namespace);
   return (1);
}

int dkr_api_blob_push_get_url(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum,char ** url,char ** Auth,int * reg)
{
   char *registry_name=NULL;
   char *namespace=NULL;
   char *reference=NULL;
   int registry_number=0;
   char * http_s=NULL;
   const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,&reference);
   
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
	
   
   const char accept[]="";
   char * location=NULL;
   char * result_string=NULL;
   int res=0;
   res=dkr_api_do(dkr_api_data,registry_number , "POST",NULL,NULL,accept,&location, NULL, 0, NULL,0, "%s%s/v2/%s/blobs/uploads/",http_s,registry_name,namespace);
   // todo process result string
   // todo
   
	if ((res==202  )&& location)
	{
		// if location returns '.../<Uuid>?...=...' we need adding '&digest=<digest>'
		// othewise if '...../<Uuid> we need to add ?digest=<digest>
		if (blobSum)
		{
			char * digest_escaped=NULL;
			char *p=NULL;
			if ((p=strchr(blobSum, ':')))
			{
				char * t=calloc(1, strlen(blobSum)+1+2);
				int len=(int)(p-blobSum);
				sprintf(t, "%.*s%%3A%s",len,blobSum,p+1);
				digest_escaped=t;
			}
			else
				digest_escaped=strdup(blobSum);
			char seperator=0;
			if (strchr(location,'?')!=NULL)
				seperator='&';
			else
				seperator='?';
			debug("\n%s: result from POST uploads: %s\n",image,location);
			*url=calloc(1, strlen(location)+1+7+strlen(digest_escaped)+1);
			sprintf(*url,"%s%cdigest=%s",location,seperator,digest_escaped);
			if (digest_escaped) free(digest_escaped);
			free(location);
		}
		else
		{
			*url=location;
		}
		if (reg) *reg=registry_number;
		if (Auth && registry) *Auth=cJSON_get_stringvalue_dup(registry, "Bearer_auth");
	}
	if (http_s) free(http_s);
	if (result_string) free (result_string);
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
	return (res);
}


int dkr_api_blob_push(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, const char *filepath,...)
{
	char * filename=CSPRINTF(filepath);
	
	if(!filename)
	{
		error("processing filepath, no memory???\n");
		return -1;
	}
   int res=0;
   char * result_string=NULL;
   char * url=NULL;
   int registry_number=-1;
   res= RETRY_HTTP( dkr_api_blob_push_get_url(dkr_api_data,image,blobSum,&url,NULL,&registry_number));
	
   if (url)
   {
      char * content_type=strdup("application/octet-stream");
      const char accept[]="";
      res=dkr_api_do(dkr_api_data,registry_number , "PUT",NULL,filename,accept,&result_string, NULL, 0,&content_type,0,"%s", url);
	   
	   debug("push blob: %s\n",result_string);
	   if (result_string) free(result_string);
      if (content_type) free(content_type);
      free(url);
      url=NULL;
   }
	free(filename);
   return ((int)res);
}

int dkr_api_blob_push_raw(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, const char *blob,size_t size)
{
   char *registry_name=NULL;
   char *namespace=NULL;
   int registry_number=0;
   char * http_s=NULL;
   
	const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,NULL);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
   char * digest_escaped=NULL;
   char *p=NULL;
   if ((p=strchr(blobSum, ':')))
   {
      char * t=calloc(1, strlen(blobSum)+1+2);
      int len=(int)(p-blobSum);
      sprintf(t, "%.*s%%3A%s",len,blobSum,p+1);
      digest_escaped=t;
   }
   else
      digest_escaped=strdup(blobSum);
   
   const char accept[]="";
   char * location=NULL;
   char * result_string=NULL;
   int res=0;
   res=dkr_api_do(dkr_api_data,registry_number , "POST",NULL,NULL,accept,&location, NULL, 0, NULL,0, "%s%s/v2/%s/blobs/uploads/",http_s,registry_name,namespace);
   // todo process result string
   // todo
   
   if (res==202 && location)
   {
      // if location returns '.../<Uuid>?...=...' we need adding '&digest=<digest>'
      // othewise if '...../<Uuid> we need to add ?digest=<digest>
      char seperator=0;
      if (strchr(location,'?')!=NULL)
         seperator='&';
      else seperator='?';
      debug("%s: result from POST uploads: %s\n",image,location);
      char * content_type=strdup("application/octet-stream");
      res=dkr_api_do(dkr_api_data,registry_number , "PUT",NULL,NULL,accept,&result_string, blob, &size,&content_type,0, "%s%cdigest=%s",location,seperator,digest_escaped);
      if (content_type) free(content_type);
      free(location);
   }
   if (result_string) free (result_string);
   if (digest_escaped) free(digest_escaped);
   if(registry_name) free(registry_name);
   if(namespace) free(namespace);
   if (http_s) free(http_s);
   return ((int)res);
}

int dkr_api_manifest_push_raw(struct dkr_api_data_s * dkr_api_data, const char *image,const char *tag,char *manifest, int V2)
{
   char * http_s=NULL;
   char *registry_name=NULL;
   char *namespace=NULL;
   char *reference=NULL;
   long res=0;
   int registry_number=0;
   const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,&reference);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
   char * this_tag=NULL;
   if (tag) this_tag=strdup(tag);
   else if (!tag && reference) this_tag=strdup(reference);
   else if (!tag && !reference) this_tag=strdup("latest");
   
   const char accept[]="";
	char * content_type=NULL;
	if (V2)
      content_type=strdup("application/vnd.docker.distribution.manifest.v2+json");
	else
      content_type=strdup("application/vnd.docker.distribution.manifest.v1+json");
	
	// dkr_api_do( dkr_api_data ,registry_number,methode, f,f_out,accept,** result,** data, data_len, ** Content_type, int Check_v2_only,const char * requestv, ...)
 
	char *result=NULL;
   res=dkr_api_do(dkr_api_data,registry_number , "PUT",NULL,NULL,accept,&result, manifest,NULL,&content_type,0, "%s%s/v2/%s/manifests/%s",http_s,registry_name,namespace,this_tag);
	if (result) free(result);
	if (http_s)
   {
      free(http_s);
      http_s=NULL;
   }
	if (this_tag)
   {
      free(this_tag);
      this_tag=NULL;
   }
   
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
   if (content_type) free(content_type);
   return ((int)res);
}

int dkr_api_manifest_delete(struct dkr_api_data_s * dkr_api_data, char **image,const char *blobSum)
{
	char * http_s=NULL;
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	long res=0;
	int registry_number=0;
	const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,*image,&registry_number,&registry_name,&namespace,&reference);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
	char * digest_escaped=NULL;
	char *p=NULL;
	if ((p=strchr(blobSum, ':')))
	{
		char * t=calloc(1, strlen(blobSum)+1+2);
		int len=(int)(p-blobSum);
		sprintf(t, "%.*s%%3A%s",len,blobSum,p+1);
		digest_escaped=t;
	}
	else
		digest_escaped=strdup(blobSum);

 

	res=dkr_api_do(dkr_api_data,registry_number , "DELETE",NULL,NULL,NULL,NULL, NULL,NULL,NULL,0, "%s%s/v2/%s/manifests/%s",http_s,registry_name,namespace,digest_escaped);
	
	
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
	if(digest_escaped) free(digest_escaped);
   if (http_s) free(http_s);
	return ((int)res);
}



int dkr_api_manifest_push(struct dkr_api_data_s * dkr_api_data,const char *image,const char *tag, const char *filepath,...)
{
	char * filename=CSPRINTF(filepath);
	
	if(!filename)
	{
		error("processing filepath, no memory???\n");
		return -1;
	}
   char * http_s=NULL;
   char *registry_name=NULL;
   char *namespace=NULL;
   char *reference=NULL;
   long res=0;
   int registry_number=0;
   const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,&reference);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
   const char accept[]="";
   char * content_type=strdup("application/vnd.docker.distribution.manifest.v2+json");
   res=dkr_api_do(dkr_api_data,registry_number , "PUT",NULL,filename,accept,NULL, NULL, 0,&content_type,0, "%s%s/v2/%s/manifests/%s",http_s,registry_name,namespace,tag);
   if (content_type) free(content_type);
   if (filename) free(filename);
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
   if (http_s) free(http_s);
   return ((int)res);
}

/*
int dkr_api_pull_layer(struct dkr_api_data_s * dkr_api_data,int ref,const char *registry_api,const char *name,const char *blobSum, const char *filename)
{
    return(dkr_api_do(dkr_api_data,ref , "GET",filename,NULL,"",NULL, NULL, NULL, NULL,0, "%s/v2/%s/blobs/%s",registry_api,name,blobSum));
    
}
*/

int dkr_api_manifest_exists( struct dkr_api_data_s * dkr_api_data,const char * image, char ** image_sha, char ** tag)
{
   char *registry_name=NULL;
   char *namespace=NULL;
   char *reference=NULL;
   long res= 0;
   int registry_number=0;
   char * http_s=NULL;
   const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,&reference);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
   if (!reference) reference=strdup("latest");
   debug("using registry #: %d, %s\n",registry_number,cJSON_get_key(registry, "name"));
	char * result=NULL;
   res=dkr_api_do(dkr_api_data,registry_number , "HEAD",NULL,NULL,"application/vnd.docker.distribution.manifest.v2+json",&result, NULL, 0, NULL,0, "%s%s/v2/%s/manifests/%s",http_s,registry_name,namespace,reference);
	debug("result dkr_api_manifest_exists %d\n",res);
	if (res==200 && image_sha)
	{
		if (result)
		{
			cJSON * docker_headers=cJSON_Parse(result);
			if (docker_headers)
			{
				if (*image_sha) free(*image_sha);
				*image_sha=cJSON_get_stringvalue_dup(docker_headers,"Docker-Content-Digest");
				cJSON_Delete(docker_headers);
			}
			free(result);
		}
	}
	if (tag && reference)
	{
		*tag=reference;
	}
	else if(reference) free(reference);
   if(registry_name) free(registry_name);
   if(namespace) free(namespace);
   if (http_s) free(http_s);
   // should return resultdata and docker registy
   return (int)res;
}


int dkr_api_get_manifest( struct dkr_api_data_s * dkr_api_data ,const char * accept,const char * image, cJSON ** result)
{
    char *registry_name=NULL;
    char *namespace=NULL;
    char *reference=NULL;
    long res= 0;
    int registry_number=0;
    char * http_s=NULL;
    const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,&reference);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
	if (result && *result) {cJSON_Delete(*result); *result=NULL;}
   char * result_string=NULL;
   if (!reference) reference=strdup("latest");
   debug("using registry #: %d, %s\n",registry_number,cJSON_get_key(registry, "registry_name"));
   
   res=(dkr_api_do(dkr_api_data,registry_number , "GET",NULL,NULL,accept,&result_string, NULL, 0, NULL,0, "%s%s/v2/%s/manifests/%s",http_s,registry_name,namespace,reference));
   if (res==200 && result_string)
        (*result)=cJSON_Parse(result_string);
   else res=-1;
   if (result_string) free(result_string);
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
   if (http_s) free(http_s);
    // should return resultdata and docker registy
   return (int)res;
}

int dkr_api_get_manifest_raw( struct dkr_api_data_s * dkr_api_data ,const char * accept,const char * image, char** result)
{
   char *registry_name=NULL;
   char *namespace=NULL;
   char *reference=NULL;
   long res= 0;
   int registry_number=0;
   char * http_s=NULL;
   const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,&reference);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
   if (!reference) reference=strdup("latest");
   debug("using registry #: %d, %s\n",registry_number,cJSON_get_key(registry, "name"));
	if (result && *result) {free(*result);*result=NULL;}
   res=dkr_api_do(dkr_api_data,registry_number , "GET",NULL,NULL,accept,result, NULL, 0, NULL,0, "%s%s/v2/%s/manifests/%s",http_s,registry_name,namespace,reference);
	if(registry_name) free(registry_name);
	if(namespace) free(namespace);
	if(reference) free(reference);
   if (http_s) free(http_s);
   return (int)res;
}

int dkr_api_get_manifest_2_file( struct dkr_api_data_s * dkr_api_data ,const char * accept,const char * image, const char * filepath,...)
{
	char * filename=CSPRINTF(filepath);
	
	if(!filename)
	{
		error("processing filepath, no memory???\n");
		return -1;
	}

	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	long res= 0;
	int registry_number=0;
	char * http_s=NULL;
	const cJSON * registry= dkr_lookup_registry(dkr_api_data->registries,image,&registry_number,&registry_name,&namespace,&reference);
   if(!registry)
   {
      debug("Not found in registry : %s\n",registry_name);
      http_s=strdup("https://");
   }
   else
   {
      cJSON * insecure=cJSON_GetObjectItem(registry, "insecure");
      if (insecure && cJSON_IsTrue(insecure))
         http_s=strdup("http://");
      else
         http_s=strdup("https://");
   }
	if (!reference) reference=strdup("latest");
	debug("using registry #: %d, %s\n",registry_number,cJSON_get_key(registry, "name"));
	res=dkr_api_do(dkr_api_data,registry_number , "GET",filename,NULL,accept,NULL, NULL,0, NULL,0, "%s%s/v2/%s/manifests/%s",http_s,registry_name,namespace,reference);
	
	if (res==200)
	{
		char * file_content=read_a_file(filename);
		if (file_content)
		{
			debug("filecontent: %s\n%s\n",filename);
			cJSON * manifest=cJSON_Parse(file_content);
			if (manifest)
			{
				debug("manifest json:");
				print_json(manifest);
				cJSON_Delete(manifest);
			}
			else error("could not parese filecontent\n");
			free(file_content);
		}
		else error("No filecontent\n");
	}
	if (filename) free(filename);
   if (http_s) free(http_s);
   if(registry_name) free(registry_name);
   if(namespace) free(namespace);
   if(reference) free(reference);
	filename=NULL;
	return (int)res;
}

const cJSON * dkr_lookup_registry(const cJSON * registries,const char * image,int * number, char ** registry_name,char** namespace, char ** tag_reference)
{
   char * registry_name_t=NULL;
   char * namespace_t=NULL;
   char * reference_t=NULL;
   
   char image_check[1024];
   //debug("\nimage request for pod: >%s<\n",image);
	
	/*
	// TODO remove this, image should always be complete
	char *p_dot=strchr(image,'.');
	char *p_ddot=strchr(image,':');
	
   if ((p_dot && !p_ddot) || (p_dot && p_ddot && (p_dot < p_ddot) ))
   { // image contains a registry
      sprintf(image_check, "%s",image);
   }
   else
   { // image does not contain registry so add library/ for checking docker
      sprintf(image_check, "library/%s",image);
   }
	*/
	sprintf(image_check, "%s",image);
   //debug("image_check: %s\n",image_check);
   chop_image(image_check, &registry_name_t, &namespace_t, &reference_t);
   //debug("check: r:%s n:%s r:%s\n",registry_name_t, namespace_t, reference_t);
   // feedback if needed
  
   cJSON * reg_matches=cJSON_CreateArray();
   cJSON * registry=NULL;
   cJSON * result_registry=NULL;
   (*number)=0;
   // first match regestry name and create subset
   cJSON_ArrayForEach(registry, registries)
   {
      const char * temp=cJSON_get_key(registry, "registry_name");
      
      if(temp && strcmp(temp,registry_name_t)==0)
      {
         cJSON * tmp=cJSON_Duplicate( registry,1);
         cJSON_AddNumberToObject(tmp, "number", *number);
         cJSON_AddItemToArray(reg_matches, tmp);
      }
      (*number)++;
   }
   if (cJSON_GetArraySize(reg_matches)>0)
   {
      int highest_match_level=-1;
      int highest_match_item=-1;
      cJSON_ArrayForEach(registry, reg_matches)
      {
         const char * temp=cJSON_get_key(registry, "registry_namespace");
         if ( temp==NULL && highest_match_level<0)
         { // match, lowest level
            highest_match_item=cJSON_GetObjectItem(registry, "number")->valueint;
            highest_match_level=0;
         }
         else if (temp && strstr(namespace_t,temp)==namespace_t && (int)strlen(temp)>highest_match_level)
         {
            highest_match_item=cJSON_GetObjectItem(registry, "number")->valueint;
            highest_match_level=(int)strlen(temp);
         }
      }
      if (highest_match_item>-1)
      {
         result_registry=cJSON_GetArrayItem(registries, highest_match_item);
         *number=highest_match_item;
      }
      else
      {
         result_registry=NULL; // just to make sure
         *number=-1;
      }
   }
   else
   {
      result_registry=NULL; // just to make sure
      *number=-1;
   }
   cJSON_Delete(reg_matches);
   if (registry_name) *registry_name=registry_name_t; else {free(registry_name_t);}
   if (namespace) *namespace=namespace_t;else free(namespace_t);
   if (tag_reference) *tag_reference=reference_t; else free(reference_t);
	if (result_registry)
	{
		debug_registry_print(result_registry);
	}
   return result_registry;
}

// ***********************
// * Internal Functions  *
// ***********************

struct put_data
{
	const char *data;
	size_t len;
	size_t size;
};

static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream)
{
	struct put_data * userdata=(struct put_data *)stream;
	size_t curl_size = nmemb * size;
	size_t to_copy = (userdata->len < curl_size) ? userdata->len : curl_size;
	memcpy(ptr, userdata->data, to_copy);
	userdata->len -= to_copy;
	userdata->data += to_copy;
	return to_copy;
}

int dkr_api_do( struct dkr_api_data_s * dkr_api_data ,int registry_number,const char *methode, const char * filename,const char * filename_out,const char *accept,char ** result, const char* data, size_t * data_len, char ** Content_type, int Check_v2_only,const char * requestv, ...)
{
	char * request=CSPRINTF(requestv);
	if (!request) return -1;

	
    debug("dkr_api: %s, reg#%d %s\n",methode,registry_number,request);
    int count_401=0;
    CURLcode res=0;
    long http_code=0;
    cJSON * registry=NULL;
    char* auth_Bearer=NULL;
	const char * Location=NULL;
	 struct put_data putData;
	
    if (registry_number>-1)
    {
        registry=cJSON_GetArrayItem(dkr_api_data->registries, registry_number);
		debug_registry_print(registry);
       if (cJSON_get_key(registry, "Bearer_auth")) auth_Bearer=cJSON_get_stringvalue_dup(registry, "Bearer_auth");
    }
    int Follow_auth=(Check_v2_only==0);
    struct curl_slist *headers = NULL;
    do
    {
        debug("dkr_api (%d): %s %s\n",count_401,methode,request);
        if(dkr_api_data->curl && request)
        {
			debug("dup handler\n");
			CURL *curl=curl_easy_duphandle(dkr_api_data->curl);
            // prepare mem
            dynbuffer * chunk =dynbuffer_init();
            struct curl_headers_struct headers_res;
            curl_headers_init(&headers_res);
            
            // set request
            curl_easy_setopt(curl, CURLOPT_URL, (char*)request);
			curl_easy_setopt(curl, CURLOPT_VERBOSE,0L);
			//curl_easy_setopt(curl,CURLOPT_FORBID_REUSE,1L);
			//curl_easy_setopt(curl,CURLOPT_FRESH_CONNECT,1L);
            FILE *fd=NULL;
            
            if (data )
            {
				//debug("set data\n%s\n",data);
				if (!data_len || *data_len==0)
					putData.size=strlen(data);
				else
					putData.size=*data_len;
				
				putData.data=data;
				putData.len=putData.size;
				
				curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,(curl_off_t)putData.size);
				curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
				curl_easy_setopt(curl, CURLOPT_READDATA,&putData);
				/* enable uploading */
                curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
				headers = curl_slist_append(headers, "Expect:");
					
            }
            else if (filename_out && strlen(filename_out)>0)
            {
                fd = fopen(filename_out, "rb"); /* open file to upload */
                if(!fd)
                    return 1; /* can't continue */
                struct stat file_info;
                if(fstat(fileno(fd), &file_info) != 0)
                    return 1; /* can't continue */
                /* tell it to "upload" to the URL */
                curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
                //curl_easy_setopt(curl, CURLOPT_HEADER, 1L);
                /* set where to read from (on Windows you need to use READFUNCTION too) */
                curl_easy_setopt(curl, CURLOPT_READDATA, fd);
                
                /* and give the size of the upload (optional) */
                curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,(curl_off_t)file_info.st_size);
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
                //headers = curl_slist_append(headers, "Expect:");
				curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
            }
            else
            {
                curl_easy_setopt(curl, CURLOPT_READDATA, NULL);
                curl_easy_setopt(curl, CURLOPT_UPLOAD, 0L);
                curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,0L);
                curl_easy_setopt(curl, CURLOPT_POSTFIELDS,"");
				curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
            }
            if (Content_type && *Content_type)
            {
                size_t len=strlen("Content-Type: ")+ strlen(*Content_type)+1;
                char * temp=calloc(1,len );
                sprintf(temp,"Content-Type: %s",*Content_type);
                headers = curl_slist_append(headers, temp);
                memset(temp, 0, len);
                free(temp);
            }
           
            if (auth_Bearer)
            {
                char * temp=calloc(1, strlen("Authorization: ")+ strlen(auth_Bearer)+1);
                sprintf(temp,"Authorization: %s",auth_Bearer);
                headers = curl_slist_append(headers, temp);
                free(temp);
            }
            if (accept && strlen(accept)>0)
            {
                char *accept_header=calloc(1,1+strlen("Accept: ")+strlen(accept));
                if(accept_header)
                {
                    sprintf(accept_header, "Accept: %s",accept);
                    headers = curl_slist_append(headers, accept_header);
                    free(accept_header);
                }
            }
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
            FILE* file=NULL;
            // set methode ( GET, POST ... )
            if (methode)
            {
				curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, methode);
                if (strcmp(methode, "HEAD")==0)
				{
                    curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
					curl_easy_setopt(curl, CURLOPT_TIMEOUT, 120L);
					curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 60L);
					//curl_easy_setopt(curl,CURLOPT_FRESH_CONNECT,1L);
				}
				else
                    curl_easy_setopt(curl, CURLOPT_NOBODY, 0L);
					
            }
			else
				curl_easy_setopt(curl, CURLOPT_NOBODY, 0L);
            if (!filename)
            {
                /* send all data to this function  */
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
                /* we pass our 'chunk' struct to the callback function */
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)chunk);
            }
            else
            {
				debug ("sending data to this file %s\n",filename);
                file = fopen( filename, "wb");
				if (!file) error("opening file: %s\n",strerror(errno));
				else debug("success opening file\n");
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
                curl_easy_setopt( curl, CURLOPT_WRITEDATA, file) ;
            }
            curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION,curl_headers_cb);
            curl_easy_setopt(curl, CURLOPT_WRITEHEADER, &headers_res);
			
			// execute request
            res = curl_easy_perform(curl);
			//process http_code
			if (res==CURLE_OK)
			{
				curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
				debug("dkr api: curle_ok, http: %d\n",http_code);
				
			}
			else
			{
				http_code=res;
				debug("dkr api: res!=curle_ok %d, %s\n",res,curl_easy_strerror(res));
			}
			//
			if (file)
			 {
				//fflush(file);
				fclose(file);
				FILE* file = fopen( filename, "rb");
				if (file)
				{
				   fseek(file, 0, SEEK_END); // seek to end of file
				   size_t size = ftell(file); // get current file pointer
				   debug("\n Received Filecheck %d bytes\n",size);
				   fclose(file);
				}
				else error("failed check file\n");
			 }
             if (headers)curl_slist_free_all(headers);
             headers=NULL;
           
             // if any data returned, put parse into result
             if (dynbuffer_len(chunk)>1 && result)
             {
               if (*result) free(*result);
			   *result=NULL;
               dynbuffer_pop(&chunk, result, NULL);
             }
             else if (result && *result)
            {
              free(*result);
              *result=NULL;
            }
			dynbuffer_clear(&chunk);
			// get Location from headers if any
            Location=cJSON_get_key(headers_res.header, "Location");
            debug("dkr api: result %d\n",http_code);
            if (res==CURLE_OK && Check_v2_only)
            {
                if(validate_key(headers_res.header, "Docker-Distribution-Api-Version", "registry/2.0")==0)
                    http_code=200;
                else
                    http_code=404;
            }
            else if (res==CURLE_OK && (http_code>=300 && http_code<400) && Location)
            {
               // redirect, remove auth and use Location for request
               if (request) free(request);
				request=NULL;
               if (auth_Bearer) free(auth_Bearer);
				auth_Bearer=NULL;
               if (result && *result)
               {
                  free(*result);
                  *result=NULL;
               }
               request=strdup(Location);
               if (getdebug())
               {
                  debug("3XX: Headers\n");
                  print_json(headers_res.header);
               }
               http_code=401; // marking as 401 will loop new requestwith Location url
               count_401=0; // reset the 401 counter
				
            }
            else if (res==CURLE_OK && (http_code==200)&& result)
            {
                if (getdebug())
                {
                char * printit=cJSON_Print(headers_res.header);
                if (printit)
                {
                    debug("200: Headers\n%s\n",printit);
                    free(printit);
                }
                }
                if (strcmp(methode,"HEAD")==0 && result)
                {
                   cJSON * tmp=cJSON_CreateObject();
                   const char * Location=cJSON_get_key(headers_res.header, "Location");
                   if (Location)
                      cJSON_AddStringToObject(tmp, "url",Location);
                   else
                   {
                      cJSON_AddStringToObject(tmp, "url",request);
                      if (auth_Bearer) cJSON_AddStringToObject(tmp, "Auth", auth_Bearer);
                   }
                   if (accept)
                      cJSON_AddStringToObject(tmp, "accept", accept);
					const char * DockerContentDigest=cJSON_get_key(headers_res.header,"Docker-Content-Digest");
					if (DockerContentDigest)
						cJSON_AddStringToObject(tmp, "Docker-Content-Digest",DockerContentDigest);
                   if(*result) free(*result);
                   *result=cJSON_Print(tmp);
                   cJSON_Delete(tmp);
                }
            }
            else if (res==CURLE_OK && (http_code==202 )&& result && Location)
            {
                    if (*result) free(*result);
                    //*result=strdup(upload_Uuid);
                    *result=strdup(Location);
                   if (getdebug())
                   {
					   debug("202:(location) Headers\n");
					   print_json(headers_res.header);

				   }
            }
			else if (res==CURLE_OK && (http_code==400 )&& Location)
			{
				if (strcmp(methode,"HEAD")==0 && result)
				{
				   cJSON * tmp=cJSON_CreateObject();
				   const char * Location=cJSON_get_key(headers_res.header, "Location");
					
				   cJSON_AddStringToObject(tmp, "Location",Location);
				   cJSON_AddStringToObject(tmp, "url",request);
					
				   if (auth_Bearer) cJSON_AddStringToObject(tmp, "Auth", auth_Bearer);
				   if (accept) cJSON_AddStringToObject(tmp, "accept", accept);
				   if(*result) free(*result);
				   *result=cJSON_Print(tmp);
				   cJSON_Delete(tmp);
				}
				else
				{
						if (request) free(request);
						if (auth_Bearer) free(auth_Bearer);
						if (result && *result)
						{
							free(*result);
							*result=NULL;
						}
						auth_Bearer=NULL;
						request=strdup(Location);
						if (getdebug())
						{
							debug("400: Headers\n");
							print_json(headers_res.header);
						}
						http_code=401; // marking as 401 will loop new requestwith Location url
						count_401=0;
				}
			}
            else if (http_code == 401 && Follow_auth && headers_res.header && count_401<1)
            {
                cJSON * Www_Authenticate=bearer_extract_header(headers_res.header);
                if (Www_Authenticate)
                {
                    
                    cJSON * Bearer_result=NULL;
                    char * realm=cJSON_get_stringvalue_dup(Www_Authenticate, "realm");
                    
                    const char * error_auth=cJSON_get_key(Www_Authenticate,"error");
                    if (error_auth)
                    {
                        debug("dkr Www-authenticate Error: %s\n",error_auth);
                    }
                    cJSON_DeleteItemFromObject(Www_Authenticate, "error");
                    if (realm) cJSON_DeleteItemFromObject(Www_Authenticate, "realm");
                    //cJSON_AddStringToObject(Www_Authenticate, "client_id", "docker");
                    long res_bearer=dkr_api_get_Bearer_token(dkr_api_data->curl,&Bearer_result,realm ,Www_Authenticate,cJSON_get_key(registry, "Basic_auth"));
                    if (realm) free(realm);
                    realm=NULL;
                    const char * token=cJSON_get_key(Bearer_result, "token");
                    if(token)
                    {
                       debug("got token (%d) >%.*s ...<\n",strlen(token),16,token);
                       if (auth_Bearer) free(auth_Bearer);
                        size_t len=strlen("Bearer ")+strlen(token)+1;
                        auth_Bearer=calloc(1,len );
                        sprintf(auth_Bearer, "Bearer %s",token);
                       if (registry)
                       {
						   //TODO: need to get mutex for parallel processing and or downloading to modify bearer Auth string
                        if (cJSON_GetObjectItem(registry, "Bearer_auth"))cJSON_DeleteItemFromObject(registry, "Bearer_auth");
                        cJSON_AddStringToObject(registry, "Bearer_auth",auth_Bearer);
                       }
                        debug("Bearer Auth parsed\n");
                    }
                    else if (res_bearer==401)
                    {
                        debug("dkr_api: request key returned 401, transform to 403\n");
                        http_code=403 ; // if not needed
                    }
                    else
                    {
                        debug("dkr_api: request key returned %d, transform to 403\n",res_bearer);
                        http_code=403; // unathorized
                    }
                    if (Www_Authenticate) cJSON_Delete(Www_Authenticate);
                    if (Bearer_result) cJSON_Delete(Bearer_result);
                   Www_Authenticate=NULL;
                   Bearer_result=NULL;
                }
                else
                {
                    debug("dkr_api: no www_Authenticate received\n");
                    http_code=403;
                }
            }
            else
            {
                if (res!=CURLE_OK)
                {
                    error("dkr_api: curl error:%s %s =>%s\n",methode,request, curl_easy_strerror(res));
                }
                else if (http_code!=200)
                {
                    error("dkr_api: HTTP_code %ld for %s %s\n%s\n",http_code,methode,request,curl_easy_strerror(res));
					if (result)
                    debug("dkr_api: return data for #%d: %s\n",http_code,*result);
					print_json(headers_res.header);
                }
            }
			
            if (res==CURLE_OK && filename )
            {
				print_json(headers_res.header)
            }
            curl_headers_free(&headers_res);
			curl_easy_cleanup(curl);
        }
        else
            http_code=CURLE_FAILED_INIT;
        if (http_code==401)count_401++;
    } while (http_code==401 && count_401<2);
	if(auth_Bearer) free(auth_Bearer);
   // prevent segment faults in easy cleanup???
	
	if (request) free(request);
    return (int)http_code;
}

cJSON * bearer_extract_header(cJSON * headers)
{
    if (!headers) return NULL;
    cJSON * result=NULL;
    const char*to_process=cJSON_get_key(headers, "Www-Authenticate");
    if (!to_process) return(NULL);
    char *p=strstr(to_process,"Bearer realm=");
    if (!p) return NULL;
    char * data=strdup(to_process+7);
	
    // data='realm="https://gitlab.gioxa.com:443/jwt/auth",service="container_registry",scope="repository:deployctl/oc-image:pull"'
    
    const char *keyfind[]={"realm=\"","service=\"","scope=\"","error=\""};
    const char *key[]={"realm","service","scope","error"};
    int i;
    
    for (i=0;i<4;i++)
    {
        size_t len=0;
        char *pp=strstr(data,keyfind[i]);
        if (pp && strlen(pp)>strlen(keyfind[i]))
        {
            pp=pp+strlen(keyfind[i]);
            char* end_ptr =strchr(pp, '"');
            if (end_ptr)
            {
                len=end_ptr-pp;
            }
            if (len)
            {
                if (!result) result=cJSON_CreateObject();
                cJSON_AddStringToObject_n(result, key[i], pp, len);
            }
        }
    }
    // check if all items present!
    if(check_presence(result,(char *[]){"realm",NULL}, NULL)!=0)
    {
        cJSON_Delete(result);
        result=NULL;
    }
	if (data) free(data);
    return result;
}



static long dkr_api_get_Bearer_token( CURL * curls, cJSON ** result,const char * realm, const cJSON* realm_data, const char * basic_auth)
{
    CURLcode res;
    long http_code = 0;
    struct curl_slist *headers = NULL;
	CURL *curl=curl_easy_duphandle(curls);
    //char *data=NULL;
    debug("get token for realm: %s",realm);
    if(curl && realm)
    {
        dynbuffer * chunk=dynbuffer_init();
        struct curl_headers_struct headers_res;
        curl_headers_init(&headers_res);
        
        
        char request[0x10000];
        sprintf(request, "%s?",realm);
        cJSON * item=NULL;
        int cnt=0;
        cJSON_ArrayForEach(item, realm_data)
        {
            if (cnt>0 && !cJSON_IsFalse(item))
            {
                sprintf(request+strlen(request), "&");
            }
            if(cJSON_IsTrue(item))
            {
                sprintf(request+strlen(request), "%s=true",item->string);
            }
            else if (cJSON_IsString(item))
            {
                sprintf(request+strlen(request), "%s=%s",item->string,item->valuestring);
            }
            cnt++;
        }
        curl_easy_setopt(curl, CURLOPT_URL, (char*)request);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
        curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
        curl_easy_setopt(curl, CURLOPT_NOBODY, 0L);
		//curl_easy_setopt(curl,CURLOPT_FORBID_REUSE,1L);
        //curl_easy_setopt(curl,CURLOPT_FRESH_CONNECT,1L);
        debug("\nGET %s\n",request);
        if (basic_auth)
        {
            size_t len=strlen("Authorization :")+ strlen(basic_auth)+1;
            char * temp=calloc(1, len);
            sprintf(temp,"%s: %s","Authorization",basic_auth);
            headers = curl_slist_append(headers, temp);
            //alert("\nUse basic auth for Bearer token request: %s\n",temp);
            memset(temp, 0, len);
            free(temp);
        }
        headers = curl_slist_append(headers, "Accept: application/json");
        if (headers) curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        // set methode ( GET, POST ... )
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
        
        /* send all data to this function  */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)chunk);
        
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION,curl_headers_cb);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, &headers_res);
               // execute request
        res = curl_easy_perform(curl);
        //
        if (headers)curl_slist_free_all(headers);
        headers=NULL;
       
        // if any data returned, put parse into result
        if (dynbuffer_len(chunk)>1 && result) *result = cJSON_Parse(dynbuffer_get(chunk));
        
        //process http_code
        if (res==CURLE_OK)
            curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
        else
            http_code=res;
        
        //alert("Bearer _request: return data for #%d: %s\n",http_code,dynbuffer_get(chunk));
        if (http_code!=200)
        {
            if (res!=CURLE_OK)
                error("Bearer_request: CURL error for %s %s %s\n","GET",realm, curl_easy_strerror(res));
            else
            {
                error("Bearer_request: HTTP_code %ld for %s %s\n",http_code,"GET",realm);
            }
            
        }
        else debug("Bearer token request OK\n");
        dynbuffer_clear(&chunk);
        curl_headers_free(&headers_res);
        debug("Bearer token request OK: freed all\n");
    }
    else
        http_code=CURLE_FAILED_INIT;
	
	curl_easy_cleanup(curl);
    return http_code;
}

int dkr_api_check_registry_secure(const char * ImageStream_domain, const char * cert_file_dest, void * trace)
{
    int error_count=0;
    int result=-1;
    
    int insecure=0;
    int curl_res=0;
    do
    {   // TODO fix this, need 3 outcomes: secure/non-secure/no connect
        
        CURL * t=curl_easy_init();
        
        if (cert_file_dest)
        {
            curl_easy_setopt(t, CURLOPT_CAINFO, cert_file_dest);
            curl_easy_setopt(t, CURLOPT_SSL_VERIFYPEER, 1L);
        }
        curl_easy_setopt(t, CURLOPT_VERBOSE, 0L);
        curl_easy_setopt(t, CURLOPT_NOPROGRESS, 1L);
        curl_easy_setopt(t, CURLOPT_TIMEOUT, 300L); // 10min
        curl_easy_setopt(t, CURLOPT_CONNECTTIMEOUT,60L); // 30 sec on connection
		curl_easy_setopt(t,CURLOPT_FORBID_REUSE,0L);
		curl_easy_setopt(t,CURLOPT_FRESH_CONNECT,0L);
        struct curl_headers_struct headers_res;
        curl_headers_init(&headers_res);
        curl_easy_setopt(t, CURLOPT_HEADERFUNCTION,curl_headers_cb);
        curl_easy_setopt(t, CURLOPT_WRITEHEADER, &headers_res);
        
        curl_easy_setopt(t, CURLOPT_CUSTOMREQUEST, "HEAD");
        curl_easy_setopt(t, CURLOPT_NOBODY, 1L);
		curl_easy_setopt(t, CURLOPT_USERAGENT, PACKAGE_STRING);
        char url[1024];
        if (insecure)
            snprintf(url,1024,"http://%s/v2/",ImageStream_domain);
        else
            snprintf(url,1024,"https://%s/v2/",ImageStream_domain);
        curl_easy_setopt(t, CURLOPT_URL, (char*)url);
        
        curl_res=curl_easy_perform(t);
        
        if (curl_res==CURLE_OK )
        {
            if(validate_key(headers_res.header, "Docker-Distribution-Api-Version", "registry/2.0")==0)
            {
                if (insecure)
                    result=1;
                else
                    result=0;
            }
            else result=-2;
        }
        else if ( insecure==0 && curl_res==CURLE_SSL_CONNECT_ERROR) insecure=1;
        else if (curl_res==CURLE_COULDNT_CONNECT)
        {
            error_count++;
            usleep(2000000);
        } else error_count=16;
        curl_headers_free( &headers_res);
        curl_easy_cleanup(t);
    } while (error_count < 15 && result==-1);
    if (trace)
    {
        if (curl_res==CURLE_OK && result>=0)
        {
            if (result==0) Write_dyn_trace(trace, green,  "             [secure]\n");
            if (result==1) Write_dyn_trace(trace, yellow, "          [in-secure]\n");
        }
        else
        {
            Write_dyn_trace(trace, red,                   "               [FAIL]\n");
            if (result==-2)
                Write_dyn_trace(trace, magenta, "\n\tError: %s is not a docker api V2 registry\n",ImageStream_domain);
            else
                Write_dyn_trace(trace, magenta, "\n\tError for %s,\n\t       %s\n",curl_easy_strerror(curl_res),ImageStream_domain);
        }
    }
    return result;
}

