//
//  dyn_trace.h
//  oc-runner
//
//  Created by Danny Goossen on 16/10/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//
/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! @file dyn_trace.h
 *  @brief header file dynamic buffer operations for gitlab ci trace
 *  @author danny@gioxa.com
 *  @date  16/10/17.
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

#ifndef __oc_runner__dyn_trace__
#define __oc_runner__dyn_trace__

#include "common.h"

size_t clear_dynamic_trace( void *userp);
void free_dynamic_trace( void **userp);
int init_dynamic_trace(void**userp, const char * token, const char * url, int job_id, const char * ca_bundle);
void clear_till_mark_dynamic_trace( void *userp);
void set_mark_dynamic_trace( void *userp);
void set_time_mark_dynamic_trace( void *userp);
void update_time_dyn_trace(void * userp);
void clear_mark_dynamic_trace( void *userp);

/**
 * \brief append spaces to buff that decrease over time
 *  Solution for updating trace on progress, works with clear_till_mark_dynamic_trace() and set_mark_dynamic_trace().
 *  trace only rewrites the output in browser when trace is shorter.
 *  For this to work, we use a counter, and append (100-counter).
 * \param userp pointer to the trace
 */
void dyn_trace_add_decrement(void *userp);

/**
 * \brief append spaces to buff that decrease over time, no \\n
 *  Solution for updating trace on progress, works with clear_till_mark_dynamic_trace() and set_mark_dynamic_trace().
 *  trace only rewrites the output in browser when trace is shorter.
 *  For this to work, we use a counter, and append (100-counter).
 * \param userp pointer to the trace
 */
void dyn_trace_add_decrement_nl(void *userp);

/**
 * \brief write to trace buffer with color and Vargs
 * \param userp pointer to the trace
 * \param color enumeration of color
 * \param message the message to trace
 * \returns number of bytes writen to the trace
 */
size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...);
/**
 * \brief write to trace buffer with color,padding and Vargs
 * \param userp pointer to the trace
 * \param color enumeration of color
 * \param len2pad padding length
 * \param message the message to trace
 * \returns number of bytes writen to the trace
 */
size_t Write_dyn_trace_pad(void *userp,enum term_color color, int len2pad,const char *message, ...);

const char * get_dynamic_trace( void *userp);
void set_dyn_trace_state(void *userp,char * state);


/*
 size_t Write_dynamic_trace_color(const void *contents,char * state, enum term_color,void *userp);

 \brief write contents to trace buffer
 \todo remove, replace with
 
size_t Write_dynamic_trace(const void *contents,char * state, void *userp);

 */
size_t Write_dynamic_trace_n(const void *contents,size_t n, void *userp);


/**
 \brief internal function for printing error on http api feedback
 \param trace trace structure for job feedback
 \param error feedback code
 */
void print_api_error(struct trace_Struct* trace,int error);


#endif /* defined(__oc_runner__dyn_trace__) */
