//
//  dyn_trace.c
//  oc-runner
//
//  Created by Danny Goossen on 16/10/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "dyn_trace.h"
#include "deployd.h"

/**
 \brief array referenced with \b term_color
 */
const char * color_data[]={ANSI_BOLD_BLACK,ANSI_BOLD_RED,ANSI_BOLD_GREEN,ANSI_BOLD_YELLOW,ANSI_BOLD_BLUE,ANSI_BOLD_MAGENTA,ANSI_BOLD_CYAN,ANSI_BOLD_WHITE, \
ANSI_BLACK,ANSI_RED,ANSI_GREEN,ANSI_YELLOW,ANSI_BLUE,ANSI_MAGENTA,ANSI_CYAN,ANSI_WHITE, \
ANSI_CROSS_BLACK,ANSI_CROSS_RED,ANSI_CROSS_GREEN,ANSI_CROSS_YELLOW,ANSI_CROSS_BLUE,ANSI_CROSS_MAGENTA,ANSI_CROSS_CYAN,ANSI_CROSS_WHITE, \
ANSI_RESET};

/**
 \brief process cariage return in dyn trace buffer
 
 Gitlab trace buffer converts a '\r' to an '\n' does
 \param buf the buffer
 \param n the pointer to the strlen, is updated
 \param first_cr pointer to the first known occurange of a '\r' is updated
 \param last_nl pointer to last known pos of a newline, is updated
 */
static void do_carriage_return(char * buf,size_t * n,size_t * first_cr,size_t* last_nl)
{
   int notdone=1;
   do
   {
      // go back to last \n
      char * first_car=NULL;
      char * last_newline=NULL;
      if (*last_nl)
         last_newline=buf+(*last_nl);
      else
         last_newline=buf;
      if (*first_cr)
         first_car=buf+(*first_cr);
      else
         first_car=memchr(last_newline,'\r',(*n)-(*last_nl));
      
      if (first_car)
         *first_cr=(size_t)(first_car-buf);
      else
         *first_cr=0;
      
      if (first_car && ((*first_cr)+1)!=(*n) && buf[(*first_cr)+1]!='\n' )
      {
         last_newline=memrchr(buf,'\n',*first_cr);
         if (last_newline)
         {
            *last_nl=last_newline-buf;
            size_t t=(first_car+1-buf);
            memcpy(last_newline+1, first_car+1,(*n)-t);
            *n=(*n)-(size_t)((first_car)-(last_newline));
         }
         else
         {
            memcpy(buf, first_car+1, ((*n)-(first_car+1-buf)));
            (*n)=(*n)-(size_t)(first_car+1-buf);
         }
         buf[*n]=0;
         first_car=memchr(buf,'\r',(*n));
         if (first_car)
            (*first_cr)=first_car-buf;
         else
            (*first_cr)=0;
      }
      else if (first_car && ((*first_cr)+1)!=(*n) && buf[(*first_cr)+1]=='\n' )
      {
         last_newline=buf + (*first_cr);
         *last_nl=last_newline-buf;
         size_t t=(first_car+1-buf);
         memcpy(first_car, first_car+1,(*n)-t);
         (*n)--;
         buf[*n]=0;
         first_car=memchr(buf,'\r',(*n));
         if (first_car)
            (*first_cr)=first_car-buf;
         else
            (*first_cr)=0;
      }
      else notdone=0;
   } while ( notdone);
}

/*------------------------------------------------------------------------
* Helper function append string to dynamic trace
*------------------------------------------------------------------------*/
int init_dynamic_trace(void**userp, const char * token, const char * url, int job_id, const char * ca_bundle)
{
   struct trace_Struct **trace_p = (struct trace_Struct **)userp;
   if (!trace_p || ! token || !url) return -1;
   if (*trace_p) free_dynamic_trace(userp);
   *trace_p=NULL;
   
   (*trace_p)=calloc(1, sizeof(struct trace_Struct));
   if (*trace_p)
   {
      (*trace_p)->url=strdup(url);
      (*trace_p)->first_cr=0;
      (*trace_p)->last_nl=0;
      (*trace_p)->job_id=job_id;
      (*trace_p)->params = cJSON_CreateObject();
	   (*trace_p)->ca_bundle=ca_bundle;
      (*trace_p)->time_mark=0;
      cJSON_AddStringToObject((*trace_p)->params, "state", "running");
      cJSON_AddStringToObject((*trace_p)->params, "token", token);
      cJSON * tmp=cJSON_CreateObject();
      tmp->valuestring=calloc(1,0x10000);
      if (tmp)
      {
         tmp->type=cJSON_String;
         cJSON_AddItemToObject((*trace_p)->params, "trace", tmp);
         (*trace_p)->trace_len=0;
         (*trace_p)->trace_buf_size=0x10000; // 64K initial buffer size
         (*trace_p)->curl=curl_easy_init();;
		  curl_easy_setopt((*trace_p)->curl, CURLOPT_CAINFO, (*trace_p)->ca_bundle);
		  curl_easy_setopt((*trace_p)->curl, CURLOPT_SSL_VERIFYPEER, 1L);
		  curl_easy_setopt((*trace_p)->curl, CURLOPT_FOLLOWLOCATION, 1L);
		  curl_easy_setopt((*trace_p)->curl, CURLOPT_USERAGENT, PACKAGE_STRING);

         (*trace_p)->tests=NULL;
         (*trace_p)->mark=0;
         (*trace_p)->min_trace_buf_increment=0x4000; //increment with minimun 16K
      }
      else
      {
         cJSON_Delete( (*trace_p)->params);
         if ((*trace_p)->url) free((*trace_p)->url);
         (*trace_p)->url=NULL;
         free(*trace_p);
         (*trace_p)=NULL;
      }
   }
   if (*trace_p) debug("trace->params ptr %p\n",(*trace_p)->params);
   return 0;
}

size_t Write_dynamic_trace_n(const void *contents,size_t n, void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   if (contents && n>0)
   {
      cJSON * item=cJSON_GetObjectItem(trace->params, "trace" );
      if (item && item->valuestring)
      {
         if (trace->trace_len+1+n> trace->trace_buf_size)
         {
            size_t newsize=trace->trace_buf_size+ MAX(trace->min_trace_buf_increment, n);
            char * temp=realloc(item->valuestring,newsize);
            if (temp )
            {
               trace->trace_buf_size=newsize;
               if (temp!=item->valuestring)
               {
                  item->valuestring=temp;
               }
            }
            else
            {
               error("Write_dyn_trace_n, no more mem");
               return -1;
            }
         }
         memcpy(item->valuestring+trace->trace_len, contents, n);
         trace->trace_len+=n;
         item->valuestring[trace->trace_len]=0;
         do_carriage_return(cJSON_get_stringvalue_as_var(trace->params, "trace"), &trace->trace_len, &trace->first_cr, &trace->last_nl);
      }
      else error("Write_dyn_trace_n, cjson trace problem\n");
   }
   return n;
}

size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...)
{
   
   
   if (!userp || !message|| strlen(message)==0) return -1;
   va_list varg;
   va_start (varg, message);
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   
   size_t consten_buf_len=0;
   char * contents=NULL;
   // print into temp buffer and adjust buffer till content fits
   
   size_t cnt=0;
   int res=0;
   do {
      cnt++;
      if (contents) free(contents);
      contents=NULL;
      contents=(char *)calloc(cnt,trace->min_trace_buf_increment);
      res=-1;
      if (contents)
      {
         consten_buf_len=(size_t)(trace->min_trace_buf_increment * cnt);
         res=vsnprintf(contents,consten_buf_len ,message,varg);
      }
      else
         break;
   } while ((size_t)res >= consten_buf_len);
   
   va_end(varg);
   
   if (res<0)
   {
      error("Buffer error contents\n");
      if (contents) free (contents);
      contents=NULL;
   }
   size_t n=-1;
   // with content and length, check if trace buffer needs to be increased
   if (contents)
   {
      // content ready, let's calculate the length
      if (color== none)
         n= strlen(contents);
      else if (color== reset)
         n= strlen(contents) + strlen(ANSI_RESET);
      else
         n= strlen(contents) + strlen(color_data[color]) + strlen(ANSI_RESET);
      
      cJSON * item=cJSON_GetObjectItem(trace->params, "trace" );
      if (item && item->valuestring)
      {
         // increase buffer if needed
         if (trace->trace_len+1+n> trace->trace_buf_size)
         {
            size_t newsize=trace->trace_buf_size+ MAX(trace->min_trace_buf_increment, n+1);
            char * temp=realloc(item->valuestring,newsize);
            if (temp )
            {
               trace->trace_buf_size=newsize;
               if (temp!=item->valuestring)
               {
                  item->valuestring=temp;
               }
            }
            else
            {
               error("Write_dyn_trace_n, no more mem");
               if (contents) free(contents);
               return -1;
            }
         }
        
         char cr[2]={0,0};
         
         if (contents[strlen(contents)-1]=='\n')
         {
            cr[0]='\n';
            contents[strlen(contents)-1]=0;
         }
      
         // append contents
         if (color== none)
            sprintf(item->valuestring+trace->trace_len,"%s%s",contents,cr);
         else if (color==reset)
            sprintf(item->valuestring+trace->trace_len,"%s%s%s",ANSI_RESET, contents,cr);
         else
            sprintf(item->valuestring+trace->trace_len,"%s%s%s%s",color_data[color],contents,ANSI_RESET,cr);
         trace->trace_len+=n;
         item->valuestring[trace->trace_len]=0;
         //do_carriage_return(cJSON_get_key(trace->params, "trace"), &trace->trace_len, &trace->first_cr, &trace->last_nl);
      }
      else error("Write_dyn_trace_n, cjson trace problem\n");
   }
   if (contents) free(contents);
   return n;
}


size_t Write_dyn_trace_pad(void *userp,enum term_color color, int len2pad,const char *message, ...)
{
   if (!userp || !message|| strlen(message)==0) return -1;
   
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   
   size_t consten_buf_len=0;
   char * contents=NULL;
   // print into temp buffer and adjust buffer till content fits
   va_list varg;
   va_start (varg, message);
   int cnt=0;
   int res=0;
   do {
      cnt++;
      consten_buf_len=trace->min_trace_buf_increment * cnt;
      if (contents) free(contents);
      contents=NULL;
      contents=calloc(1,consten_buf_len);
      res=-1;
      if (contents)
         res=vsnprintf(contents,consten_buf_len ,message,varg);
      else
         break;
   } while((size_t)res >= consten_buf_len);
   va_end(varg);
   
   if (res<0)
   {
      error("Buffer error contents\n");
      if (contents) free (contents);
      contents=NULL;
   }
   size_t realsize=-1;
   // with content and length, check if trace buffer needs to be increased
   if (contents)
   {
      
      realsize = strlen(contents);
      char pad[]="                                                                                                                                                                       ";
      if (realsize<=len2pad) pad[len2pad-realsize]=0;
      else
      { // trim contents
         contents[len2pad]=0;
         if (len2pad>2)
         {
            contents[len2pad-1]='.';
            contents[len2pad-2]='.';
         }
         pad[0]=0;
      }
      size_t n;
      // content ready, let's calculate the length
      if (color== none)
         n= strlen(contents) + strlen(pad);
      else if (color== reset)
         n= strlen(contents)+strlen(pad)+strlen(ANSI_RESET);
      else n= strlen(contents) + strlen(color_data[color]) + strlen(ANSI_RESET) + strlen(pad);
      cJSON * item=cJSON_GetObjectItem(trace->params, "trace" );
      if (item && item->valuestring)
      {
         // increase buffer if needed
         if (trace->trace_len+1+n> trace->trace_buf_size)
         {
            size_t newsize=trace->trace_buf_size+ MAX(trace->min_trace_buf_increment, n+1);
            char * temp=realloc(item->valuestring,newsize);
            if (temp )
            {
               trace->trace_buf_size=newsize;
               if (temp!=item->valuestring)
               {
                  item->valuestring=temp;
               }
            }
            else
            {
               error("Write_dyn_trace_n, no more mem");
               if (contents) free(contents);
               return -1;
            }
         }
         char cr[2]={0,0};
        
         if (contents[strlen(contents)-1]=='\n')
         {
            cr[0]='\n';
            contents[strlen(contents)-1]=0;
         }
        
         // append contents with padding to the buffer
         if (color== none)
            sprintf(item->valuestring+trace->trace_len,"%s%s%s",contents,pad,cr);
         else if (color==reset)
            sprintf(item->valuestring+trace->trace_len,"%s%s%s%s",ANSI_RESET,contents,pad,cr);
         else
            sprintf(item->valuestring+trace->trace_len,"%s%s%s%s%s",color_data[color],contents,ANSI_RESET,pad,cr);
         trace->trace_len+=n;
         item->valuestring[trace->trace_len]=0;
         //do_carriage_return(cJSON_get_key(trace->params, "trace"), &trace->trace_len, &trace->first_cr, &trace->last_nl);
      }
      else error("Write_dyn_trace_n, cjson trace problem\n");
   }
   if (contents) free(contents);
   return realsize;
}

void set_dyn_trace_state(void *userp,char * state)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   if (state)
   {
      if (cJSON_get_key(trace->params, "state"))
         cJSON_ReplaceItemInObject(trace->params, "state", cJSON_CreateString(state));
      else
         cJSON_AddItemToObject(trace->params, "state", cJSON_CreateString(state));
   }
}


/*------------------------------------------------------------------------
 * Helper function get trace string
 *------------------------------------------------------------------------*/
const char * get_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   return cJSON_get_key(trace->params, "trace");
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
size_t clear_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   cJSON * tmp=cJSON_GetObjectItem( trace->params, "trace");
   if (tmp)
   {
      tmp->valuestring[0]=0;
      trace->trace_len=0;
      trace->mark=-1;
      trace->last_nl=0;
      trace->first_cr=0;
   }
   else
      error("could not get trace\n");
   return 0;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void clear_till_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   if (trace->mark!=-1 && trace->trace_len>0 && trace->trace_len>trace->mark)
   {
      cJSON * tmp=cJSON_GetObjectItem( trace->params, "trace");
      if (tmp)
      {
         tmp->valuestring[trace->mark]=0;
         trace->trace_len=trace->mark;
         trace->last_nl=0;
         trace->first_cr=0;
      }
      else
         error("could not get trace\n");
   }
   return;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void set_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->mark=trace->trace_len;
   trace->last_diff=0;
   return;
}

void set_time_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->time_mark=trace->trace_len;
   return;
}


void dyn_trace_add_decrement(void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->count++;
   if (trace->count>99) trace->count=0;
   char dec[200];
   int i;
   for (i=0;i<(200-trace->count);i++) dec[i]=' ';
   Write_dyn_trace(trace, none, "%s\n",dec);
	
}

void dyn_trace_add_decrement_nl(void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->count++;
   if (trace->count>99) trace->count=0;
   char dec[200];
   int i;
   for (i=0;i<(200-trace->count*2);i++) dec[i]=' ';
   Write_dyn_trace(trace, none, "%s",dec);
}

void update_time_dyn_trace(void * userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
time_t timer;
char time_str[26];
struct tm* tm_info;
time(&timer);
tm_info = localtime(&timer);
strftime(time_str, 26, "%Y-%m-%d %H:%M:%S\n", tm_info);
   memcpy(cJSON_get_stringvalue_as_var(trace->params,"trace")+trace->time_mark, time_str, strlen(time_str));
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void clear_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->mark=-1;
   return;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void free_dynamic_trace(void **userp)
{
   if (*userp)
   {
      struct trace_Struct **trace_p = (struct trace_Struct **)userp;
      if ((*trace_p)->params)cJSON_Delete( (*trace_p)->params);
      (*trace_p)->params=NULL;
      if ((*trace_p)->url) free((*trace_p)->url);
      (*trace_p)->url=NULL;
      if ((*trace_p)->curl) curl_easy_cleanup((*trace_p)->curl);
      (*trace_p)->curl=NULL;
      (*trace_p)->mark=-1;
      free (*trace_p);
      (*trace_p)=NULL;
   }
   return;
}

void print_api_error(struct trace_Struct* trace,int error)
{
	if (error!=200)
	{
		{
			if (error<90 && error>0)
			{
				Write_dyn_trace(trace, magenta, "\n\tERROR:(%d)  %s\n",error,curl_easy_strerror(error));
			}
			else
				switch (error) {
					case 404:
						Write_dyn_trace(trace, magenta, "\n\tNot Found (HTTP %d)\n",error);
						break;
					case 500:
						Write_dyn_trace(trace, magenta, "\n\tServer error (HTTP %d)\n",error);
						break;
					case 403:
						Write_dyn_trace(trace, magenta, "\nForbidden (HTTP %d)\n",error);
						break;
					case 401:
						Write_dyn_trace(trace, magenta, "\n\tNot Authorised (HTTP %d)\n",error);
						break;
					default:
						Write_dyn_trace(trace, magenta, "\n\tHttp-Error : HTTP %d\n",error);
						break;
				}
		}
	}
}

