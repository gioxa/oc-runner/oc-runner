/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! @file virgin.h
 *  @brief virgin startup headers, registering runner, deploy new runner.yml
 *  @author danny@gioxa.com
 *  @date 28/08/18
 *  @copyright (c) 2018 Danny Goossen,Gioxa Ltd.
 */


#ifndef __oc_runner__virgin__
#define __oc_runner__virgin__

#include <stdio.h>

/**
 * \brief process virgin config map, create new deployment and configmap, delete current deployment
 */
int virgin_process(struct oc_api_data_s *oc_api_data ,parameter_t * parameters);

#endif /* defined(__oc_runner__virgin__) */
