/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the oc-runner
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! \file image_parameters.h
 *  \brief headers fparameter handling image for registry/Imagestream etc
 *  \author Created by Danny Goossen on  29/7/18.
 *  \copyright 2018, Danny Goossen. MIT License
 *
 */


#ifndef oc_runner_image_parameters_h
#define oc_runner_image_parameters_h

#include "common.h"
#include "dkr_api.h"
#include "oc_api.h"
#include "openssl.h"
#include "cJSON_deploy.h"
#include "dyn_trace.h"
#include "image_parameters.h"

#include "wordexp_var.h"

/**
 \brief typedef parameter structure, \b  dkr_parameter_s
 */
typedef  struct dkr_parameter_s dkr_parameter_t;

/**
 \brief parameter structure, general sub-program settings
 Parameter settings for the dkr API sub programs
 */
struct dkr_parameter_s
{
	u_int8_t verbose; /**< verbose output */
	u_int8_t quiet;   /**< no output */
	u_int8_t u2g;   /**< copy user permissions to group */
	u_int8_t allow_fail;   /**< if set, on failure, exit code will be 0 */
	u_int8_t GLR;       /**< is GLR registry */
	u_int8_t ISR;       /**< is ISR registry */
	u_int8_t skip_auto; /**< skip adding extra layer on a pure FROM */
	char * rootfs;        /**< absolute path to rootfs to transform to docker image */
	//char * content;       /**< what content to add to the layer, defaults to "." */
	char * image_name;    /**< imagename, as subspacename of the oc runner e.g. $PROJECT_PATH_NAME_SLUG */
	char * reference;     /**< tag reference to push image*/
	char * namespace;     /**< namespace of the registry (namespace oc) */
	char * registry_name; /**< for imagestream ip:5000 */
	char * image;         /**< full registry image name */
	char * image_nick;    /**< image nick name e.g. ImageStream/test:latest */
	char * image_dir;     /**< directory for image layer.tar.gz / config / manifest */
	char * archive;  /**< Archive name */
	char * tag;           /**< tag reference */
	char * creds;         /**< credentials base64 usr/token */
	char * layer_content_digest; /**<  digest of content */
	char * layer_blob_digest;/**< digest of blob*/
	size_t layer_size;/**< size of the blob */
	char * docker_config; /**< yaml file containing the blob*/
	char * docker_config_digest; /**< yaml file containing the blob*/
	char * image_sha;     /**< the SHA256 digest of the manifest */
	char * image_description;     /**< the short description as per DockerHub */
	char * image_long_description_filename;     /**< the long description as per DockerHub fileref */
	char * const * pathname; /**< pointer list of pathname to add to layer */
	size_t pathname_cnt; /**< number of pathnames */
	char * info_command; /**< info for docker config history / container command */
	char * alt_name; /**< for label: name */
};

void clear_dkr_parameters(dkr_parameter_t **parameter);

/**
 \brief internal, set's the parameters according to the arguments
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameter docker registry image parameters
 
 */
int process_options(size_t argc, char *const*argv, dkr_parameter_t *parameter);


/**
 \brief function to prepare and initialize the dkr gitlab registry api
 
 this function calculates image details (registry name, image_name and reference),
 initializes the registry api
 and adds the registry to the registry database
 posible commandline options:
 * image_name
 * reference
 
 parameter->imagename is compiled as '<CI_PROJECT_PATH>[/image_name]'
 
 parameters->image as <CI_REGISTRY_IMAGE>/[/image_name][:reference]
 
 parameters->reference as <reference> or latest is <reference> is not defined
 
 parameters->registry_name as <CI_REGISTRY>
 
 <CI_REGISTRY_IMAGE> is added to the registry database with <CI_REGESTRY_USER/PASSWORD>
 
 
 \param job environment variables defined per ci-job in struct cJSON
 \param parameters docker registry image parameters
 \param api_data returns pointer to api_data docker api data structure
 \param argc argument count \b argv
 \param argv variable arguments passed from command line in script
 \return 0 on success
 \note caller is responsible for freeing \b api_data
 */
int prep_GLR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,size_t argc,char*const*argv);


int set_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data);


int set_registry_by_image(const cJSON * job, const char * image,struct dkr_api_data_s * api_data);


void set_GLR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data);


// used by public push registry
int prep_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data);

void set_ISR_registry(const cJSON * job, dkr_parameter_t * parameters,struct dkr_api_data_s ** api_data,struct oc_api_data_s **oc_api_data);

#endif
