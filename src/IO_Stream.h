/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.

 This file is part of the oc-runner

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/*! \IO_Stream.h
 *  \brief helper callback to retrieve curl headers into cJSON object on the fly
 *  \author Created by Danny Goossen on  8/8/18.
 *  \copyright 2018, Danny Goossen. MIT License
 */

#ifndef __oc_runner__IO_Stream__
#define __oc_runner__IO_Stream__

#define _GNU_SOURCE
#include <stdio.h>

#define IO_Stream_getc(IO_Stream) (IO_Stream->getc)(IO_Stream)
#define IO_Stream_ferror(IO_Stream) (IO_Stream->ferror)(IO_Stream)
#define IO_Stream_unget(IO_Stream,c) (IO_Stream->ungetc)(IO_Stream,c)
#define IO_Stream_putc(IO_Stream,c) (IO_Stream->putc)(IO_Stream,c)
#define IO_Stream_fwrite(IO_Stream,ptr,size,nmemb) (IO_Stream->fwrite)(IO_Stream,ptr,size,nmemb)

#define IO_Stream_fread(IO_Stream,ptr,size,nmemb) (IO_Stream->fread)(IO_Stream,ptr,size,nmemb)

#define IO_Stream_fputs(IO_Stream,string) (IO_Stream->fputs)(IO_Stream,string)

/**
 * \brief Defines stream type
 *
 */
typedef enum {
	/** IO_Stream with file */
	io_type_file,

	/** IO_Stream with dynbuf */
	io_type_DynBuffer,

} io_type_t;

typedef struct IO_Stream_s IO_Stream;
/**
 \brief struct holding the wrappers
 \note does not hold the actual stream data

 */
typedef struct IO_Stream_s {
	io_type_t type; /**< stream type (file or dynbuffer) */
	void * file; /**< the data object reference (FILE* or DynBuf*) */
	int(*getc)(IO_Stream *); /**< getc wrapper */
	int(*ungetc)(IO_Stream *,int); /**< ungetc wrapper */
	size_t(*fwrite)(IO_Stream *, const void *, size_t, size_t );  /**< fwrite wrapper */
	size_t(*fread)(IO_Stream *, void *, size_t, size_t );  /**< fread wrapper */
	int(*fputs)(IO_Stream *,const char *);  /**< fputs wrapper */
	int(*putc)(IO_Stream *,int);  /**< putc wrapper */
	int(*ferror)(IO_Stream *);  /**< ferror wrapper */
}IO_Stream;

/**
 \brief creates a new IO_Stream object
 \return the IO_Stream wrapper object
 */
IO_Stream * IO_Stream_init(void * p,io_type_t type);

/**
 \brief free the IO_Stream struct
 \note does not free stream data
 */
void IO_Stream_clear(IO_Stream**self);

/**
 \brief copy IO_Stream from in to out
 */
int IO_Stream_copy(IO_Stream * in, IO_Stream * out, char ** errormsg ,size_t BlockSize);

#endif /* defined(__oc_runner__IO_Stream__) */
