image start

```mermaid
graph TB

  ci1((image check)) --> RM[ Read Manifest]
  RM --> HM{ Have Manifest }
  HM -- yes -->  CV{ Check Shema Version}
  HM -- no -->  Er((Error))
  CV -- V1 --> Vdef1( Default Schma Version 1) 
  CV -- V2 --> Vdef2( Default Schma Version 2) 
  
  Vdef1 --> Rv2[ Read V2 ]
  
  Rv2 --> cv2{ Have V2 }
  cv2 -- no --> NHV2( No V2 Support )
  NHV2 --> TB[ transfer Blobs ]
  cv2 -- yes --> Rcf[ Read Config]
  Vdef2 --> Rcf
  Rcf[ Read Config] --> TB

 TB --> NMC{ No V2 Support }
 NMC -- yes --> NCC[ Create config from V1 ]
 NMC -- no --> SCF[ Sent config ]
 NCC --> SCF
 SCF --> SMV2[ Sent V2 Manifest ]
 SMV2 --> IMC{ Invalid Manifest }
 IMC -- yes --> CCFV2[ Create V1 manifest ]
 CCFV2 --> SMV1[ Sent V1 Manifest ]
 IMC -- no --> SSS(( Success ))
 SMV1 --> SSS
  

     classDef green fill:#9f6,stroke:#333,stroke-width:2px;
     classDef orange fill:#f96,stroke:#333,stroke-width:4px;
     class sq,e green
     class di orange
```