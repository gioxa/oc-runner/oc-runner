//
//  test_specials.c
//  oc-runner
//
//  Created by Danny Goossen on 4/5/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

//LCOV_EXCL_START
#include <check.h>
//LCOV_EXCL_STOP

#include "../src/specials.h"
#include "../src/cJSON_deploy.h"
#include "../src/yaml2cjson.h"

#include "../src/error.h"

#define DOCKER_REGISTRY "registry.hub.docker.com"

//LCOV_EXCL_START
//Dummy for compling
size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...)
{
	return 0;
}
//LCOV_EXCL_STOP
// the command structure


static Suite * specials_suite(void);


//get_gitlab_group_url

START_TEST(check_slugit)
{
	struct test_data_t
	{
		char * name_space;
		char * name_ref;
	} ;
	static const struct test_data_t testlist[4] = {
		{ "!@#hallo#$%^","hallo"},
		
	{ "012xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx","012xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"},
		{ "gioxa#$%image-test","gioxa-image-test"},
		{ "@#$%",NULL},
	};
	char * name_ref=NULL;
	
	name_ref= slug_it(testlist[_i].name_space,0);
	if (testlist[_i].name_ref)
	{
		ck_assert_ptr_nonnull(name_ref);
	    ck_assert_str_eq(name_ref,testlist[_i].name_ref );
	}
	else ck_assert_ptr_null(name_ref);
	if (name_ref) free(name_ref);
}
END_TEST


START_TEST(check_chop_ns)
{
	struct test_data_t
	{
		char * name_space;
		char * name_ref;
	} ;
	static const struct test_data_t testlist[6] = {
		{ "library/centos","centos"},
		{ "gioxa/oc-runner/oc-runner","oc-runner"},
		{ "gioxa/oc-runner/build-image","build-image-oc-runner"},
		{ "centos/redis","redis-centos"},
		{ "dgoo2308/fpm-image","fpm-image-dgoo2308"},
		{ "deployctl/test_docker_config", "test-docker-config-deployctl"},
	};
	char * name_ref=NULL;
	
	name_ref= name_space_rev_slug(testlist[_i].name_space);
	
	ck_assert_ptr_nonnull(name_ref);
	ck_assert_str_eq(name_ref,testlist[_i].name_ref );
}
END_TEST

START_TEST(check_chop_image)
{
	struct test_data_t
	{
		char * image;
		char * registry;
		char * name_space;
		char * reference;
	} ;
	static const struct test_data_t testlist[39] = {
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build","172.30.208.107:5000","oc-runner/is-deployctl-test-oc-build",NULL},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:1","172.30.208.107:5000","oc-runner/is-deployctl-test-oc-build","1"},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:1.1","172.30.208.107:5000","oc-runner/is-deployctl-test-oc-build","1.1"},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:1:1","172.30.208.107:5000","oc-runner/is-deployctl-test-oc-build","1:1"},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:1-1.2","172.30.208.107:5000","oc-runner/is-deployctl-test-oc-build","1-1.2"},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","172.30.208.107:5000","oc-runner/is-deployctl-test-oc-build","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		
		{ "172.30.208.107/oc-runner/is-deployctl-test-oc-build","172.30.208.107","oc-runner/is-deployctl-test-oc-build",NULL},
		{ "172.30.208.107/oc-runner/is-deployctl-test-oc-build:1","172.30.208.107","oc-runner/is-deployctl-test-oc-build","1"},
		{ "172.30.208.107/oc-runner/is-deployctl-test-oc-build:1.1","172.30.208.107","oc-runner/is-deployctl-test-oc-build","1.1"},
		{ "172.30.208.107/oc-runner/is-deployctl-test-oc-build:1:1","172.30.208.107","oc-runner/is-deployctl-test-oc-build","1:1"},
		{ "172.30.208.107/oc-runner/is-deployctl-test-oc-build:1-1.2","172.30.208.107","oc-runner/is-deployctl-test-oc-build","1-1.2"},
		{ "172.30.208.107/oc-runner/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","172.30.208.107","oc-runner/is-deployctl-test-oc-build","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		
		{ "a.b:5000/oc-runner/is-deployctl-test-oc-build","a.b:5000","oc-runner/is-deployctl-test-oc-build",NULL},
		{ "a.b:5000/oc-runner/is-deployctl-test-oc-build:1","a.b:5000","oc-runner/is-deployctl-test-oc-build","1"},
		{ "a.b:5000/oc-runner/is-deployctl-test-oc-build:1.1","a.b:5000","oc-runner/is-deployctl-test-oc-build","1.1"},
		{ "a.b:5000/oc-runner/is-deployctl-test-oc-build:1:1","a.b:5000","oc-runner/is-deployctl-test-oc-build","1:1"},
		{ "a.b:5000/oc-runner/is-deployctl-test-oc-build:1-1.2","a.b:5000","oc-runner/is-deployctl-test-oc-build","1-1.2"},
		{ "a.b:5000/oc-runner/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","a.b:5000","oc-runner/is-deployctl-test-oc-build","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		
		{ "a.b/oc-runner/is-deployctl-test-oc-build","a.b","oc-runner/is-deployctl-test-oc-build",NULL},
		{ "a.b/oc-runner/is-deployctl-test-oc-build:1","a.b","oc-runner/is-deployctl-test-oc-build","1"},
		{ "a.b/oc-runner/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","a.b","oc-runner/is-deployctl-test-oc-build","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		{ "a.b/oc-runner/is-deployctl-test-oc-build:1.1","a.b","oc-runner/is-deployctl-test-oc-build","1.1"},
		{ "a.b/oc-runner/is-deployctl-test-oc-build:1:1","a.b","oc-runner/is-deployctl-test-oc-build","1:1"},
		{ "a.b/oc-runner/is-deployctl-test-oc-build:1-1.2","a.b","oc-runner/is-deployctl-test-oc-build","1-1.2"},
		
		
		{ "centos",DOCKER_REGISTRY,"library/centos",NULL},
		{ "centos:1",DOCKER_REGISTRY,"library/centos","1"},
		{ "centos:1.1",DOCKER_REGISTRY,"library/centos","1.1"},
		{ "centos:1:1",DOCKER_REGISTRY,"library/centos","1:1"},
		{ "centos:1-1.2",DOCKER_REGISTRY,"library/centos","1-1.2"},
		{ "centos@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d",DOCKER_REGISTRY,"library/centos","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		
		{ "jekyll/builder",DOCKER_REGISTRY,"jekyll/builder",NULL},
		{ "jekyll/builder:1",DOCKER_REGISTRY,"jekyll/builder","1"},
		{ "jekyll/builder:1.1",DOCKER_REGISTRY,"jekyll/builder","1.1"},
		{ "jekyll/builder:1:1",DOCKER_REGISTRY,"jekyll/builder","1:1"},
		{ "jekyll/builder:1-1.2",DOCKER_REGISTRY,"jekyll/builder","1-1.2"},
		{ "jekyll/builder@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d",DOCKER_REGISTRY,"jekyll/builder","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		
		{ "dockregi.gioxa.com/docker/centos7-coreutils","dockregi.gioxa.com","docker/centos7-coreutils",NULL},
		{ "dockregi.gioxa.com/docker/centos7-coreutils:1","dockregi.gioxa.com","docker/centos7-coreutils","1"},
		
		{ "dockregi.gioxa.com/docker/centos7-coreutils:latest","dockregi.gioxa.com","docker/centos7-coreutils","latest"},
		
	};

	cJSON * env_vars=cJSON_CreateObject();
	cJSON_AddStringToObject(env_vars,"CI_PROJECT_PATH_SLUG" , "deployctl-test-oc-build");
	cJSON_AddStringToObject(env_vars,"CI_REGISTRY_IMAGE" , "dockregi.gioxa.com/deployctl/test_oc_build");
	cJSON_AddStringToObject(env_vars,"TEST_VAR" , "testvar");
	cJSON_AddStringToObject(env_vars,"" , "");
	
	char * reference=NULL;
	char * registry=NULL;
	char * namespace=NULL;
	
	chop_image(testlist[_i].image, &registry, &namespace,&reference );
	ck_assert_ptr_nonnull(registry);
	ck_assert_ptr_nonnull(namespace);
	ck_assert_str_eq(registry,testlist[_i].registry );
	ck_assert_str_eq(namespace,testlist[_i].name_space);
	if (reference && testlist[_i].reference)ck_assert_str_eq(reference,testlist[_i].reference);
	ck_assert(!((reference && !testlist[_i].reference) || (!reference && testlist[_i].reference)));
	if (reference) free(reference);
	if(registry) free(registry);
	if (namespace) free(namespace);
}
END_TEST


START_TEST(check_process_nick)
{
	struct test_data_t
	{
		char * nick;
		char * nick_result;
		char * image;
	} ;
	static const struct test_data_t testlist[17] = {
		{ "centos", "centos","registry.hub.docker.com/library/centos"},
		{ "gioxa/oc-runner:1", "gioxa/oc-runner:1","registry.hub.docker.com/gioxa/oc-runner:1"},
		{ "ImageStream", "ImageStream","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build"},
		{ "ImageStream:1.1-2", "ImageStream:1.1-2","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:1.1-2"},
		{ "ImageStream/lev1", "ImageStream/lev1","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build-lev1"},
		{ "ImageStream/$TEST_VAR", "ImageStream/testvar","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build-testvar"},
		{ "ImageStream@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "ImageStream@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		{ "ImageStream/$TEST_VAR@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "ImageStream/testvar@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build-testvar@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		{ "Gitlab", "Gitlab","dockregi.gioxa.com/deployctl/test_oc_build"},
		{ "Gitlab:1-2.3", "Gitlab:1-2.3","dockregi.gioxa.com/deployctl/test_oc_build:1-2.3"},
		{ "Gitlab/lev2:1-2.3", "Gitlab/lev2:1-2.3","dockregi.gioxa.com/deployctl/test_oc_build/lev2:1-2.3"},
		{ "Gitlab/lev2@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "Gitlab/lev2@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","dockregi.gioxa.com/deployctl/test_oc_build/lev2@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		{ "Gitlab@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "Gitlab@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","dockregi.gioxa.com/deployctl/test_oc_build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
		{ "dockregi.gioxa.com/docker/centos7-coreutils","dockregi.gioxa.com/docker/centos7-coreutils","dockregi.gioxa.com/docker/centos7-coreutils"},
		{ "dockregi.gioxa.com/docker/centos7-coreutils:1","dockregi.gioxa.com/docker/centos7-coreutils:1","dockregi.gioxa.com/docker/centos7-coreutils:1"},
		{ "$CI_REGISTRY/docker/centos7-coreutils:latest","dockregi.gioxa.com/docker/centos7-coreutils:latest","dockregi.gioxa.com/docker/centos7-coreutils:latest"},
		{ "Gitlab/noci","Gitlab/noci",NULL}
	};

	cJSON * env_vars=cJSON_CreateObject();
	cJSON_AddStringToObject(env_vars,"CI_PROJECT_PATH_SLUG" , "deployctl-test-oc-build");
	if(strcmp("Gitlab/noci",testlist[_i].nick)!=0)
    	cJSON_AddStringToObject(env_vars,"CI_REGISTRY_IMAGE" , "dockregi.gioxa.com/deployctl/test_oc_build");
	cJSON_AddStringToObject(env_vars,"TEST_VAR" , "testvar");
	cJSON_AddStringToObject(env_vars,"" , "");
		cJSON_AddStringToObject(env_vars,"CI_REGISTRY" , "dockregi.gioxa.com");
	
	char *image_nick=strdup(testlist[_i].nick);
	char * image=NULL;

	image=process_image_nick(&image_nick, env_vars, "172.30.208.107:5000", "oc-runner", "registry.hub.docker.com");
	ck_assert_ptr_nonnull(image_nick);
	ck_assert_str_eq(image_nick,testlist[_i].nick_result );
	if (testlist[_i].image)
	ck_assert_str_eq(image,testlist[_i].image);
	else ck_assert_ptr_null(image);
	free(image_nick);
	free(image);
}
END_TEST

//int change_image_tag(char ** image, const char *new_tag)

START_TEST(check_change_image_tag)
{
	struct test_data_t
	{
		char * image;
		char * tag;
		char * image_result;
		int result;
	} ;
	static const struct test_data_t testlist[13] = {
		{ "ImageStream", "1","ImageStream:1",0},
		{ "ImageStream:latest", "1","ImageStream:1",0},
		{ "ImageStream:1", NULL,"ImageStream",0},
		{ "ImageStream@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", NULL,"ImageStream",0},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "1.1-2","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:1.1-2",0},
		{ "ImageStream/lev1@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "1","ImageStream/lev1:1",0},
		{ "", "latest","",(int) (-1)},
		{ NULL, "latest",NULL,(int)(-1)},
		{ NULL, NULL,NULL,(int)(-1)},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build", "1","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:1",0},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:2", "3","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:3",0},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build:latest", "","172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build",0},
		{ "172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build", NULL,"172.30.208.107:5000/oc-runner/is-deployctl-test-oc-build",0}
	};
	
	char *image=NULL;
	if (testlist[_i].image) image=strdup(testlist[_i].image);
	
	
	cJSON * env_vars=cJSON_CreateObject();
	cJSON_AddStringToObject(env_vars,"CI_PROJECT_PATH_SLUG" , "deployctl-test-oc-build");
	cJSON_AddStringToObject(env_vars,"CI_REGISTRY_IMAGE" , "dockregi.gioxa.com/deployctl/test_oc_build");
	cJSON_AddStringToObject(env_vars,"TEST_VAR" , "testvar");
	cJSON_AddStringToObject(env_vars,"" , "");
	
	int res=change_image_tag(&image,testlist[_i].tag);
	//printf("%s, \'%s\' =>%d : \'%s\' whish \'%s\'\n",testlist[_i].image,testlist[_i].tag,res,image,testlist[_i].image_result);
	ck_assert_int_eq(res, testlist[_i].result);
	if (testlist[_i].image)
	{
		ck_assert_ptr_nonnull(image);
	    ck_assert_str_eq(image,testlist[_i].image_result );
	}
	else
		ck_assert_ptr_null(image);
	if (image) free(image);
}
END_TEST

struct test_data_comma_t
{
	char * data;
	char *result;
} ;

static const struct test_data_comma_t test_data_comma[] = {
	{"test,hallo", "- test\n- hallo\n" },
	{"test", "- test\n" },
	{"test,hallo,", "- test\n- hallo\n" },
	{"test   ,   hallo  ", "- test\n- hallo\n" },
	NULL
};






START_TEST(check_comma_sep_list)
{
	//printf("%d: %s",_i,test_data_comma[_i].data);
	cJSON * test=comma_sep_list_2_json_array(test_data_comma[_i].data);
	cJSON* result=yaml_sting_2_cJSON (NULL ,test_data_comma[_i].result);
	int res=0;
	cJSON * wish=cJSON_GetArrayItem(result,0);
	int len_test=cJSON_GetArraySize(test);
	int len_result=cJSON_GetArraySize(wish);
	if (len_test!=len_result)
	{
		
		res=-1;
	}
	else
	{
		int j=0;
		for (j=0;j<len_result;j++)
		{
			const char * a=cJSON_GetArrayItem(wish, j)->valuestring;
			const char * b=cJSON_GetArrayItem(test, j)->valuestring;
			if(strcmp(a,b)!=0){
				res=-1;
				break;
			}
		}
	}
	cJSON_Delete(test);
	cJSON_Delete(result);
	ck_assert_int_eq(res, 0);
}
END_TEST


void cb(void*a,char*path);

void cb(void*a,char*path)
{
	cJSON_AddItemToBeginArray((cJSON*)a, cJSON_CreateString(path));
}




START_TEST(check_recursive_dir)
{
	cJSON* list=cJSON_CreateArray();
	cJSON* cblist=cJSON_CreateArray();
	cJSON* wish=cJSON_CreateArray();
	cJSON_AddItemToBeginArray(wish, cJSON_CreateString("/build"));
	cJSON_AddItemToBeginArray(wish, cJSON_CreateString("/build/test3"));
	cJSON_AddItemToBeginArray(wish, cJSON_CreateString("/build/test3/test31"));
	const char test[]="/build/test";
	size_t len=strlen(test);
	recursive_dir_extract(test, len, list, cb, cblist);
	
	const char test2[]="/build/test2";
	size_t len2=strlen(test2);
	recursive_dir_extract(test2, len2, list, cb, cblist);
	
	const char test3[]="/build/test3/test31/";
	size_t len3=strlen(test3);
	recursive_dir_extract(test3, len3, list, cb, cblist);
	
	const char test4[]="/build/test3/test55";
	size_t len4=strlen(test4);
	recursive_dir_extract(test4, len4, list, cb, cblist);
	int i=0;
	int res=0;
	int wlen=cJSON_GetArraySize(wish);
	int llen=cJSON_GetArraySize(list);
	int cblen=cJSON_GetArraySize(cblist);
	for (i=0;i<wlen;i++)
	{
		res+=strcmp(cJSON_GetArrayItem(wish, i)->valuestring,cJSON_GetArrayItem(list, i)->valuestring);
		res+=strcmp(cJSON_GetArrayItem(wish, i)->valuestring,cJSON_GetArrayItem(cblist, i)->valuestring);
	}
	cJSON_Delete(cblist);
	cJSON_Delete(list);
	ck_assert_int_eq(res, 0);
	ck_assert_int_eq(wlen, llen);
	ck_assert_int_eq(wlen, cblen);
}
END_TEST

struct test_data_bool_t
{
	char * data;
	int false_test;
	int true_test;
} ;

static const struct test_data_bool_t test_data_bool[14] = {
	{"true",0,1 },
	{"True",0,1 },
	{"on",0,1 },
	{"ON",0,1 },
	{"1",0,1 },
	{"YeS",0,1 },
	{"False",1,0 },
	{"FALSE",1,0 },
	{"0",1,0 },
	{"OFF",1,0 },
	{"no",1,0 },
	{"somethingelse",0,0 },
	{"",0,0 },
	{NULL,0,0}
};

START_TEST(check_bool)
{
	if (test_data_bool[_i].data)
		setenv("mybooltest", test_data_bool[_i].data, 1);
	else
		unsetenv("mybooltest");
	ck_assert_int_eq(env_var_is_false("mybooltest"), test_data_bool[_i].false_test);
	ck_assert_int_eq(env_var_is_true("mybooltest"), test_data_bool[_i].true_test);
	unsetenv("mybooltest");
}
END_TEST


struct test_data_resolve_t
{
	char * data;
	char *result;
} ;

static const struct test_data_resolve_t test_data_resolve[5] = {
	{"t${test_var}", "ttestvar" },
	{"${unknown}", "" },
	{ "$test_var","testvar"},
	{ "$test_var;",NULL},
	{ NULL,NULL}
};

int resolve_vars(char ** image,const cJSON * env_vars);

START_TEST(check_resolve)
{
	cJSON*env=cJSON_CreateObject();
	cJSON_add_string(env, "test_var", "testvar");
	char * rc=resolve_const_vars(test_data_resolve[_i].data, env);
	if (test_data_resolve[_i].result)
	{
	ck_assert_ptr_nonnull(rc);
	ck_assert_str_eq(rc, test_data_resolve[_i].result);
	}
	else
		ck_assert_ptr_null(rc);
	char *cc=NULL;
	if (test_data_resolve[_i].data)cc=strdup(test_data_resolve[_i].data);
	int rcc=resolve_vars(&cc, env);
	if (test_data_resolve[_i].result)
	{
		ck_assert_int_eq(0,rcc);
		ck_assert_ptr_nonnull(rc);
		ck_assert_str_eq(rc, test_data_resolve[_i].result);
	}
	else
	{
		ck_assert_int_ne(0,rcc);
		ck_assert_ptr_null(rc);
	}
	if (env) cJSON_Delete(env);
	if(rc) free(rc);
	if(cc) free(cc);
}
END_TEST



Suite * specials_suite(void)
{
	Suite *s;
	TCase *tc_core;
	//TCase *tc_progress;
	s = suite_create("test_error");
	/* Core test case */
	tc_core = tcase_create("Core");
	//tcase_add_checked_fixture(tc_core, setup, teardown);
	tcase_add_loop_test (tc_core, check_slugit,0,4);
	tcase_add_loop_test (tc_core, check_chop_ns, 0, 5);
	tcase_add_loop_test (tc_core, check_chop_image, 0, 38);
	tcase_add_loop_test (tc_core, check_process_nick, 0, 17);
	tcase_add_loop_test (tc_core, check_change_image_tag, 0, 12);
	tcase_add_loop_test (tc_core, check_comma_sep_list, 0, 4);
	tcase_add_test(tc_core, check_recursive_dir);
	tcase_add_loop_test (tc_core, check_bool,0,14);
	tcase_add_loop_test (tc_core, check_resolve,0,5);
	tcase_add_unchecked_fixture(tc_core, NULL, NULL);
	tcase_set_timeout(tc_core,15);
	suite_add_tcase(s, tc_core);
	return s;
}


int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;
	
	s = specials_suite();
	sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
