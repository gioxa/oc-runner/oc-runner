/*
 utils.c
 Created by Danny Goossen, Gioxa Ltd on 2/5/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


#include "utils.h"
#include "../src/deployd.h"

int write_test_file(const char * base,const char * sub,const char * file, const char * data)
{
  //LCOV_EXCL_START
   char filename[1024];
   sprintf((char *)filename,"%s%s/%s",base,sub,file);
    debug("write test file %s\n",filename);
   FILE *f = fopen(filename, "w");
   if (f == NULL){ debug("Error opening file : %s\n",strerror(errno));return -1;}
   /* print some text */
   int retvalue=0;
   int errvalue=0;
   do{ retvalue=fprintf(f, "%s",data);} while ( retvalue==-1 && (errvalue=errno)== EINTR );
   if (retvalue<0) { debug("Error io-command %s: %s\n", filename,strerror(errvalue)); return -1; }
   if (fclose(f) <0 ) { debug("Error io-command %s: %s\n", filename,strerror(errno));return -1;}
   return retvalue;
   //LCOV_EXCL_STOP
}

int write_test_file_n(const char * base,const char * sub,const char * file, const char * data, size_t n)
{
	//LCOV_EXCL_START
	char filename[1024];
	sprintf((char *)filename,"%s%s/%s",base,sub,file);
	debug("write test file %s\n",filename);
	FILE *f = fopen(filename, "wb");
	if (f == NULL){ debug("Error opening file : %s\n",strerror(errno));return -1;}
	/* print some text */
	size_t retvalue=0;
	int errvalue=0;
	retvalue=fwrite(data,1,n,f);//;} while ( retvalue==0 && (errvalue=ferror(<#FILE *#>))== EINTR );

	if (retvalue!=n) { debug("Error io-command %s: %s\n", filename,strerror(errvalue)); return -1; }
	if (fclose(f) <0 ) { debug("Error io-command %s: %s\n", filename,strerror(errno));return -1;}
	return (int)retvalue;
	//LCOV_EXCL_STOP
}
