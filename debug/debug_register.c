//
//  debug_register.c
//  oc-runner
//
//  Created by Danny Goossen on 3/8/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "deployd.h"
#include "registry.h"

int  update_details_state(void * userp)
{
	return 0;
}

int  update_details(void * userp)
{
	return 0;
}

int main(void) {
	
	int res=0;
	const char command[]="registry_push --rootfs=$HOME/test_reg --ISR * /Users/dgoo2308";
	char pwdir[PATH_MAX];
	const char *this_dir=getcwd(pwdir,PATH_MAX);
	cJSON * env_vars=cJSON_CreateObject();
	cJSON * JOB=cJSON_CreateObject();
	cJSON_AddItemToObject(JOB, "env_vars", env_vars);
	cJSON_add_string(env_vars, "HOME", getenv("HOME"));
	setdebug();
	word_exp_var_t wev;
	
	memset(&wev, 0, sizeof(word_exp_var_t));
	int exp_res=bootstrap_expander_registry_push(&wev, command, env_vars, this_dir);
	if (!exp_res)
	{
		// just to be sure we're in the correct directory
	
		int i=0;
		debug ("Calling %s with:\n",wev.argv[0]);
		for (i=1;i<wev.argc;i++) debug("\t%s\n",wev.argv[i]);
		res=registry_push(JOB,trace, wev.argc,wev.argv);
	}
	else
	{
		res=-1;
		
	}
	word_exp_var_free(&wev);

	
	return res;
}
