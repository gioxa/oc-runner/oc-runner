//
//  main.c
//  testbash_substitude
//
//  Created by Danny Goossen on 16/10/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include <stdio.h>
#include "deployd.h"
#include "wordexp_var.h"

//cJSON * get_job(cJSON * runner,cJSON * info, int n_runners, CURL * curl);

//int get_artifact_auth(cJSON * job){return 0;}
//int upload_artifact(cJSON * job, char * upload_name){return 0;}
int update_details(void * p){return 0;}

int main(int argc, const char * argv[]) {
   //cJSON * env_vars=read_job_file(".","env.json");
	cJSON * as=cJSON_CreateStringArray((const char**)((char * const[]){"one","two","tree",NULL}) , 3);
	setdebug();
	print_json(as);
	cJSON * item=NULL;
	cJSON_ArrayForEach(item, as)
	{
		print_json(item);
	}
	cJSON * env_vars=cJSON_CreateObject();
	cJSON_add_string(env_vars, "CI_PROJECT_DIR", "/build");
	cJSON_add_string(env_vars, "CI_COMMIT_REF_NAME", "master");
   setdebug();
	word_exp_var_t result;
	int res=word_exp_var_dyn(&result, "${CI_COMMIT_REF_NAME#master}", env_vars, "/");
	//printf("\n (%d) result: %s\n",res,result);
	int i=0;
	for (i=0;i<result.argc;i++)
	{
		printf("\n (%d) result: >%s<\n",res,result.argv[i]);
	}
   // insert code here...
   printf("Hello, World!\n");
    return 0;
}
